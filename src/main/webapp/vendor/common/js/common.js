
/**
 * 公用模块
 * 2016-08-02
 */
/**
 * 创建对象,想当于命名空间，
 * 所有的可以给外部访问的属性和方法均放入
 */
var c = (function () {
    var C = {};


    C.formatObj = function (data, name) {
        var o = [];
        for (obj in data) {
            o.push({name: name + "." + obj, value: data[obj]});
        }
        return o;
    }, C.formatArray = function (array, name, obj) {
        if (!(arguments[0] instanceof Array && typeof arguments[1] == 'string')) {
            throw new Error(arguments[0] + arguments[1] + "参数传递类型不正确");
        }
        if (obj === undefined) {
            obj = [];
        }
        for (l in arguments[0]) {
            obj.push({name: name, value: arguments[0][l]});
        }
        return obj;
    },
    //清除复选框默认被选中
    C.clearCheckbox = function(){
		var tbody = document.querySelectorAll("tbody  input[type='checkbox']").checked=false;
		var thead = document.querySelector("thead  input[type='checkbox']").checked=false;
		tbody.checked=false;
		for(var i = 0;i < thead.length;i++){
			thead[i].checked=false;
		}
	}
    C.toggleCheckBox = function () {
        var array = [];
        var tbody = document.querySelectorAll("#checkBody  input[type='checkbox']");
        var thead = document.querySelector("#checkHead  input[type='checkbox']");
        if (arguments[0] === undefined) {
            for (var i = 0; i < tbody.length; i++) {
                tbody[i].checked = thead.checked;
                if (thead.checked) {
                    array.push(tbody[i].value);
                }
            }
        } else {
            for (var i = 0; i < tbody.length; i++) {
                if (tbody[i].checked)
                    array.push(tbody[i].value);
            }
            if (array.length != tbody.length)
                thead.checked = false;
            else
                thead.checked = true;

        }
        return array;
    },
    C.toggleCheckBoxAuthority = function () {
        var menu = document.querySelectorAll("#menuSelect input[type='checkbox']")
        var fun = document.querySelectorAll("#permissionSelect input[type='checkbox']")
        var menuIds = [];
        var permissionIds = [];
        for (var j = 0; j < fun.length; j++) {
            if (fun[j].checked) {
                permissionIds.push(fun[j].value)
            }
        }
        for (var i = 0; i < menu.length; i++) {
            if (menu[i].checked) {
                menuIds.push(menu[i].value)
            }
        }
        return menuIds.length+permissionIds.length;
    },
    C.check = function () {
        var array = [];
        var lis = document.querySelectorAll("input[type='checkbox']")
        for (var i = 0; i < lis.length; i++){
        	if(lis[i].checked){
        		array.push(lis[i].value);
        	}
        }
        return array;
    },
    C.commonEvent = function (dom, type, fn) {
        if (dom.addEventListener) {
            dom.addEventListener(type, fn);
        } else if (dom.attachEvent) {
            dom.attachEvent("on" + type, fn);
        } else {
            dom["on" + type] = fn;
        }
    }, C.formatParam = function (data) {
        var o = [];
        for (i in data) {
            o.push({name: i, value: data[i]})
        }
        return o;
    }, 
    C.formatUrl = function (url, data) {
        url += "?";
        for (i in data) {
            url += i;
            url += "=";
            url += data[i];
            url += "&";
        }
        var length = url.length - 1;
        return url.substring(0, length);
    },
	C.$confirm = function(title,afterFunction){
		layer.confirm(title,{
			title:'提示',
			btn: ['确定','取消'],
            skin: 'layer-ext-moon'
		},function(index){
			layer.close(index);
			afterFunction();
		},function(){
			layer.closeAll();
		})
	},
	
	C.$threeBtnConfirm = function(content, btn, afterFunction){
		layer.confirm(content,{
			title:'操作成功',
			btn: [btn, '继续添加'],
            skin: 'layer-ext-moon'
		},function(index){
			layer.close(index);
			afterFunction();
		}, function(index){
			layer.close(index);
		})
	},
        
        
        C.$msg = function(msg,life,theme){
      	  $.jGrowl.defaults.closerTemplate = "<div class='"+theme+"'>[ 关闭全部 ]</div>";
      	  $.jGrowl(msg,{
      		  header: '提示',
      		  speed:'fast',
      		  life:life,
      		  theme:theme
      	  });
      	},
      	
		C.$date = function(){
      		$(".time").datetimepicker({
      			icons : {
        			time : "fa fa-clock-o",
        			up : "fa fa-arrow-up",
        			down : "fa fa-arrow-down"
        		},
        		language:'zh-CN',
        		format:'yyyy-mm-dd',
        		minView:2,
        		todayBtn:true,
        		autoclose:true,
        	    pickDate: false,
        	    pickTime: false,
        	    
        	});
      	}
    	
      	C.$year = function(){
      		$(".year").datetimepicker({
      			icons : {
        			time : "fa fa-clock-o",
        			up : "fa fa-arrow-up",
        			down : "fa fa-arrow-down"
        		},
        		language:'zh-CN',
        		format:'yyyy',
        		minView:2,
        		todayBtn:true,
        		autoclose:true,
        	    pickDate: false,
        	    pickTime: false,
        	    
        	});
      	}
        C.$getRandomColor = function () {
            var c = '#';
            var cArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
            for (var i = 0; i < 6; i++) {
                var cIndex = Math.round(Math.random() * 15);
                c += cArray[cIndex];
            }
            return c;
        }
        
        C.toggleCheckBoxT = function () {
            var array = [];
            var tbody = document.querySelectorAll("#checkT  input[type='checkbox']");
            var thead = document.querySelector("#checkHeadT  input[type='checkbox']");
            if (arguments[0] === undefined) {
                for (var i = 0; i < tbody.length; i++) {
                    tbody[i].checked = thead.checked;
                    if (thead.checked) {
                        array.push(tbody[i].value);
                    }
                }
            } else {
                for (var i = 0; i < tbody.length; i++) {
                    if (tbody[i].checked)
                        array.push(tbody[i].value);
                }
                if (array.length != tbody.length)
                    thead.checked = false;
                else
                    thead.checked = true;

            }
            return array;
        },
        C.toggleCheckBoxD = function () {
            var array = [];
            var tbody = document.querySelectorAll("#checkBodyD  input[type='checkbox']");
            var thead = document.querySelector("#checkHeadD  input[type='checkbox']");
            if (arguments[0] === undefined) {
                for (var i = 0; i < tbody.length; i++) {
                    tbody[i].checked = thead.checked;
                    if (thead.checked) {
                        array.push(tbody[i].value);
                    }
                }
            } else {
                for (var i = 0; i < tbody.length; i++) {
                    if (tbody[i].checked)
                        array.push(tbody[i].value);
                }
                if (array.length != tbody.length)
                    thead.checked = false;
                else
                    thead.checked = true;

            }
            return array;
        }
        //显示加载层
        C.$load = function(){
        	layer.load(1, {shade: [0.3,'#fff']});
        }
        
        //关闭加载层
        C.$closeLoad = function(){
        	layer.closeAll('loading');
        }
        
        //post请求获取下载文件
        C.$postDownloadFile = function(url,data){
        	 $("#downloadform").remove();  
             var form = $("<form>");//定义一个form表单  
             form.attr("id", "downloadform");  
             form.attr("style", "display:none");  
             form.attr("target", "_blank");  
             form.attr("method", "post");  
             form.attr("action", url);  
             var input1 = $("<input>");  
             input1.attr("type", "hidden");  
             input1.attr("name", "data");  
             var str = JSON.stringify(data);
             input1.attr("value", str);  
             form.append(input1);  
             $("body").append(form);//将表单放置在web中  
             form.submit();//表单提交   
        }
        
        
    return C;
})()