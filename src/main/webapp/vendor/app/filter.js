angular.module("myApp.filter", [])
    .filter("sexFilter", function () {
        return function (sex) {
            return sex == '1' ? "先生" : "女士";
        }
    })
    .filter("idTypeFilter", function () {
        return function (idType) {
            console.log(typeof idType);
            if(idType == '0'){
            	return "身份证";
            }
            if(idType == '1'){
            	return "军官证";
            }
            if(idType == '2'){
            	return "护照"
            }
        }
    })
    .filter("educationFilter", function () {
        return function (education) {
            console.log(typeof education);
            if(education == '0'){
            	return "初中以下";
            }
            if(education == '1'){
            	return "高中/中专/技校";
            }
            if(education == '2'){
            	return "大学/大专";
            }
            if(education == '3'){
            	return "硕士以上";
            }
        }
    })
    .filter("sysFilter", function () {
        return function (system) {
            console.log(typeof system);
            return system ? "是" : "否";
        }
    })
    
    .filter("checkedFilter", function () {
        return function (id, check) {
            var bool = false;
            angular.forEach(check, function (data, index, array) {
                if (data.id == id) {
                    bool = true;
                }
            })
            return bool;
        }
    })
    .filter("isRoleAnMenu", function ($rootScope) {
        var menusList = $rootScope.menusRoleList;
        return function (menuId) {
            var boo = false;
            for (var i = 0; i < menusList.length; i++) {
                if (menuId == menusList[i].menu_id) {
                    boo = true;
                }
            }
            $rootScope.menusRoleList = null;
            return boo;
        }
    })
    .filter("isRoleAnPer", function ($rootScope) {
        var permissionList = $rootScope.permissionRoleList;
        return function (perId) {
            var boo = false;
            for (var i = 0; i < permissionList.length; i++) {
                if (perId == permissionList[i].permission_id) {
                    boo = true;
                }
            }
            $rootScope.permissionRoleList = null;
            return boo;
        }
    })
    .filter("isSelected", function ($rootScope) {
        return function (id, selected) {
        	return (selected).toString().indexOf(id) == -1 ? false : true;
        }
    })
    .filter('progressOrStatus',function(){
    	return function(progress,status){
    		if(progress==null){
    			return status;
    		}else{
    			return progress;
    		}
    	}
    })
    .filter('stringLength', function(){
    	return function(input,length){
    		if(input!=null){
    			if(input.length<=length){
    				return input;
    			}else{
    				return input.substring(0,length)+'...';
    			}
    		}else{
    			return null;
    		}
    	}
    })
    .filter("timedFilter", function () {
        return function (is_enable) {
            return is_enable ? "启用" : "禁用";
        }
    })
    .filter('classNameFilter', function(){
    	return function(input,length){
    		if(input!=null){
    			if(input.length<=20){
    				return input;
    			}else{
    				return input.substring(0,20)+'...';
    			}
    		}else{
    			return null;
    		}
    	}
    })
    
    .filter('dayFilter', function(){
    	return function(date,day,yMonth){
	    	if(date){
	    		day = date.substring(8,10);
	    		return day
	    	}	
    	}
    })
    
    .filter('yMonthFilter', function(){
    	return function(date,day,yMonth){
	    	if(date){
	    		yMonth = date.substring(0,7);
	    		return yMonth
	    	}	
    	}
    })
    
    
    
    .filter("statusFilter", function () {
        return function (item) {
            if(item == 'A'|| item == 'N'){
            	return "现行";
            }else if(item=='D'){
            	return "作废";
            }else{
            	return "全部"
            }
            
        }
    })
    
    
    .filter("standardFilter", function () {
        return function (item) {
            if(item == 'xxk'){
            	return "现行";
            }else if(item=='zfk'){
            	return "作废";
            }else{
            	return "全部"
            }
            
        }
    })
    
    .filter("typeFilter", function () {
        return function (item) {
            if(item == 'NEW'){
            	return "免费";
            }else if(item == 'PAY'){
            	return "已付费";
            }else if(item == 'UNPAY'){
            	return "未付费";
            }
            
        }
    })
    
    .filter('subStr', function(){
    	return function(item) {
    		if(item.indexOf("<br>") != -1){
//    			console.log(item.indexOf("<br>"));
    			return item.substring(0,item.indexOf("<br>"));
    		}
    	}
    })
    
    .filter('pushTypeFilter', function(){
    	return function(item){
    		if(item == "SUBSCRIPTION"){
    			return "领域品种";
    		}else if(item == "PUSH"){
    			return "标准跟踪";
    		}else if(item == "BUY"){
    			return "历史跟踪";
    		}
    	}
    })
    
    .filter('hideMail', function(){
    	return function(item){
    		if(String(item).indexOf('@') > 0){
    			var str = item.split('@');
    			var _s = '';
    			if(str[0].length > 6){
    				_s = '***'
    				var email = str[0].substring(0, 3) + _s + str[0].substring(6, str[0].length) + '@' + str[1];
    			} else {
    				if (str[0].length < 3 && str[0].length > 1) {
    					_s = '**'
    					var email = str[0].substring(0, 2) + _s + '@' + str[1];
    				} else if (str[0].length < 2) {
    					var email = item;
    				} else {
    					_s = '**'
    					var email = str[0].substring(0, 2) + _s + str[0].substring(4, str[0].length) + '@' + str[1];
    				}
    			}
    		}
    		return email;
    	}
    })
    
    .filter('provinceFilter', function(){
    	return function(text){
    		if(text == "省份"){
    			return "";
    		}else if(text == "市县"){
    			return "";
    		}else{
    			return text;
    		}
    		
    	}
    })
    
    .filter('urlFilter', function(){
    	return function(text){
    		if(text != null){
    			var index = text.lastIndexOf('/');
        	    if(index != -1){
        	    	text = text.substring(0,index);
        	    }
    		}
    	    return text;
    	}
    })
