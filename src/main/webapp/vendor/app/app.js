angular.module('myApp', ['ui.router',"ngFileUpload", 'ngSanitize', 'ngCookies', 'myApp.controller', 'myApp.service', 'myApp.filter', 'myApp.directive'])
    .run(function ($state,$rootScope,$http) {
        /*$http.post("/menus").success(function(data){
            $rootScope.menus = data;
        })*/
         $rootScope.$on('$locationChangeSuccess', function(a,b,c){
            $('.modal-backdrop').hide();
            $(document.body).removeClass("modal-open");
            $('body,html').animate({scrollTop:0},0);
        })
    })
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.interceptors.push(['$rootScope', '$q', '$location', '$timeout', function ($rootScope, $q, $location, $timeout) {
            return {
                'request': function (config) {
                    config.headers['X-Requested-With'] = 'XMLHttpRequest';
                    return config || $q.when(config);
                },
                'requestError': function (rejection) {
                    return rejection;
                },
                'response': function (response) {
                    return response || $q.when(response);
                },
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403 || response.status == 302) {
                        var deferred = $q.defer(), req = {
                            config: response.config,
                            deferred: deferred
                        };
                        c.$closeLoad();
                        c.$msg("会话超时，请重新登录！", 5000, "error");
                        $timeout(function () {
                            document.location = "/";
                        }, 3000);
                        return deferred.promise;
                    }else if (response.status === 500) {
                    	c.$closeLoad();
                        c.$msg("出错了~~~");
                        return false;
                    }
                    return $q.reject(response);
                }
            }
        }])
        $urlRouterProvider.otherwise("/main");
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "modules/main/main.html",
                controller:"MainController"
            })
	        .state("announcement_list",{//通知公告
	        	url: "/announcement_list/:id",
	        	templateUrl: 'modules/announcement/announcement.html',
	        	controller:"announcementController"
	        })
	        
	        .state("announcement_item",{//通知公告详情
	        	url: "/announcement_item/:channalId/:id",
	        	templateUrl: 'modules/announcement/announcement_item.html',
	        	controller:"announcementItemController"
	        })
	        
	        .state("standard_service",{//标准服务
	        	url: "/standard_service/:id",
	        	templateUrl: 'modules/stantard/standard_service.html',
	        	controller:"standardIntroduceController"
	        })
	        
	        .state("standard_introduce",{//标准服务简介详情
	        	url: "/standard_introduce/:contextId",
	        	templateUrl: 'modules/stantard/standard_introduce.html',
	        	controller:"standardIntroduceController"
	        })
	        
	        .state("special_list",{
	        	url: "/special_list",
	        	templateUrl: 'modules/special/special.html',
	        	controller:"specialController"
	        })
	        
	        .state("special_hit",{
	        	url: "/special_hit/:uid",
	        	templateUrl: 'modules/special/special_hit.html',
	        	controller:"specialController"
	        })
	        
	         .state("order_list",{
	        	url: "/order_list",
	        	templateUrl: 'modules/order/order_list.html',
	        	controller:"orderController"
	        })
	        
	        .state("order_list.handle_order_list",{//待处理订单列表
	        	url: "/handle_order_list",
        		views:{
    	        	'':{
    	        		templateUrl: 'modules/order/handle_order_list.html',
    	        		controller:"orderController"
    	        	}
    	        }
	        })
	        .state("order_list.proccess_order_list",{//已处理订单列表
	        	url: "/proccess_order_list",
        		views:{
    	        	'':{
    	        		templateUrl: 'modules/order/proccess_order_list.html',
    	        		controller:"orderProccessController"
    	        	}
    	        }
	        })
	        .state("order_list.abandon_order_list",{//中标分类
	        	url: "/abandon_order_list",
        		views:{
    	        	'':{
    	        		templateUrl: 'modules/order/abandon_order_list.html',
    	        		controller:"orderAbandonController"
    	        	}
    	        }
	        })
	        .state("order_item",{
	        	url: "/order_item/:code",
	        	templateUrl: 'modules/order/order_item.html',
	        	controller:"orderItemController"
	        })
	        
	        .state("cart",{
	        	url: "/cart",
	        	templateUrl: 'modules/cart/cart.html',
	        	controller:"cartController"
	        })
	        
	        .state("shop_process",{
	        	url: "/shop_process",
	        	params:{"cartList":null},
	        	templateUrl: 'modules/cart/shop_process.html',
	        	controller:"shopProcessController"
	        })
	        
	        .state("adderss",{
	        	url: "/adderss",
	        	templateUrl: 'modules/order/adderss.html',
	        	controller:"addressController"
	        })
	        .state("adderss_edit",{
	        	url: "/adderss_edit/:id",
	        	templateUrl: 'modules/order/adderss_edit.html',
	        	controller:"adderssEditController"
	        })
	        
	        .state("sdandard_dynamic",{//标准动态
	        	url:"/sdandard_dynamic/:id",
	        	templateUrl:'modules/stantard/standard_dynamic.html',
	        	controller:"standardDynamicController"
	        })
            
	        .state("server_agreement",{//服务协议
	        	url:"/server_agreement",
	        	templateUrl:'modules/member/server_agreement.html',
	        	controller : "memberController"
	        })
	        .state("member_register",{ //会员注册
	        	url: "/member_register",
	        	templateUrl: 'modules/member/member_register.html',
	        	controller: "memberController"
	        })
	        
	        .state("forget_password",{ //忘记密码
	        	url:"/forget_password",
	        	templateUrl : 'modules/member/forget_password.html',
	        	controller : "memberController"
	        })
	        
	        .state("forget_password_success",{ //忘记密码验证成功
	        	url:"/forget_password_success/:userName",
	        	templateUrl : 'modules/member/forget_password_success.html',
	        	controller : "memberController"
	        })
	        
	        .state('standard_conllection', { //标准收藏
	        	url:"/standard_conllection",
	        	templateUrl : 'modules/stantard/standard_collection.html',
	        	controller : 'conllectionController'
	        })
	        
	        .state('register_information', { //个人中心
	        	url:"/register_information",
	        	templateUrl : 'modules/member/register_information.html',
	        	controller : 'memberInfoController'
	        })
	        
	        .state('standard_info', { //标准详情
	        	url:"/standard_info/:id",
	        	templateUrl : '/modules/stantard/standard_info.html',
	        	controller : 'standardInfoController'
	        })
	        
	        .state('standard_track', { //标准跟踪
	        	url:'/standard_track',
	        	templateUrl : 'modules/stantard/standard_track.html',
	        	controller : 'standardTrackController'
	        })
	        
	        .state('track_history', { //跟踪历史
	        	url:'/track_history',
	        	templateUrl : 'modules/stantard/track_history.html',
	        	controller : 'trackHistoryController'
	        })
	        
	        .state('update_password', {
	        	url:'/update_password',
	        	templateUrl : 'modules/member/update_password.html',
	        	controller : 'memberInfoController'
	        })
	        
	        .state('advanced_search', { 
	        	url:"/advanced_search",
	        	templateUrl : 'modules/search/advanced_search.html',
	        	controller : 'advancedSearchController'
	        })
	        
	        .state('class_search', {
	        	url:"/class_search",
	        	templateUrl : 'modules/search/class_search.html',
	        	controller : 'classSearchController'
	        })
	        
	        .state('list_search', {
	        	url:"/list_search/:keyWord/:standardId/:icsNum/:ccsNumJson/:adopt/:standardSort/:pubYearBegin/:pubYearEnd/:standardStatus",
	        	params:{advanchList:null,searchHistoryList:null},
	        	templateUrl : 'modules/search/search.html',
	        	controller : 'listSearchController'
	        })
	        
	        .state('simple_search_result', {
	        	url:"/simple_search_result/:condation",
	        	params:{searchHistoryList:null},
	        	templateUrl : 'modules/search/simple_search_result.html',
	        	controller : 'mainSearchController'
	        })
	        
	        
	        .state('standard_validity_confirm', {
	        	url:"/standard_validity_confirm",
	        	templateUrl : 'modules/stantard/standard_validity_confirm.html',
	        	controller : 'standardValidityController'
	        })
	        
	        .state('success_list', {
	        	url:"/success_list/:code1",
	        	params:{list:null},
	        	templateUrl : 'modules/stantard/success.html',
	        	controller : 'successValidityController'
	        })
	        
	        .state('fail_list', {
	        	url:"/fail_list/:code",
	        	params:{failList:null},
	        	templateUrl : 'modules/stantard/fail.html',
	        	controller : 'failValidityController'
	        })
    }])
    .constant('ngPaginationConfig', {
        //最多显示页数
        visiblePageCount: 10,
        preIcon: "«",
        lastIcon: "»"
    });
    