angular.module("myApp.directive", [])
  
    .directive("pager", ['$rootScope','ngPaginationConfig', "$http" ,function ($rootScope,ngPaginationConfig,$http) {
        return {
            link: function (scope, element, attrs) {
            	//显示分页图标数
            	var visiblePageCount = angular.isDefined($rootScope.visiblePageCount) ? $rootScope.visiblePageCount : ngPaginationConfig.visiblePageCount;
                //上一页图标样式
                scope.preIcon = angular.isDefined(attrs.preIcon) ? attrs.preIcon : ngPaginationConfig.preIcon;
                //下一页图标样式
                scope.lastIcon = angular.isDefined(attrs.lastIcon) ? attrs.lastIcon : ngPaginationConfig.lastIcon;
                //当前页数
                scope.currentPage = 1;
//                visiblePageCount = parseInt(visiblePageCount);
                scope.pageChange = function (page) {
                	if(page == "..."){
                		return ;
                	}
            		if (page < 1) {
            			scope.currentPage = 1;
            		} else if(page > scope.pageCount) {
            			scope.currentPage = scope.pageCount;
            		} else {
            			scope.currentPage = page
            		}
                }

                function build() {
                    var minPage, 
                        maxPage,
                        cPage;

                    scope.pagenums = [];

                    if (scope.pageCount === 0) {
                        return;
                    }

                    if (scope.currentPage > scope.pageCount) {
                        scope.currentPage = scope.pageCount;
                    }

                    if (scope.pageCount <= visiblePageCount) {
                    	minPage = 1;
                    	maxPage = scope.pageCount;
                    	for (; minPage <= maxPage; minPage++) {
                            scope.pagenums.push(minPage);
                        }
                    } else {
                    	cPage = Math.ceil(visiblePageCount / 2);
                    	minPage = Math.max(scope.currentPage - cPage + 1, 1);
                    	maxPage = Math.min(minPage + visiblePageCount - 1, scope.pageCount);
                    	middlePage = Math.ceil((minPage + maxPage) / 2);
                    	if(scope.currentPage <= cPage){
                    		for (; minPage <= maxPage - 2 ; minPage++) {
                                scope.pagenums.push(minPage);
                            }
                    		scope.pagenums.push("...");
                    		scope.pagenums.push(scope.pageCount);
                    	}else if(scope.currentPage + cPage >= scope.pageCount){
                    		scope.pagenums.push(1);
                    		scope.pagenums.push("...");
                    		var count = scope.pageCount - visiblePageCount + 2;
                    		for (; count <= scope.pageCount; count++) {
                                scope.pagenums.push(count);
                            }
                    	}else{
                    		scope.pagenums.push(1);
                    		scope.pagenums.push("...");
                    		var count = scope.currentPage - 2;
                    		for (; count <= scope.currentPage +3 ; count++) {
                                scope.pagenums.push(count);
                            }
                    		scope.pagenums.push("...");
                    		scope.pagenums.push(scope.pageCount);
                    	}
                    }
                    
                }

                scope.$watch('currentPage', function (a, b) {
                    if (a !== b ) {
                        build();
                        scope.onPageChange();
                    }
                });

                scope.$watch('pageCount', function (a, b) {
                    if (!!a) {
                        build();
                    }
                });

            },
            replace: true,
            restrict: "E",
            scope: {
                pageCount: '=',
                currentPage: '=',
                totalRow: '=',
                onPageChange: '&'
            },
            templateUrl: '/modules/common/pager.html'
        }
    }])
    
    
    .directive('menuTree',['$timeout', '$parse',"$rootScope", function ($timeout, $parse,$rootScope) {
	    return {
	        restrict: 'ECAM',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                $timeout(function () {
	                    scope.$emit('ngRepeatFinished3'); //事件通知
                        var fun = scope.$eval(attr.menusList);
                        if(fun && typeof(fun)=='function'){  
                            fun();  //回调函数
                        }  
	                });
	            }
	        },
	        controller:["$scope",function($scope){
	        	$scope.$on('ngRepeatFinished3', function(ngRepeatFinishedEvent) {
            	    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
            	    $('.tree li.parent_li > span').on('click', function (e) {
            	    	var brother = $(this).parent('li.parent_li').find(' > a');
            	        var children = $(this).parent('li.parent_li').find(' > ul > li');
            	        if (children.is(":visible")) {
            	        	brother.hide('fast');
            	            children.hide('fast');
            	            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
            	        } else {
            	        	brother.show('fast');
            	            children.show('fast');
            	            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
            	        }
            	        e.stopPropagation();
            	    })
	        	})
	        }]
	    }
	}])
    .directive('advan',['$timeout', '$parse',"$rootScope", function ($timeout, $parse,$rootScope) {
	    return {
	        restrict: 'ECAM',
	        scope: false,
	        link: function (scope, element, attr) {
	            if (scope.$last != true) {
	            	$('.loadHide').hide();
	            } else {
	            	$('.loadHide').show();
	            }
	        }
	    }
	}])
