
/**
 * 
 * @param msg 提示的信息
 * @param life 存活时间 毫秒
 * @param theme 皮肤样式 warning、success、info、error
 */
$msg=function(msg,life,theme){
  $.jGrowl.defaults.closerTemplate = "<div class='"+theme+"'>[ 关闭全部 ]</div>";
  $.jGrowl(msg,{
	  header: '提示',
	  speed:'fast',
	  life:life,
	  theme:theme
  });
}