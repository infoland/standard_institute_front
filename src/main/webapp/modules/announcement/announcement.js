 appController.controller('announcementController',  ["$scope","$rootScope","$stateParams","$state","announcementService",function ($scope,$rootScope,$stateParams,$state,announcementService) {
	/**
	 * 通知公告数据列表展示
	 */
	announcementService.list().success(function (data) {
		$scope.channalList = data.list;
	})
	
	
     announcementService.findAllContext({id:$stateParams.id}).success(function (data) {
    	console.log(data);
     	$scope.pageList = data;
     	$scope.pageCount = data.totalPage;
 		$scope.totalRow = data.totalRow;
     })
     
     
     $scope.onPageChange = function () {
		announcementService.findAllContext({pageNumber: $scope.currentPage,id:$stateParams.id}).success(function(data){
			$scope.pageList = data
			$scope.pageCount = data.totalPage;
	 		$scope.totalRow = data.totalRow;
		});
	};
     
     /**
      * 跳转文章详情页面
      */
     $scope.goItem = function(channalId,id){
			$state.go('announcement_item',{channalId:channalId,id:id})
	 }
     
     
     
     
	}])
	
	.controller('announcementItemController',  ["$scope","$rootScope","$stateParams","$state","announcementService",function ($scope,$rootScope,$stateParams,$state,announcementService) {
		
		$scope.goAnnouncementList = function(){
			$state.go("announcement_list",{id:$stateParams.channalId});
		}
		/**
		 * 通知公告数据列表展示
		 */
		announcementService.list().success(function (data) {
        	$scope.channalList = data.list;
        })
        
		/**
		 * 文章详情
		 */
        announcementService.contextDetail($stateParams.channalId,$stateParams.id).success(function (data) {
        	$scope.c=data
        	console.log($scope.c)
        })
	
	}])
appService.service('announcementService', ['$http', function ($http) {
    return {
    	list:function () {
            return $http.post('/channal/findAllChannal');
       },
       findAllContext:function (id) {
           return $http.post('/context/findAllContext',id);
       },
       contextDetail:function (channalId,id) {
           return $http.post('/context/contextDetail/'+channalId+"-"+id);
       },
       titleList:function () {
           return $http.post('/context/titleList');
       },
       contextLastMonth:function () {
           return $http.post('/context/contextLastMonth');
       },
    }
}])