appController.controller('MainController', ['$interval','$scope', '$rootScope', '$state', '$stateParams', '$timeout', '$cookieStore', 'MainService', 'announcementService', 'memberService',"standardService","standardDynamicService",function ($interval,$scope, $rootScope, $state, $stateParams, $timeout, $cookieStore, MainService,announcementService, memberService,standardService,standardDynamicService) {
	
	setInterval(function(){
		$(".apple").find("ul").animate({  
			marginTop : "-18px"  
		},1500,function(){ 
			$(this).css({marginTop : "0px"}).find("li:first").appendTo(this);  
		})
	},3000);
	
	
	//init alert box
    $('.cd-popup4').on('click', function(event){
        if( $(event.target).is('.cd-popup-close4') || $(event.target).is('.cd-popup4') ) {
            event.preventDefault();
            $(this).removeClass('is-visible4');
        }
    });
    //ESC关闭
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.cd-popup4').removeClass('is-visible4');
        }
    });
    //加载默认关闭
    c.$closeLoad();
    
    c.$load();
    
    
    /**
     * 查询文章标题
     */
    announcementService.titleList().success(function(data){
    	$scope.datasetData = data.object;
    })
    
    /**
     * 首页公告跳转公告详情
     */
    $scope.goAnnouncementItem = function(channalId,id){
    	$state.go("announcement_item",{channalId:channalId,id:id});
    }
    /**
	 * 加载页面判断用户是否登陆
	 */
    $scope.chanList = function(){
    	MainService.list().success(function (data) {
    		$scope.channalList = data.list;
        	memberService.getSessionUserInfo().success(function(data){
        		if(data.success){
        			//遍历导航列表
        			angular.forEach($scope.channalList, function(data, index, arr){
        				if(data.c_name == '会员登录'){
        					data.c_name = '个人中心';
        					data.c_url = 'register_information';
        				}
        			});
        		}
        	});
        });
    }
    $scope.chanList();
    
    /**
     *  -------------------- 获取标准动态列表 START --------------------
     */
    
    standardDynamicService.list({standard_type:'NEW_PUBLISH'}).success(function(data){
    	if(data.success){
    		$scope.newPublishList = data.object.list;
    	}else{
    		$scope.newPublishList = {};
    	}
    	
    });
    standardDynamicService.list({standard_type:'NEW_WITHDRAW'}).success(function(data){
    	if(data.success){
    		$scope.newWithdrawList = data.object.list;
    	}else{
    		$scope.newWithdrawList = {};
    	}
    });
    standardDynamicService.list({standard_type:'NEW_IMPLEMEWNT'}).success(function(data){
    	if(data.success){
    		$scope.newImplemewntList = data.object.list;
    	}else{
    		$scope.newImplemewntList = {};
    	}
    	c.$closeLoad();
    });
    
    /**
     *  -------------------- 获取标准动态列表 END --------------------
     */
	
    /**
     * 点击更新验证码
     */
	$scope.changeCaptcha = function(){
		var timestamp = (new Date()).valueOf();
        var imageSrc = $("#captchaImage").attr("src");
        if (imageSrc.indexOf("?") >= 0) {
            imageSrc = imageSrc.substring(0, imageSrc.indexOf("?"));
        }
        imageSrc = imageSrc + "?timestamp=" + timestamp + "&width=100&height=45&fontsize=35";
        $("#captchaImage").attr("src", imageSrc);
	}
	
	/**
	 *  点击导航判断是页面跳转还是弹出登陆框
	 */
	$scope.loginOrUrl = function(channal){
		if(channal.c_name == '会员登录'){
			if(remberAccount == "true"){
				$scope.userName = userName;
				$scope.password = password;
				$scope.remberAccount = true;
			}else{
				$scope.userName = undefined;
				$scope.password = undefined;
				$scope.remberAccount = undefined;
			}
			$scope.loginCaptcha = undefined;
			event.preventDefault();
            $('.cd-popup4').addClass('is-visible4');
		}else {
			var index = channal.c_url.indexOf('/');
			if(channal.c_url.indexOf('/') != -1){
				$state.go(channal.c_url.substring(0, index), {id:channal.c_url.substring(index+1)});
			}else{
				$state.go(channal.c_url);
			}
		}
	}
	
    /**
	 * 会员登陆
	 */
    $scope.login = function(){
	//勾选记住登陆
    	c.$load();
		if ($scope.remberAccount) {
			$cookieStore.put("remberAccount", "true", { expires: 7 }); //存储一个带7天期限的cookie
			$cookieStore.put("userName", $scope.userName, { expires: 7 });
			$cookieStore.put("password", $scope.password, { expires: 7 });
	    } else {
	    	$cookieStore.put("remberAccount", "false", { expire: -1 });
	    	$cookieStore.put("userName", "", { expires: -1 });
	    	$cookieStore.put("password", "", { expires: -1 });
	    }
		memberService.login($scope.userName, $scope.password, $scope.loginCaptcha).success(function(data){
			//刷新验证码
			$scope.changeCaptcha();
			if(data.success){
				$scope.chanList();
				c.$msg(data.message, 1000, 'success');
				$('.cd-popup4').removeClass('is-visible4');
			}else{
				 c.$msg(data.message, 1000, 'error');
			}
			c.$closeLoad();
		});
	}
    var userName = $cookieStore.get("userName");
	var password = $cookieStore.get("password");
	var remberAccount = $cookieStore.get("remberAccount");
	
	
	/**
	 * 主页搜索--简单搜索
	 */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];//将搜索历史添加到cookie
	
	$scope.search = function(){
		if($scope.condation){
			c.$load();
			arrs.unshift($scope.condation);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
			$state.go("simple_search_result",{condation:$scope.condation,searchHistoryList:arrs})
		}else{
			c.$msg("请输入查询条件",1000,"error");
			return false;
		}
	}
	
	/**
	 * 跳转购物车
	 */
	$scope.goCart = function(){
		memberService.getSessionUserInfo().success(function(data){
			//遍历导航列表
			angular.forEach($scope.channalList, function(result, index, arr){
				if(result.c_name == '会员登录'){
					c.$msg("请先登录再进行查看",1000,"error");
					return false;
				}else{
					if(data.success && result.c_name == '个人中心'){
						$state.go("cart");
					}
				}
			});
    	});
	}
	MainService.copyRight().success(function(data){
		$scope.copy = data
	})
	
}])
	
.controller('mainSearchController',  ["$window","$cookieStore","$scope","$rootScope","$stateParams","$state","MainService","standardService","memberService","conllectionService","standardTrackService","standardInfoService","cartService",function ($window,$cookieStore,$scope,$rootScope,$stateParams,$state,MainService,standardService,memberService,conllectionService,standardTrackService,standardInfoService,cartService) {
	/**
	 * 获取标准动态
	 */
	standardService.getStandardStatus().success(function(data){
		console.log(0);
		$scope.standardList = data
		$scope.current=data[0].d_value;
		$scope.total = data[2].d_value;
		$scope.abolish = data[1].d_value;
		
	})
	
	//判断是否是pc访问
	function isPC() {
	   var userAgentInfo = navigator.userAgent;
	   var Agents = ["Android", "iPhone",
	      "SymbianOS", "Windows Phone",
	      "iPad", "iPod"];
	   var flag = true;
	   for (var v = 0; v < Agents.length; v++) {
	      if (userAgentInfo.indexOf(Agents[v]) > 0) {
	         flag = false;
	         break;
	      }
	   }
	   return flag;
	}
	//判断是否是Android访问
	function isAndroid(){
		var u = navigator.userAgent, 
			app = navigator.appVersion;
		var flag = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
		return flag;
	}
	
	$scope.getElec = function(identifier,s){
		console.log(s)
		console.log(
				'http://bzyjs.aft100.com/dynamic/getElecFileByBefore?code=' + 
				s.standardNo + '&filename=' + s.standardNo.replace(" ","").replace("/","-") + 
				'.pdf'
		)
		standardInfoService.getStandardElec({code:identifier}).success(function(data){
			if(data.success){
				$scope.elec = data.object[0];
				console.log($scope.elec)
			}else{
				c.$msg(data.message, 1000, 'error');
			}
			if(data.success){
				//http://bzyjs.aft100.com/dynamic/getElecFileByBefore?code=GB/T%201112-2012&filename=GB_T%201112-2012_3125.pdf
				var url = 'http://bzyjs.aft100.com/dynamic/getElecFileByBefore?code='+data.object[0].a100+"&filename="+data.object[0].fileName;
				if(isPC() || isAndroid()){
					console.log("Android")
					window.open(url);
				}else{
					location.href = url;
				}
			}else{
				c.$msg(data.message, 1000, 'error');
			}
		})
	}
	
	/**
	 * 在结果中查询
	 */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];
	$scope.seachResult = function(){
		c.$load();
		if($scope.name){
			arrs.unshift($scope.name);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		$scope.searchHistory = angular.fromJson($cookieStore.get("searchHistory"));
			MainService.simpleSearch({pageNumber:$scope.currentPage,condation:$stateParams.condation}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				c.$closeLoad();
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			
			/**
			 * 获取热门标准
			 */
			console.log(data.object)
			if(data.object!=null && data.object[1]){
				standardService.getConcernStandard(data.object[1].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
				})
			}
		})
	}
	$scope.searchHistory = angular.fromJson($cookieStore.get("searchHistory"));
	
	/**
	 * 翻页
	 */
	$scope.onPageChange = function () {
		c.$load();
		MainService.simpleSearch({pageNumber:$scope.currentPage,condation:$stateParams.condation,orderBy:name,ordering:sort}).success(function(data){
			console.log($scope.currentPage)
			var t = angular.fromJson(data.object);
			if(data.success){
				c.$closeLoad();
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			/**
			 * 获取热门标准
			 */
			if(data.object!=null && t[0]){
				standardService.getConcernStandard(t[0].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
				})
			}
		})
    };
    
    /**
     * 获取热词数据
     */
    standardService.hotWordsList().success(function(data){
		$scope.hotWordsList = data;
	});
    
    /**
     * 清除检索历史
     */
    $scope.removeSearchHistory = function(){
    	$scope.searchHistory = "";
    	$cookieStore.remove("searchHistory");
    }
    
    /**
     * 清除浏览历史
     */
    $scope.removeBrowseHistory = function(){
    	$scope.browseHistory="";
    	$cookieStore.remove("sName");
    }
    
    /**
     * 浏览历史
     */
    $scope.sName = $cookieStore.get("sName") ? angular.fromJson($cookieStore.get("sName")) : [];
    $scope.goStandardInfo = function(id,b){
		 $scope.sName.unshift(b);
		 console.log($scope.sName);
		 $cookieStore.put("sName",angular.toJson($scope.sName,true));
		 $state.go("standard_info",{id:id});
	}
    $scope.browseHistory = $scope.sName;
    console.log("浏览历史:"+$cookieStore.get("sName"));
    
    
    $scope.goCurrent = function(){
    	if($("#s1").is(":checked")){
    		standardService.advencedSearch({standardStatus:$scope.current}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    $scope.goTotal = function(){
    	if($("#s2").is(":checked")){
    		standardService.advencedSearch({standardStatus:$scope.total}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    	
    }
    
    $scope.goAbolish = function(){
    	if($("#s3").is(":checked")){
    		standardService.advencedSearch({standardStatus:$scope.abolish}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    
    $scope.getStandardElec = function(a001){
    	$scope.count = 1;
    	memberService.getSessionUserInfo().success(function(dataUser){
			if(dataUser.success){
				$scope.title = "立即购买";
				$('.cd-popup3').addClass('is-visible3');
				standardInfoService.getStandardElec({code:a001}).success(function(data){
					if(data.success){
						$scope.elec = data.object[0];
					}else{
						c.$msg(data.message, 1000, 'error');
					}
					/*if(data.success){
						standardInfoService.getElecFileByBefore({code : data.object[0].standardNo, filename : data.object[0].fileName}).success(function(dataElec){
						});
					}else{
						c.$msg(data.message, 1000, 'error');
					}*/
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		})
	}

    /**
	 * 加入购物车
	 */
	$scope.getCartElec = function(a001){
		memberService.getSessionUserInfo().success(function(dataUser){
			if(dataUser.success){
				$scope.title = "加入购物车";
				$('.cd-popup3').addClass('is-visible3');
				standardInfoService.getStandardElec({code:a001}).success(function(data){
					if(data.success){
						$scope.elec = data.object[0];
					}else{
						c.$msg(data.message, 1000, 'error');
					}
				})
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		})
	}
    
	/**
     * 立即购买
     */
    var array = [];
    $scope.instantaneousBuy = function(elec,count){
    	if(elec){
    		if($scope.title == "立即购买"){
    			elec.count = count
    			array.push(elec);
    			$state.go("shop_process",{cartList:array});
    		}else{
    			memberService.getSessionUserInfo().success(function(data){
    				if(data.success){
    					elec.count = count;
    					standardService.addCart(elec).success(function(data){
    						console.log(elec.count);
    						if(data.success){
    							$('.cd-popup3').removeClass('is-visible3');
    							c.$msg(data.message,1000,"success");
    						}else{
    							c.$msg(data.message,1000,"error");
    						}
    					})
    				}else{
    					$('.cd-popup4').addClass('is-visible4');
    				}
    			});
    		}
    	}else{
    		c.$msg("没有获得当前标准号相关信息,无法加入购物车",1000,"error");
    	}
    } 
    
    
    /**
	 * 添加一个数量
	 */
    $scope.count = 1;//默认电子文献数量为1
	$scope.plus = function(elec) {
		++$scope.count;
	}
	
	/**
	 * 减少一个数量
	 */
	$scope.minus = function(elec) {
		--$scope.count;
	}
    
    /**
	 * 收藏方法 standardId动态标识符 standardName动态名
	 */
	$scope.collection = function(standardId, standardName){
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				c.$load();
				//如果用户以及登陆执行收藏方法 data.object.id 用户id
				conllectionService.save({id : data.object.id, standard_id:standardId}).success(function(dataCollection){
					if(dataCollection.success){
						c.$threeBtnConfirm("您已经将《" + standardName + "》收藏了", "查看标准收藏列表", function(){
							$state.go('standard_conllection');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	/**
	 * 跟踪方法 standardId动态标识符 standardName动态名
	 */
	$scope.tracking = function(standardId, standardName){
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				c.$load();
				//如果用户以登陆执行跟踪方法 
				standardTrackService.saveTrackingInfo(standardId).success(function(dataCollection){
					if(dataCollection.success){
						c.$threeBtnConfirm(dataCollection.message, "查看标准跟踪列表", function(){
							$state.go('standard_track');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	
	/**
	 * 简单检索结果页
	 */
	MainService.simpleSearch({pageNumber:$scope.currentPage,condation:$stateParams.condation}).success(function(data){
		var t = angular.fromJson(data.object);
		if(data.success){
			$scope.pageCount = t[0].totalPage;
			$scope.totalRow = t[0].totalRow;
			t.splice(0,1);
			$scope.mainSimpleSearchList = t;
		}else{
			c.$msg(data.message,1000,"error");
		}
		c.$closeLoad();
		/**
		 * 获取热门标准
		 */
		if(angular.fromJson(data.object)!=null && angular.fromJson(data.object)[1]){
			standardService.getConcernStandard(angular.fromJson(data.object)[1].identifier).success(function(result){
				$scope.popStandardList = angular.fromJson(result.object);
				console.log($scope.popStandardList)
			})
		}
	})
	
	
	var i =0;
	var sort = "";
	var name = "";
	/**
	 * 按照文献号进行排序
	 */
    $scope.hitStandardCode = function(){
    	name = "文献号";
    	if(i++%2==0){
    		sort = "desc";
    		console.log(i);
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		console.log(i);
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    
    /**
     * 按照标准名称进行排序
     */
    var k = 0;
    $scope.hitStandardName = function(){
    	name = "中文题名";
    	if(k++%2==0){
    		sort = "desc";
    		console.log(k);
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    /**
     * 按照发布日期进行排序
     */
    var t = 0;
    $scope.hitReleaseDate = function(){
    	name = "发布日期";
    	if(t++%2==0){
    		sort = "desc";
    		console.log(t);
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    /**
     * 按照实施日期进行排序
     */
    var n = 0;
    $scope.hitImeplementDate = function(){
    	name = "实施日期";
    	if(n++%2==0){
    		sort = "desc";
    		console.log(n);
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		MainService.simpleSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.mainSimpleSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    
    $scope.getHotWords = function(name){
		c.$load();
		MainService.simpleSearch({pageNumber:$scope.currentPage,condation:name}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			c.$closeLoad();
			/**
			 * 获取热门标准
			 */
			if(angular.fromJson(data.object)!=null && angular.fromJson(data.object)[1]){
				standardService.getConcernStandard(angular.fromJson(data.object)[1].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
					console.log($scope.popStandardList)
				})
			}
		})
	}
	
	$scope.getPop = function(code){
		c.$load();
		MainService.simpleSearch({pageNumber:$scope.currentPage,condation:code}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			c.$closeLoad();
			/**
			 * 获取热门标准
			 */
			if(angular.fromJson(data.object)!=null && angular.fromJson(data.object)[1]){
				standardService.getConcernStandard(angular.fromJson(data.object)[1].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
					console.log($scope.popStandardList)
				})
			}
		})
	}
	
	$scope.getSearch = function(historyname){
		c.$load();
		MainService.simpleSearch({pageNumber:$scope.currentPage,condation:historyname}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			c.$closeLoad();
			/**
			 * 获取热门标准
			 */
			if(angular.fromJson(data.object)!=null && angular.fromJson(data.object)[1]){
				standardService.getConcernStandard(angular.fromJson(data.object)[1].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
					console.log($scope.popStandardList)
				})
			}
		})
	}
	
	$scope.getBrowse = function(browse){
		c.$load();
		MainService.simpleSearch({pageNumber:$scope.currentPage,condation:browse}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.mainSimpleSearchList = t;
			}else{
				c.$msg(data.message,1000,"error");
			}
			c.$closeLoad();
			/**
			 * 获取热门标准
			 */
			if(angular.fromJson(data.object)!=null && angular.fromJson(data.object)[1]){
				standardService.getConcernStandard(angular.fromJson(data.object)[1].identifier).success(function(result){
					$scope.popStandardList = angular.fromJson(result.object);
					console.log($scope.popStandardList)
				})
			}
		})
	}
    
}])
appService.service('MainService', ['$http', function ($http) {
    return {
    	list:function () {
            return $http.post('/channal/findAllChannal');
        },
        simpleSearch:function (query) {
        	console.log("------------"+"service");
            return $http.post('/search/simpleSearch',query);
        },
        findCartOne:function (code) {
            return $http.post('/cart/findOneCart',{code:code});
        },
        updateOrder : function (code) {
            return $http.post('/order/updateOrder',code);
        },
        copyRight : function () {
            return $http.post('/copyRight/findAll');
        },
    }
}])
