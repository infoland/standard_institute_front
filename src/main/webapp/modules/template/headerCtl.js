appController.controller('headerCtl', ['$scope', '$state', '$cookieStore', '$location', 'MainService', 'memberService', function($scope, $state, $cookieStore, $location, MainService, memberService){
	//该参数判断是否点击购物车未登录跳转
	var isGoToCart = false;
	//init alert box
    $('.cd-popup4').on('click', function(event){
        if( $(event.target).is('.cd-popup-close4') || $(event.target).is('.cd-popup4') ) {
            event.preventDefault();
            $(this).removeClass('is-visible4');
        }
        isGoToCart = false;
    });
    //ESC关闭
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.cd-popup4').removeClass('is-visible4');
        }
    });
    
    /**
     * 导航样式动态添加
     */
    $scope.url = $location.url().substring(1);
    var index = $scope.url.lastIndexOf('/');
    if(index != -1){
    	$scope.url = $scope.url.substring(0,index);
    }
    /**
     * 跳转购物车
     */
    $scope.goToCart = function(){
    	$state.go("cart");
    /*	memberService.getSessionUserInfo().success(function(data){
    		if(data.success){
    			$state.go("cart");
    		}else{
    			isGoToCart = true;
    			$('.cd-popup4').addClass('is-visible4');
    		}
    	});*/
    }
    
    /**
	 * 加载页面判断用户是否登陆
	 */
    $scope.chanList = function(){
    	MainService.list().success(function (data) {
    		$scope.channalList = data.list;
        	memberService.getSessionUserInfo().success(function(data){
        		if(data.success){
        			//遍历导航列表
        			angular.forEach($scope.channalList, function(data, index, arr){
        				if(data.c_name == '会员登录'){
        					data.c_name = '个人中心';
        					data.c_url = 'register_information';
        				}
        			});
        		}
        	});
        });
    }
    $scope.chanList();	
	
	
	/**
     * 点击更新验证码
     */
	$scope.changeCaptchaT = function(){
		var timestamp = (new Date()).valueOf();
        var imageSrc = $("#captchaImage").attr("src");
        if (imageSrc.indexOf("?") >= 0) {
            imageSrc = imageSrc.substring(0, imageSrc.indexOf("?"));
        }
        imageSrc = imageSrc + "?timestamp=" + timestamp + "&width=100&height=45&fontsize=35";
        $("#captchaImage").attr("src", imageSrc);
	}
	
	/**
	 *  点击导航判断是页面跳转还是弹出登陆框
	 */
	$scope.loginOrUrl = function(channal){
		if(channal.c_name == '会员登录'){
			if(remberAccount == "true"){
				$scope.userName = userName;
				$scope.password = password;
				$scope.remberAccount = true;
			}else{
				$scope.userName = undefined;
				$scope.password = undefined;
				$scope.remberAccount = undefined;
			}
			$scope.loginCaptcha = undefined;
			event.preventDefault();
            $('.cd-popup4').addClass('is-visible4');
		}else {
			var index = channal.c_url.indexOf('/');
			if(channal.c_url.indexOf('/') != -1){
				$state.go(channal.c_url.substring(0, index), {id:channal.c_url.substring(index+1)});
			}else{
				$state.go(channal.c_url);
			}
		}
	}
	
    /**
	 * 会员登陆
	 */
    $scope.loginT = function(){
		//勾选记住登陆
		c.$load();
		if ($scope.remberAccount) {
			$cookieStore.put("remberAccount", "true", { expires: 7 }); //存储一个带7天期限的cookie
			$cookieStore.put("userName", $scope.userName, { expires: 7 });
			$cookieStore.put("password", $scope.password, { expires: 7 });
		} else {
			$cookieStore.put("remberAccount", "false", { expire: -1 });
			$cookieStore.put("userName", "", { expires: -1 });
			$cookieStore.put("password", "", { expires: -1 });
		}
		memberService.login($scope.userName, $scope.password, $scope.loginCaptcha).success(function(data){
			//刷新验证码
			$scope.changeCaptchaT();
			if(data.success){
				$scope.chanList();	
				c.$msg(data.message, 1000, 'success');
				$('.cd-popup4').removeClass('is-visible4');
				if(isGoToCart){
					$state.go("cart");
				}
			}else{
				 c.$msg(data.message, 1000, 'error');
			}
			c.$closeLoad();
		});
	}
    var userName = $cookieStore.get("userName");
	var password = $cookieStore.get("password");
	var remberAccount = $cookieStore.get("remberAccount");
	
	
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];//将搜索历史添加到cookie
	$scope.search = function(){
		if($scope.condation){
			c.$load();
			arrs.unshift($scope.condation);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
			$state.go("simple_search_result",{condation:$scope.condation,searchHistoryList:arrs})
		}else{
			c.$msg("请输入查询条件",1000,"error");
			return false;
		}
	}
}])























