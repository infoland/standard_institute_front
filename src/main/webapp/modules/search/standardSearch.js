 appController.controller('advancedSearchController',  ["$cookieStore","$scope","$rootScope","$stateParams","$state","standardService","$rootScope","standardInfoService",function ($cookieStore,$scope,$rootScope,$stateParams,$state,standardService,$rootScope,standardInfoService) {
	/**
	 * 分类检索--获取标准品种
	 */
	standardService.getStandardSortList().success(function(data){
		console.log(data);
		if(data.object){
			$scope.standardSortList = data.object;
		}
	})

	/**
	 * 获取国际标准分类号
	 */
	standardService.findInterstandardClasses().success(function(data){
		$scope.icsList = data
	})

	/**
	 * 获取中国标准分类号
	 */
	standardService.findAll().success(function(data){
		$scope.ccsList = data
	})
	
	
	 /**
	 * 获取标准动态
	 */
	standardService.getStandardStatus().success(function(data){
		$scope.standardList = data
		$scope.current=data[0].d_value;
		$scope.total = data[2].d_value;
		$scope.abolish = data[1].d_value;
		
	})
	
	$scope.chooseAdvanced = function(){
		$("#advancedSearch").modal('show')
		$(".modal-backdrop").hide();
	}
	
	$scope.chooseChinaClass = function(){
		$("#chinaStandard").modal('show')
		$(".modal-backdrop").hide();
	}
	
	$scope.chooseClass = function(){
		$("#standardClass").modal('show')
		$(".modal-backdrop").hide();
	}
	
	
	 $scope.close = function(){
		  $("#advancedSearch").modal('hide')
	  }
	  
	  $scope.closeT = function(){
		  $("#chinaStandard").modal('hide');
	  }
	  
	  $scope.closeD = function(){
		  $("#standardClass").modal('hide');
	  }
	  

	/**
	 * 国家标准分类全选
	 */
	$scope.toggleCheckBox = function (id) {
	    $scope.ids = c.toggleCheckBox(id);
	}
	
	/**
	 * 中国标准分类全选
	 */
	$scope.toggleCheckBoxT = function (id) {
	    $scope.ids = c.toggleCheckBoxT(id);
	}
	
	/**
	 * 标准号分类全选
	 */
	$scope.toggleCheckBoxD = function (id) {
	    $scope.ids = c.toggleCheckBoxD(id);
	}
	
	/**
	 * 国家标准分类确定
	 */
	var s = "";
	$scope.sure = function(){
		$scope.icsNum=""
		for(var j = 0;j<$scope.icsList.length;j++){
			for(var i = 0;i<$scope.ids.length;i++){
				if($scope.icsList[j].id == $scope.ids[i]){
					$scope.icsNum += $scope.icsList[j].cName+" ";
					s += $scope.icsList[j].icsn+" ";
				}
			}
		}
		$("#advancedSearch").modal('hide')
	}
	
	/**
	 * 中国标准分类确定
	 */
	var cs = "";
	$scope.Csure = function(){
		$scope.ccsNumJson=""
		for(var j = 0;j<$scope.ccsList.length;j++){
			for(var i = 0;i<$scope.ids.length;i++){
				if($scope.ccsList[j].id == $scope.ids[i]){
					$scope.ccsNumJson += $scope.ccsList[j].cc+" ";
					cs += $scope.ccsList[j].cn+" ";
				}
			}
		}
		$("#chinaStandard").modal('hide')
	}
	
	
	/**
	 * 获取高级检索时间
	 */
	standardService.standardTimeList().success(function(data){
		$scope.pubYearBeginList = data;
		$scope.pubYearEndList = data;
	})
	/**
	 * 标准号分类确定
	 */
	$scope.Isure = function(){
		$scope.standardSort="";
		for(var i = 0;i<$scope.ids.length;i++){
	    	$scope.standardSort+=$scope.ids[i];
	    }
		$("#standardClass").modal('hide')
	}
	
	/**
     * 查询
     */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];//将搜索历史添加到cookie
    $scope.search = function(){
		if($scope.keyWord){
			arrs.unshift($scope.keyWord);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		if($scope.standardId){
			arrs.unshift($scope.standardId);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		
		if($scope.icsNum){
			arrs.unshift($scope.icsNum);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		if($scope.ccsNumJson){
			arrs.unshift($scope.ccsNumJson);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		if($scope.adopt){
			arrs.unshift($scope.adopt);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		if($scope.standardSort){
			arrs.unshift($scope.standardSort);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}
		standardService.advencedSearch({keyWord:$scope.keyWord,standardId:$scope.standardId,icsNum:s,
			ccsNumJson:cs,adopt:$scope.adopt,standardSort:$scope.standardSort,
			pubYearBegin:$scope.pubYearBegin,pubYearEnd:$scope.pubYearEnd,standardStatus:$scope.standardStatus}).success(function(data){
				var t = angular.fromJson(data.object);
				$state.go("list_search",{keyWord:$scope.keyWord == undefined ? "" :$scope.keyWord ,
				standardId:$scope.standardId == undefined ? "":$scope.standardId,
				icsNum:$scope.icsNum == undefined ? "":s,
				ccsNumJson:$scope.ccsNumJson == undefined ? "":cs,
				adopt:$scope.adopt == undefined ? "":$scope.adopt,
				standardSort:$scope.standardSort == undefined ? "":$scope.standardSort,
				pubYearBegin:$scope.pubYearBegin == undefined ? "":$scope.pubYearBegin,
				pubYearEnd:$scope.pubYearEnd == undefined ? "":$scope.pubYearEnd,
				standardStatus:$scope.standardStatus == undefined ? "":$scope.standardStatus,
				advanchList:angular.fromJson(data.object),
				searchHistoryList:arrs});
				console.log(angular.fromJson(data.object))
			})
    }
	
    /**
     * 重置
     */
    $scope.reset = function(){
    	$scope.keyWord="",
    	$scope.standardId="",
    	$scope.icsNum="",
    	$scope.ccsNumJson="",
    	$scope.adopt="",
    	$scope.standardSort="",
    	$scope.pubYearBegin="",
    	$scope.pubYearEnd="",
    	$scope.standardStatus=""
    }
    
    
    $scope.goClassSearch = function(){
		$state.go("class_search")
    }
}])

.controller('classSearchController',  ["$scope","$rootScope","$stateParams","$state","standardService","$cookieStore",function ($scope,$rootScope,$stateParams,$state,standardService,$cookieStore) {
	$scope.isShow_bz = "up";
	$scope.isShow_gj = "down";
	$scope.isShow_zg = "down";
	$scope.upDown = function(a){
		switch(a){
		case 1:
			if($scope.isShow_bz == "up"){
				$("#tab_bz").slideUp(500);
				$scope.isShow_bz = "down";
			}else{
				$scope.isShow_bz = "up";
				$scope.isShow_gj = "down";
				$scope.isShow_zg = "down";
				$("#tab_bz").slideDown(500);
				$("#tab_gj").slideUp(500);
				$("#tab_zg").slideUp(500);
			}
		  break;
		case 2:
			if($scope.isShow_gj == "up"){
				$("#tab_gj").slideUp(500);
				$scope.isShow_gj = "down";
			}else{
				$scope.isShow_bz = "down";
				$scope.isShow_gj = "up";
				$scope.isShow_zg = "down";
				$("#tab_bz").slideUp(500);
				$("#tab_gj").slideDown(500);
				$("#tab_zg").slideUp(500);
			}
		  break;
		default:
			if($scope.isShow_zg == "up"){
				$("#tab_zg").slideUp(500);
				$scope.isShow_zg = "down";
			}else{
				$scope.isShow_bz = "down";
				$scope.isShow_gj = "down";
				$scope.isShow_zg = "up";
				$("#tab_bz").slideUp(500);
				$("#tab_gj").slideUp(500);
				$("#tab_zg").slideDown(500);
			}
		}
	}
	/**
	 * 分类检索--获取标准品种
	 */
	standardService.getStandardSortList().success(function(data){
		var arrss = [];
		var arrs = [];
		for(var i = 0; i < angular.fromJson(data.object).length; i ++){
			arrs.push(angular.fromJson(data.object)[i]);
			if((i % 3 == 0 || i == angular.fromJson(data.object).length - 1) && i != 0){
				arrss.push(arrs);
				arrs = [];
			}
		}
		if(arrss[arrss.length-1].length < 3){
			for(var i = arrss[arrss.length-1].length-1; i < 3; i++){
				arrss[arrss.length-1].push({
					description:"",
					id:""
				});
			}
		}
		$scope.standardSortList = arrss;
	})

	/**
	 * 获取国际标准分类号
	 */
	standardService.findInterstandardClasses().success(function(data){
		var arrss = [];
		var arrs = new Array(3);
		var count = 0;
		for(var i = 0; i < data.length; i ++){
			arrs[count] = data[i];
			if(++count > 2){
				count = 0;
				arrss.push(arrs);
				arrs = new Array();
			}
		}
		if(arrss[arrss.length-1].length < 3){
			for(var i = arrss[arrss.length-1].length-1; i < 3; i++){
				arrss[arrss.length-1].push({
					cName:"",
					id:""
				});
			}
		}
		$scope.icsList = arrss
	})

	/**
	 * 获取中国标准分类号
	 */
	standardService.findAll().success(function(data){
		var arrss = [];
		var arrs = [];
		var count = 0;
		for(var i = 0; i < data.length; i ++){
			arrs[count] = data[i];
			if(++count > 2){
				count = 0;
				arrss.push(arrs);
				arrs = new Array();
			}
		}
		if(arrss[arrss.length-1].length < 3){
			for(var i = arrss[arrss.length-1].length-1; i < 3; i++){
				arrss[arrss.length-1].push({
					cc:"",
					prt:""
				});
			}
		}
		$scope.ccsList = arrss
	})
	
	/**
	 * 分类检索--中国标准号检索
	 */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];
	$scope.goCcs = function(prt,ccsName){
		arrs.unshift(ccsName);
		$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		standardService.advencedSearch({ccsNumJson:prt}).success(function(data){
			$state.go("list_search",{keyWord:$stateParams.keyWord == undefined ? "" :$stateParams.keyWord ,
									standardId:$stateParams.standardId == undefined ? "":$stateParams.standardId,
									icsNum:$stateParams.icsNum == undefined ? "":$stateParams.icsNum,
									ccsNumJson:prt,
									adopt:$stateParams.adopt == undefined ? "":$stateParams.adopt,
									standardSort:$stateParams.standardSort == undefined ? "":$stateParams.standardSort,
									pubYearBegin:$stateParams.pubYearBegin == undefined ? "":$stateParams.pubYearBegin,
									pubYearEnd:$stateParams.pubYearEnd == undefined ? "":$stateParams.pubYearEnd,
									standardStatus:$stateParams.standardStatus == undefined ? "":$stateParams.standardStatus})
			
				$scope.advanSearchList = angular.fromJson(data.object);
		})
	}
	
	/**
	 * 分类检索--国际标准号检索
	 */
	$scope.goIcs = function(id,icsName){
		arrs.unshift(icsName);
		$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		standardService.advencedSearch({icsNum:id}).success(function(data){
			$state.go("list_search",{keyWord:$stateParams.keyWord == undefined ? "" :$stateParams.keyWord ,
									standardId:$stateParams.standardId == undefined ? "":$stateParams.standardId,
									icsNum:id,
									ccsNumJson:$scope.ccsNumJson == undefined ? "":$scope.ccsNumJson,
									adopt:$stateParams.adopt == undefined ? "":$stateParams.adopt,
									standardSort:$stateParams.standardSort == undefined ? "":$stateParams.standardSort,
									pubYearBegin:$stateParams.pubYearBegin == undefined ? "":$stateParams.pubYearBegin,
									pubYearEnd:$stateParams.pubYearEnd == undefined ? "":$stateParams.pubYearEnd,
									standardStatus:$stateParams.standardStatus == undefined ? "":$stateParams.standardStatus})
			
				$scope.advanSearchList = angular.fromJson(data.object);
		})
	}
	
	/**
	 * 分类检索--标准号检索
	 */
	$scope.goResult = function(id,rName){
		arrs.unshift(rName);
		$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		standardService.advencedSearch({standardSort:id}).success(function(data){
			c.$load();
			$state.go("list_search",{keyWord:$stateParams.keyWord == undefined ? "" :$stateParams.keyWord ,
									standardId:$stateParams.standardId == undefined ? "":$stateParams.standardId,
									icsNum:$stateParams.icsNum == undefined ? "":$stateParams.icsNum,
									ccsNumJson:$scope.ccsNumJson == undefined ? "":$scope.ccsNumJson,
									adopt:$stateParams.adopt == undefined ? "":$stateParams.adopt,
									standardSort:id,
									pubYearBegin:$stateParams.pubYearBegin == undefined ? "":$stateParams.pubYearBegin,
									pubYearEnd:$stateParams.pubYearEnd == undefined ? "":$stateParams.pubYearEnd,
									standardStatus:$stateParams.standardStatus == undefined ? "":$stateParams.standardStatus})
			
				$scope.advanSearchList = angular.fromJson(data.object);
			if(data.success){
				c.$closeLoad();
			}
		})
	}
	$(".ng-scope").css("cursor","default");
}])

.controller('listSearchController',  ["standardTrackService","conllectionService","$cookieStore","$scope","$rootScope","$stateParams","$state","standardService","memberService","standardInfoService","$window",function (standardTrackService ,conllectionService,$cookieStore,$scope,$rootScope,$stateParams,$state,standardService,memberService,standardInfoService,$window) {
	/**
	 * 获取标准动态
	 */
	standardService.getStandardStatus().success(function(data){
		$scope.standardList = data
		$scope.current=data[0].d_value;
		$scope.total = data[2].d_value;
		$scope.abolish = data[1].d_value;
	})
	
	/**
	 * 获取热门标准
	 */
	if($stateParams.advanchList!=null && $stateParams.advanchList[1]){
		standardService.getConcernStandard({identifier:$stateParams.advanchList[1].identifier}).success(function(data){
			$scope.popStandardList = data;
			console.log(data);
			//加载完成，计数并关闭遮罩层
//			temp++;
//			close();
		})
	}
	
	standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
		ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
		pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:$stateParams.standardStatus}).success(function(data){
			var t = angular.fromJson(data.object);
			if(data.success){
				$scope.pageCount = t[0].totalPage;
				$scope.totalRow = t[0].totalRow;
				t.splice(0,1);
				$scope.advanSearchList = t;
				console.log(t);
			}else{
				c.$msg(data.message,1000,"error")
			}
	})
	/**
	 * 在结果中查询
	 */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];
	$scope.seachResult = function(){
		c.$load();
		if($scope.cName){
			arrs.unshift($scope.cName);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
		}else{
			$scope.advanSearchList = [];
			c.$closeLoad();
			return;
		}
		$scope.searchHistory = angular.fromJson($cookieStore.get("searchHistory"));
		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
			ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
			pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:$stateParams.standardStatus}).success(function(data){
				var t = angular.fromJson(data.object);
				if(data.success){
					c.$closeLoad();
					$scope.pageCount = t[0].totalPage;
					$scope.totalRow = t[0].totalRow;
					t.splice(0,1);
					$scope.advanSearchList = t;
					console.log(t);
				}else{
					c.$msg(data.message,1000,"error")
				}
		})
	}
		$scope.searchHistory = angular.fromJson($cookieStore.get("searchHistory"));
	/**
	 * 翻页
	 */
	var status = "";
    $scope.onPageChange = function () {
    	if($stateParams.standardStatus!="" && t==""){
    		status = $stateParams.standardStatus;
    	}else{
    		status = t;
    	}
    	if($("#s1").is(":checked") || $("#s2").is(":checked") || $("#s3").is(":checked")){
    		status  = t;
    		$stateParams.standardStatus = status
    	}
    	standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
    		ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
    		pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:status,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    	})
    };
    
    /**
	 * 加入购物车
	 */
    var boo = false;
	$scope.getCartElec = function(a001){
			memberService.getSessionUserInfo().success(function(dataUser){
				if(dataUser.success){
					$scope.title = "加入购物车";
					$('.cd-popup3').addClass('is-visible3');
					standardInfoService.getStandardElec({code:a001}).success(function(data){
						if(data.success){
							$scope.elec = data.object[0];
							console.log($scope.elec)
						}else{
							c.$msg(data.message, 1000, 'error');
						}
						/*if(data.success){
							standardInfoService.getElecFileByBefore({code : data.object[0].standardNo, filename : data.object[0].fileName}).success(function(dataElec){
							});
						}else{
							c.$msg(data.message, 1000, 'error');
						}*/
	    	if(boo){
	    		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
	    			ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
	    			pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:$stateParams.standardStatus}).success(function(data){
	    				var t = angular.fromJson(data.object);
	    				if(data.success){
	    					$scope.pageCount = t[0].totalPage;
	    					$scope.totalRow = t[0].totalRow;
	    					t.splice(0,1);
	    					$scope.advanSearchList = t;
	    					console.log(t);
	    				}else{
	    					c.$msg(data.message,1000,"error")
	    				}
	    		})
	    	}else{
	    		boo=true;
	    	}
	    });
	}else{
		$('.cd-popup4').addClass('is-visible4');
	}
   })
}
    /**
     * 获取热词数据
     */
    standardService.hotWordsList().success(function(data){
		$scope.hotWordsList = data;
	})
	var t = "";
	$scope.goCurrent = function(){
    	if($("#s1").is(":checked")){
    		t = "xxk";
    		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
    			ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
    			pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:t}).success(function(data){
    				var t = angular.fromJson(data.object);
    				if(data.success){
    					$scope.pageCount = t[0].totalPage;
    					$scope.totalRow = t[0].totalRow;
    					t.splice(0,1);
    					$scope.advanSearchList = t;
    					console.log(t);
    				}else{
    					c.$msg(data.message,1000,"error")
    				}
    		})
    	}
    	
    }
    
    $scope.goTotal = function(){
    	if($("#s2").is(":checked")){
    		t = "";
    		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
    			ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
    			pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:t}).success(function(data){
    				var t = angular.fromJson(data.object);
    				if(data.success){
    					$scope.pageCount = t[0].totalPage;
    					$scope.totalRow = t[0].totalRow;
    					t.splice(0,1);
    					$scope.advanSearchList = t;
    					console.log(t);
    				}else{
    					c.$msg(data.message,1000,"error")
    				}
    		})
    	}
    	
    }
    
    $scope.goAbolish = function(){
    	if($("#s3").is(":checked")){
    		t = "zfk";
    		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:$stateParams.keyWord,standardId:$stateParams.standardId,icsNum:$stateParams.icsNum,
    			ccsNumJson:$stateParams.ccsNumJson,adopt:$stateParams.adopt,standardSort:$stateParams.standardSort,
    			pubYearBegin:$stateParams.pubYearBegin,pubYearEnd:$stateParams.pubYearEnd,standardStatus:t}).success(function(data){
    				var t = angular.fromJson(data.object);
    				if(data.success){
    					$scope.pageCount = t[0].totalPage;
    					$scope.totalRow = t[0].totalRow;
    					t.splice(0,1);
    					$scope.advanSearchList = t;
    					console.log(t);
    				}else{
    					c.$msg(data.message,1000,"error")
    				}
    		})
    	}
    	
    }
    
    /**
     * 清除检索历史
     */
    $scope.removeSearchHistory = function(){
    	$scope.searchHistory = "";
    	$cookieStore.remove("searchHistory");
    }
    
    /**
     * 清除浏览历史
     */
    $scope.removeBrowseHistory = function(){
    	$scope.browseHistory="";
    	$cookieStore.remove("sName");
    }
    
    $scope.sName = $cookieStore.get("sName") ? angular.fromJson($cookieStore.get("sName")) : [];
    $scope.goStandardInfo = function(id,b){
    	 $scope.browseHistory = angular.fromJson($cookieStore.get("sName"));
    	 $scope.sName.unshift(b);	
    	 $cookieStore.put("sName",angular.toJson($scope.sName,true));
		 $state.go("standard_info",{id:id});
	}
	$scope.browseHistory = $scope.sName;

    
	var i =0;
	var sort = "";
	var name = "";
	/**
	 * 按照文献号进行排序
	 */
    $scope.hitStandardCode = function(){
    	name = "文献号";
    	if(i++%2==0){
    		sort = "desc";
    		console.log(i);
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		console.log(i);
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    
    /**
     * 按照标准名称进行排序
     */
    var k = 0;
    $scope.hitStandardName = function(){
    	name="中文题名";
    	if(k++%2==0){
    		sort = "desc";
    		console.log(k);
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    /**
     * 按照发布日期进行排序
     */
    var t = 0;
    $scope.hitReleaseDate = function(){
    	name = "发布日期";
    	if(t++%2==0){
    		sort = "desc";
    		console.log(t);
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    /**
     * 按照实施日期进行排序
     */
    var n = 0;
    $scope.hitImeplementDate = function(){
    	name = "实施日期";
    	if(n++%2==0){
    		sort = "desc";
    		console.log(n);
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			console.log(data);
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}else{
    		sort = "asc";
    		standardService.advencedSearch({pageNumber:$stateParams.currentPage,orderBy:name,ordering:sort}).success(function(data){
    			var t = angular.fromJson(data.object);
    			if(data.success){
    				$scope.pageCount = t[0].totalPage;
    				$scope.totalRow = t[0].totalRow;
    				t.splice(0,1);
    				$scope.advanSearchList = t;
    				console.log(t);
    			}else{
    				c.$msg(data.message,1000,"error")
    			}
    		})
    	}
    }
    
    
    /**
     * 立即购买
     */
    var array = [];
    $scope.instantaneousBuy = function(elec,count){
    	if(elec){
    		if($scope.title == "立即购买"){
    			elec.count = count
    			array.push(elec);
    			$state.go("shop_process",{cartList:array});
    		}else{
    			memberService.getSessionUserInfo().success(function(data){
    				if(data.success){
    					elec.count = count;
    					standardService.addCart(elec).success(function(data){
    						if(data.success){
    							$('.cd-popup3').removeClass('is-visible3');
    							c.$msg(data.message,1000,"success");
    						}else{
    							c.$msg(data.message,1000,"error");
    						}
    					})
    				}else{
    					$('.cd-popup4').addClass('is-visible4');
    				}
    			});
    		}
    	}else{
    		c.$msg("没有获得当前标准号相关信息,无法加入购物车",1000,"error");
    	}
    } 
    $scope.getStandardElec = function(a001){
    	$scope.count = 1;
    	memberService.getSessionUserInfo().success(function(dataUser){
			if(dataUser.success){
				$scope.title = "立即购买";
				$('.cd-popup3').addClass('is-visible3');
				standardInfoService.getStandardElec({code:a001}).success(function(data){
					if(data.success){
						$scope.elec = data.object[0];
					}else{
						c.$msg(data.message, 1000, 'error');
					}
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		})
	}
    
    
    /**
	 * 添加一个数量
	 */
    $scope.count = 1;//默认电子文献数量为1
	$scope.plus = function(elec) {
		++$scope.count;
	}
	
	/**
	 * 减少一个数量
	 */
	$scope.minus = function(elec) {
		--$scope.count;
	}
    
	$scope.tracking = function(standardId, standardName){
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				c.$load();
				//如果用户以登陆执行跟踪方法 
				standardTrackService.saveTrackingInfo(standardId).success(function(dataCollection){
					if(dataCollection.success){
						c.$threeBtnConfirm(dataCollection.message, "查看标准跟踪列表", function(){
							$state.go('standard_track');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	$scope.collection = function(standardId, standardName){
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				c.$load();
				//如果用户以及登陆执行收藏方法 data.object.id 用户id
				conllectionService.save({id : data.object.id, standard_id:standardId}).success(function(dataCollection){
					if(dataCollection.success){
						c.$threeBtnConfirm("您已经将《" + standardName + "》收藏了", "查看标准收藏列表", function(){
							$state.go('standard_conllection');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	$scope.getElec = function(identifier){
		standardInfoService.getStandardElec({code:identifier}).success(function(data){
			if(data.success){
				$scope.elec = data.object[0];
				console.log($scope.elec)
			}else{
				c.$msg(data.message, 1000, 'error');
			}
			if(data.success){
				window.open('/dynamic/getElecFileByBefore?code='+data.object[0].a100+"&filename="+data.object[0].fileName);
			}else{
				c.$msg(data.message, 1000, 'error');
			}
		})
	}
	
	$scope.getHotWords = function(name){
		c.$load();
		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:name}).success(function(data){
				var t = angular.fromJson(data.object);
				if(data.success){
					c.$closeLoad();
					$scope.pageCount = t[0].totalPage;
					$scope.totalRow = t[0].totalRow;
					t.splice(0,1);
					$scope.advanSearchList = t;
					console.log(t);
				}else{
					c.$msg(data.message,1000,"error")
				}
		})
	}
	
	$scope.getPop = function(code){
		c.$load();
		standardService.advencedSearch({pageNumber:$scope.currentPage,standardId:code}).success(function(data){
				var t = angular.fromJson(data.object);
				if(data.success){
					c.$closeLoad();
					$scope.pageCount = t[0].totalPage;
					$scope.totalRow = t[0].totalRow;
					t.splice(0,1);
					$scope.advanSearchList = t;
					console.log(t);
				}else{
					c.$msg(data.message,1000,"error")
				}
		})
	}
	
	$scope.getSearch = function(historyname){
		c.$load();
		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:historyname}).success(function(data){
				var t = angular.fromJson(data.object);
				if(data.success){
					c.$closeLoad();
					$scope.pageCount = t[0].totalPage;
					$scope.totalRow = t[0].totalRow;
					t.splice(0,1);
					$scope.advanSearchList = t;
					console.log(t);
				}else{
					c.$msg(data.message,1000,"error")
				}
		})
	}
	
	$scope.getBrowse = function(browse){
		c.$load();
		standardService.advencedSearch({pageNumber:$scope.currentPage,keyWord:browse}).success(function(data){
				var t = angular.fromJson(data.object);
				if(data.success){
					c.$closeLoad();
					$scope.pageCount = t[0].totalPage;
					$scope.totalRow = t[0].totalRow;
					t.splice(0,1);
					$scope.advanSearchList = t;
					console.log(t);
				}else{
					c.$msg(data.message,1000,"error")
				}
		})
	}
}])

appService.service('standardService', ['$http', function ($http) {
    return {
    	getStandardSortList:function (query) {
            return $http.post('/search/getStandardSortList',query);
        },
        getStandardStatus:function () {
            return $http.post('/search/getStandard');
        },
        advencedSearch:function (query) {
            return $http.post('/search/advencedSearch',query);
            console.log("111");
        },
        findAll:function () {
            return $http.post('/search/findAll');
        },
        findInterstandardClasses:function () {
            return $http.post('/search/findInterstandardClasses');
        },
        addCart:function (code) {
            return $http.post('/order/addCart',{code:code});
        },
        hotWordsList:function () {
            return $http.post('/search/hotWordsList');
        },
        getConcernStandard:function (identifier) {
            return $http.post('/search/getConcernStandard',{identifier:identifier});
        },
        standardTimeList:function () {
            return $http.post('/search/standardTimeList');
        },
    }
}])