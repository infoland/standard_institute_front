 appController.controller('cartController',  ["$cookieStore","$scope","$rootScope","$stateParams","$state","cartService","orderService",function ($cookieStore,$scope,$rootScope,$stateParams,$state,cartService,orderService) {
	$scope.title="购物车"
	$scope.goAddress = function(){
		$state.go("adderss")
	}
	/**
	 * 购物车列表数据展示
	 */
 	$scope.pageCount = 1;
	cartService.list().success(function (data) {
    	$scope.cartList = data;
    	$scope.pageCount = data.totalPage;
    	$scope.totalRow = data.totalRow;
    })
    
	/**
	 * 复选框
	 */
	$scope.toggleCheckBox = function (id) {
	    $scope.ids = c.toggleCheckBox(id);
	    var a = $scope.cartList.list;
	    if($("#checkAll").is(":checked")){
	    	for(var i = 0;i < a.length;i++){
	    		$scope.number += a[i].count;
	    		$scope.total += a[i].price*a[i].count;
	    	}
	    }else{
	    	$scope.number = 0;
    		$scope.total = 0;
	    }
	}
	    
    /**
     * 单个删除
     */
    $scope.del = function(id){ 
		var arr = [id];
		c.$confirm("确认删除？", function () {
			cartService.del(arr).success(function(data){
				if(data.success){
					c.$msg(data.message,1000,'success');
				}else{
					c.$msg(data.message,1000,'error');
				}
				$state.reload();
			})
		})
	}
		
		
	/**
	 * 批量删除
	 */	
	$scope.deleteAll = function(){
		if($("#allselect").hasClass("sc_b0 input")){ 
			$("#allselect").removeClass("sc_b0 input");
			$(".sc_check").each(function (i){
				//$(".oneselect").attr("checked", false);
				if($(this).hasClass("sc_check input")){
					$(this).removeClass("sc_check input");
				}else{
					$(this).addClass("sc_check input");
				} 
			});
		}
		if($scope.ids !== undefined){
			if($scope.ids.length > 0){
				c.$confirm("确认删除？", function () {
					cartService.del($scope.ids).success(function(data){
						if(data.success){
							c.$msg(data.message,1000,'success');
						}else{
							c.$msg(data.message,1000,'error');
						}
					})
					$state.reload();
				})
			}else{
				c.$msg("请选择要删除的数据",1000,'error');
			}
		}else{
			c.$msg("请勾选至少一条要删除的数据",1000,'error');
		}
	}
		
	/**
	 * 通过下标循环购物车里的商品
	 */
	var findIndex = function(id) {
		var index = -1;		
		angular.forEach($scope.cartList.list, function(item, key) {//key:下标
				if(item.id == id){
					index = key ;
					return ;
				}
		})
		return index;
	}
	
	/**
	 * 添加一个数量
	 */
	$scope.plus = function(cart) {
		var index = findIndex(cart.id);//根据购物车id发现要添加数量的数据
		if (index !== -1) {
			if($scope.cartList.list[index].count>0){
				++$scope.cartList.list[index].count;
			}
		}
	}
	
	/**
	 * 减少一个数量
	 */
	$scope.minus = function(cart) {
		var index = findIndex(cart.id);//根据购物车id发现要添加数量的数据
		if (index !== -1) {//如果下标不等-1,表示购物车里至少有一条数据,判断被选中的复选框是否有此样式,如果没有添加此样式
			if($scope.cartList.list[index].count>1){
				--$scope.cartList.list[index].count;
			}
		}
	}
		
		
	/**
	 * 选中商品
	 */
	$scope.number = 0;
	$scope.total = 0;
	$scope.itemSelected = function(cart){
		var index = findIndex(cart.id);
		if (index !== -1) {
			angular.forEach($scope.cartList.list, function(item) {
				if($("#box_checkIcon_"+item.id).is(':checked')){
					$scope.number += item.count;
					$scope.total += item.price*item.count;
				} 
			})
		}
 	}
	 
	 /**
	  * 翻页
	  */
	 $scope.onPageChange = function(){
		 var thead = document.querySelector("#checkHead  input[type='checkbox']");
		 thead.checked = false
		 $scope.number=0;
		 $scope.total=0;
		 cartService.list({pageNumber:$scope.currentPage}).success(function (data) {
	    	$scope.cartList = data;
	    	$scope.pageCount = data.totalPage;
	    	$scope.totalRow = data.totalRow;
		 })
	 }
	 
	 
	 /**
	  * 结算
	  */
	 $scope.goShop = function(){
		 var arrs = [];
		 if($scope.ids == null){
			 c.$msg("请选择您要结算的订单",1000,"error");
			 return false;
		 }else{
			 for(var i=0;i < $scope.cartList.list.length;i++){
				 for(var j = 0;j < $scope.ids.length;j++){
					 if($scope.cartList.list[i].id == $scope.ids[j]){
						 arrs.push($scope.cartList.list[i])
					 }
				 }
			 }
			 $state.go("shop_process",{cartList:arrs})
		 }
		 
	 }
	 
}])
	
appService.service('cartService', ['$http', function ($http) {
    return {
    	list:function(query){
    		return $http.post('/cart/cartList',query);
    	},
    	del:function(ids){
    		return $http.post('/cart/deleteCartById',{ids:ids});
    	},
    	addCart:function (cart) {
            return $http.post('/order/addCart',cart);
        },
        saveOrder:function (ids) {
            return $http.post('/order/saveOrder',{ids:ids});
        },
    }
}])

