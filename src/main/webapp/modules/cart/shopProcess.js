 appController.controller('shopProcessController',  ["$cookieStore","$scope","$rootScope","$stateParams","$state","cartService","orderService",function ($cookieStore,$scope,$rootScope,$stateParams,$state,cartService,orderService) {
	 console.log($stateParams.cartList)
	 if($stateParams.cartList){
		 $scope.confirmList = $stateParams.cartList;
	 }
	 
	/**
	 * 查询地址
	 */
	orderService.findIsDefaultAddress().success(function(data){
		$scope.consigneeList = data
	})
	
	/**
	 * 点击我要付款弹出提示	
	 */
	$scope.payMoney = function(){
		$("#payMoney").modal('show')
		$(".modal-backdrop").hide();//去除遮罩层
	}
	/**
	 * 选择地址
	 */
	var addr = {};
	$scope.choiceAddr = function(index){
		for(var i = 0;i < $scope.consigneeList.length;i ++){
			if(i == index){
				addr = $scope.consigneeList[i];
				$("#addr_"+$scope.consigneeList[i].id).attr("checked",true);
				console.log(addr);
			}else{
				$("#addr_"+$scope.consigneeList[i].id).attr("checked",false);
			}
		}
	}
	
	/**
	 * 提交订单
	 */
	var rId = "";
	var record = {};
	$scope.submitOrder = function () {
		if(!addr.id){
			c.$msg("请选择收获地址",1000,"error");
			return false;
		}
		cartService.saveOrder({cId:addr.id,record:$scope.confirmList}).success(function(data){
			if(data.success){
				c.$msg(data.message,1000,'success');
			}else{
				c.$msg(data.message,1000,'error');
			}
			$("#success").modal("show");
			$(".modal-backdrop").hide();//去除遮罩层
		})
	}
	
	$scope.back = function(){
		$state.go("cart");
		console.log($stateParams.cartList);
	}
}])
		
appService.service('cartService', ['$http', function ($http) {
    return {
    	list:function(query){
    		return $http.post('/cart/cartList',query);
    	},
    	del:function(ids){
    		return $http.post('/cart/deleteCartById',{ids:ids});
    	},
    	addCart:function (cart) {
            return $http.post('/order/addCart',cart);
        },
        saveOrder:function (query) {
            return $http.post('/order/saveOrder',query);
        },
    }
}])

