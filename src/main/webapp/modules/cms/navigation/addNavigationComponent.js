/**
 * 导航管理新增
 */
var util = Common; 
appController.controller('addNavigationController',["$scope","$rootScope","$stateParams","$state","navigationService","navigationCategoryService",
                                                    function($scope,$rootScope,$stateParams,$state,navigationService,navigationCategoryService){
	
	/**
	 * 加载fileinput
	 * 
	 */
        util.$initFileinput(1, [], []);
	
	/**
	 * 数据清空
	 */
	$scope.navigation = [];

	/**
	 * 上级分类下拉菜单数据加载
	 */
	navigationCategoryService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.input = function(){
		var imageIds = $("#imageIds").val();
		//$scope.navigation.na_url = "#/" + $scope.urlType + "/" + $scope.urlValue;
		navigationService.input($scope.navigation, imageIds).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_navigation');
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_navigation');
	}
}])
