/**
 * 导航管理列表
 */
var util = Common; 
appController.controller('listNavigationController',["$scope","$rootScope","$stateParams","$state","navigationService","navigationCategoryService",
                                                     function($scope,$rootScope,$stateParams,$state,navigationService,navigationCategoryService){
	
	/**
	 * 列表数据加载
	 */
	util.pageNumber = 1;
	util.pageSize = 10;
	navigationService.list().success(function(data){
		$scope.pageList = data;
		$scope.pageCountList = util.pageFun(data);
		$scope.pageUrl = "/navigation/list";
		$scope.queryFromId = 'myForm';
	})
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	navigationCategoryService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 重置
	 */
	$scope.reset = function(){
		 $scope.navigation = [];
	}
	
	/**
	 * 搜索
	 */	
	$scope.search = function(){
		util.pageNumber = 1;//添加 分页搜索 2017/05/24 yanglj
		util.pageSize = 10;
		$("#navigation_category_id").val($scope.navigation.navigation_category_id);
		navigationService.list().success(function(data){
			$scope.pageList = data;
			$scope.pageCountList = util.pageFun(data);
			$scope.pageUrl = "/navigation/list"; // 2017/5/24 修改路径
			$scope.queryFormId = "myForm";
		})
	}
	/**
	 * 新增
	 */	
	$scope.add = function(){
		$scope.navigation = null;
		$rootScope.urlType = null;
		$rootScope.urlValue =null;
		$state.go("add_navigation");
	}
	/**
	 * 编辑
	 */	
	$scope.edit = function(id){
		$state.go("edit_navigation",{id:id});
	}
	
	$scope.toggleCheckBox = function(id){
		$scope.ids = util.toggleCheckBox(id)
	}
	/**
	 * 单项删除
	 */	
	$scope.del = function(id){
		util.$confirm("确定要删除吗？",function(){
			navigationService.del(id).success(function(data){
				if(data.success){
					$("#myModalConfirm").modal('hide');
					util.$alert(data.message);
					$state.go('list_navigation',{}, {reload: true});
				}else{
					util.$alert(data.message);
				}
			}).error(function(status){
				util.$alert("服务器"+status+"错误");
			})
		})
	}
	
	/**
	 * 批量删除
	 */
	$scope.mDel = function(){
		console.log($scope.ids);
		if($scope.ids!=undefined){
			if($scope.ids.length>0){
				util.$confirm("确定要删除吗？",function(){
		    		navigationService.del($scope.ids).success(function(data){
		    			if(data.success){
		    				$("#myModalConfirm").modal('hide');
		    				util.$alert(data.message);
		    				$state.go('list_navigation',{}, {reload: true});
		    			}else{
		    				util.$alert(data.message);
		    			}
		    		}).error(function(status){
		    			util.$alert("服务器"+status+"错误");
		    		})
				})
			}else{
				util.$alert("请选择要删除的数据");
			}
		}else{
			util.$alert("请选择要删除的数据");
		}
	}
	
}])