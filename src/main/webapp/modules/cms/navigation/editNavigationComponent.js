/**
 * 导航管理编辑
 */
var util = Common; 
appController.controller('editNavigationController',["$scope","$rootScope","$stateParams","$state","navigationService","navigationCategoryService",
                                                     function($scope,$rootScope,$stateParams,$state,navigationService,navigationCategoryService){
	
	/**
	 * 数据加载
	 */
	navigationService.getNavigation($stateParams.id).success(function(data){
		$scope.navigation = data;
		var url = data.na_url.split("/");
		$rootScope.urlType = url[1];
		$rootScope.urlValue = url[2];
	})
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	navigationCategoryService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 图片数据加载
	 */
	navigationService.getNavigationImg($stateParams.id).success(function(data){
		var initialPreview = [];
		var initialPreviewConfig = [];
		if(data != undefined){
			angular.forEach(data, function(img, index){
				initialPreview.push(img.uploadimage_url);
				initialPreviewConfig.push({
					caption: img.uploadimage_name, 
					width: "120px", 
					url: "/upload/deleteFile/" + img.id, 
					key: index
				})
			});
		}
		util.$initFileinput(1, initialPreview, initialPreviewConfig);
	})
	
	/**
	 * 更新数据
	 */
	$scope.update = function(){
		var imageIds = $("#imageIds").val();
		//$scope.navigation.na_url = "#/" + $scope.urlType + "/" + $scope.urlValue;
		navigationService.input($scope.navigation, imageIds).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_navigation');
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_navigation');
	}
	
}])