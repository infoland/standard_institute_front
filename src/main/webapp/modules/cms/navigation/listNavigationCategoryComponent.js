/**
 * 广告分类列表页
 */
var util = Common;
appController .controller('listNavigationCategoryController',["$scope","$rootScope","$location","$stateParams","$state","navigationCategoryService","navigationService",function ($scope,$rootScope,$location,$stateParams,$state,navigationCategoryService,navigationService){
   	
	/**
	 * 列表数据加载
	 */
	util.pageNumber = 1;
	util.pageSize = 10;
	$scope.navigationCategory = [];
	navigationCategoryService.list().success(function(data){
		$scope.pageList = data;
		$scope.pageCountList = util.pageFun(data);
		$scope.pageUrl = '/navigationCategory/list';
		$scope.queryFromId = 'myForm';
	})
	
	$scope.toggleCheckBox = function(id){
		$scope.ids = util.toggleCheckBox(id)
	}
	
	/**
	 * 重置
	 */
	$scope.reset = function(){
		 $scope.nc = [];
	}
	
	/**
	 * 批量删除
	 */	
	$scope.delAll = function (){
		if($scope.ids != undefined){
			if($scope.ids.length > 0){
				util.$confirm("确认要删除数据吗？",function(){
		    		navigationCategoryService.del($scope.ids).success(function(data){
		    			if(data.success){
		    				$("#myModalConfirm").modal('hide');
		    				$state.go("list_navigation_category",{}, {reload: true});
		    			}else{
		    				util.$alert("有子项的数据不能被删除")
		    			}
		    		})
				})
			}else{
				util.$alert("请选择要删除的数据");
			}
		}else{
			util.$alert("请选择要删除的数据");
		}
	}

	/**
	 * 单项删除
	 */	
	$scope.del = function(id){
		util.$confirm("确认要删除吗？",function(){
			navigationCategoryService.del(id).success(function(data){
				if(data.success){
					$("#myModalConfirm").modal('hide');
					$state.go("list_navigation_category",{}, {reload: true});
				}else{
					util.$alert("有子项的数据不能删除");
				}
			})
		})
	}
	
	/**
	 * 搜索
	 */		
	$scope.search = function() {
		util.pageNumber = 1;
		util.pageSize = 10;
		navigationCategoryService.list().success(function(data){
			$scope.pageList = data;
			$scope.pageCountList = util.pageFun(data);
			$scope.pageUrl = '/navigationCategory/list';
			$scope.queryFromId = 'myForm';
		})
	}
	
	/**
	 * 新增
	 */	
	$scope.add = function(){
		$rootScope.ac = null;
		$state.go("add_navigation_category");
	}
	
	/**
	 * 编辑
	 */	
	$scope.edit = function(id){
		$state.go("edit_navigation_category",{id:id});
	}
}])