/**
 * 广告分类新增页
 */
var util = Common;
appController .controller('addNavigationCategoryController',["$scope","$rootScope","$location","$stateParams","$state","navigationCategoryService","navigationService",function ($scope,$rootScope,$location,$stateParams,$state,navigationCategoryService,navigationService){
   	
	/**
	 * 数据清空
	 */
	$scope.nc = [];
	
	/**
	 * 放置位置下拉菜单数据加载
	 */
	navigationCategoryService.getPositionList().success(function(data){
		$scope.positionList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.save = function(){
		navigationCategoryService.save($scope.nc).success(function(data){
			if(data.success){
				$state.go("list_navigation_category");
			}else{
				alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_navigation_category");
	}
}])