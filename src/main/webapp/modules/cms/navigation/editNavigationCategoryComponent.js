/**
 * 广告分类编辑页
 */
var util = Common;
appController .controller('editNavigationCategoryController',["$scope","$rootScope","$location","$stateParams","$state","navigationCategoryService","navigationService",function ($scope,$rootScope,$location,$stateParams,$state,navigationCategoryService,navigationService){
   	
	/**
	 * 数据加载
	 */
	navigationCategoryService.getAc($stateParams.id).success(function(data){
		$scope.nc = data;
	})
	
	/**
	 * 放置位置下拉菜单数据加载
	 */
	navigationCategoryService.getPositionList().success(function(data){
		$scope.positionList = data;
	})
	
	/**
	 * 更新数据
	 */
	$scope.update = function(){
		navigationCategoryService.update($scope.nc).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go("list_navigation_category");
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_navigation_category");
	}
}])