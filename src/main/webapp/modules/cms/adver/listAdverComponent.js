/**
 * 广告列表页
 */
var util = Common;
appController.controller('listAdverController',["$scope","$rootScope","$location","$stateParams","$state","adverService","adCateService",function ($scope,$rootScope,$location,$stateParams,$state,adverService,adCateService) {

	/**
	 * 列表数据加载
	 */
	util.pageNumber = 1;
	util.pageSize = 10;
	adverService.list().success(function(data){
		$scope.pageList = data;
		$scope.pageCountList = util.pageFun(data);
		$scope.pageUrl = "/adver/list";
		$scope.queryFromId = 'myForm';
	});
	
	$scope.toggleCheckBox = function(id){
		$scope.ids = util.toggleCheckBox(id)
	}
	
	/**
	 * 重置
	 */
	$scope.reset = function(){
		 $scope.adver = [];
	}
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	adCateService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 搜索
	 */		
	$scope.search = function(){
		util.pageNumber = 1;
		util.pageSize = 10;
		$("#adver_category_id").val($scope.adver.adver_category_id);
		adverService.list().success(function(data){
			$scope.pageList = data;
			$scope.pageCountList = util.pageFun(data);
			$scope.pageUrl = "/adver/list";
			$scope.queryFormId = "myForm"
		})
	}
	
	/**
	 * 批量删除
	 */	
	$scope.delAll = function (){
		if(typeof $scope.ids !== "undefined"){
			if($scope.ids.length > 0){
				util.$confirm("确认要删除吗？",function(){
					adverService.del($scope.ids).success(function(data){
						if(data.success){
							$("#myModalConfirm").modal('hide');
							$state.go("list_adver",{}, {reload: true});
						}else{
							util.$alert(data.message);
						}
					})
				})
			}else{
				util.$alert("请选择要删除的数据");
			}
		}else{
			util.$alert("请选择要删除的数据");
		}
	}

	/**
	 * 单项删除
	 */	
	$scope.del = function(id){
		util.$confirm("确认要删除吗？",function(){
			adverService.del(id).success(function(data){
				if(data.success){
					$("#myModalConfirm").modal('hide');
					$state.go("list_adver",{}, {reload: true});
				}else{
					util.$alert(data.message);
				}
			});
		})
	}

	/**
	 * 新增
	 */	
	$scope.add = function(){
		$state.go("add_adver");
	}
	
	/**
	 * 编辑
	 */	
	$scope.edit = function(id){
		$state.go("edit_adver",{id: id});
	}
}])

appService.service("adverService",
		["$rootScope","$http","dictService",function($scope,$http,dictService){
			return {
				list:function(){
					return $http.post("/adver/list/"+util.pageNumber+"-"+util.pageSize, $("#myForm").serialize());
				},
				getAdver:function(id){
					if(typeof(id)!="undefined"){
						return $http.post("/adver/getAdver/"+id);
					}
				},
				getAdverImg:function(id){
					if(typeof(id)!="undefined"){
						return $http.post("/adver/getAdverImg/"+id);
					}
				},
				save:function(adver, imageIds){
					if(adver !== undefined){
						var object = util.formatObj(adver, "adver");
						if(typeof imageIds!=="undefined"){
							object = util.addToArray(imageIds, "imageIds", object);
						}
						return $http.post("/adver/save",$.param(object));
					}
				},
				update:function(adver, imageIds){
					if(adver !== undefined){
						var object = util.formatObj(adver, "adver");
						if(typeof imageIds!=="undefined"){
							object = util.addToArray(imageIds, "imageIds", object);
						}
						return $http.post("/adver/update",$.param(object));
					}
				},
				del:function(ids){
					if(ids!=undefined){
						return $http.post("/adver/delete/"+ids);
					}
				},
				getImgTypeList:function(){
					return dictService.getOptions("uploadimage_type");
				}
			}
		}])