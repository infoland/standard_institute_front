/**
 * 广告分类新增页
 */
var util = Common;
appController .controller('addAdverCategoryController',["$scope","$rootScope","$location","$stateParams","$state","adCateService","adverService",function ($scope,$rootScope,$location,$stateParams,$state,adCateService,adverService){
   	
	/**
	 * 数据清空
	 */
	$scope.ac = [];
	
	/**
	 * 放置位置下拉菜单数据加载
	 */
	adCateService.getPositionList().success(function(data){
		$scope.positionList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.save = function(){
		adCateService.save($scope.ac).success(function(data){
			if(data.success){
				$state.go("list_adver_category");
			}else{
				alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_adver_category");
	}
}])