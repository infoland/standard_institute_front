/**
 * 广告分类编辑页
 */
var util = Common;
appController .controller('editAdverCategoryController',["$scope","$rootScope","$location","$stateParams","$state","adCateService","adverService",function ($scope,$rootScope,$location,$stateParams,$state,adCateService,adverService){
   	
	/**
	 * 数据加载
	 */
	adCateService.getAc($stateParams.id).success(function(data){
		$scope.ac = data;
	})
	
	/**
	 * 放置位置下拉菜单数据加载
	 */
	adCateService.getPositionList().success(function(data){
		$scope.positionList = data;
	})
	
	/**
	 * 更新数据
	 */
	$scope.update = function(){
		adCateService.update($scope.ac).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go("list_adver_category");
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_adver_category");
	}
}])