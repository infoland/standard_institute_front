/**
 * 广告新增页
 */
var util = Common;
appController.controller('addAdverController',["$scope","$rootScope","$location","$stateParams","$state","adverService","adCateService",function ($scope,$rootScope,$location,$stateParams,$state,adverService,adCateService) {
 	
	/**
	 * 加载fileinput
	 * 
	 */
    util.$initFileinput(1, [], []);
    
	/**
	 * 数据清空
	 */
	$scope.adver = [];
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	adCateService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.save = function(){
		var imageIds = $("#imageIds").val();
		adverService.save($scope.adver, imageIds).success(function(data){
			if(data.success){
				$state.go("list_adver");
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_adver");
	}
	
}])