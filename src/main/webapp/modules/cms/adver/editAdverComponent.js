/**
 * 广告编辑页
 */
var util = Common;
appController.controller('editAdverController',["$scope","$rootScope","$location","$stateParams","$state","adverService","adCateService",function ($scope,$rootScope,$location,$stateParams,$state,adverService,adCateService) {
	
	/**
	 * 数据加载
	 */
	adverService.getAdver($stateParams.id).success(function(data){
		$rootScope.adver = data;
	})
	
	/**
	 * 图片数据加载
	 */
	adverService.getAdverImg($stateParams.id).success(function(data){
		var initialPreview = [];
		var initialPreviewConfig = [];
		if(data != undefined){
			angular.forEach(data, function(img, index){
				initialPreview.push(img.uploadimage_url);
				initialPreviewConfig.push({
					caption: img.uploadimage_name, 
					width: "120px", 
					url: "/upload/deleteFile/" + img.id, 
					key: index
				})
			});
		}
		util.$initFileinput(1, initialPreview, initialPreviewConfig);
	})

	/**
	 * 上级分类下拉菜单数据加载
	 */
	adCateService.getCateList().success(function(data){
		$scope.categoryList = data;
	})
	
	/**
	 * 更新数据
	 */
	$scope.update = function(){
		var imageIds = $("#imageIds").val();
		adverService.update($scope.adver, imageIds).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go("list_adver");
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go("list_adver");
	}	
	
}])