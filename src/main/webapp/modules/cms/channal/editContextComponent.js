/**
 * 文章内容编辑
 */
var util = Common;
appController.controller('editContextController',["$scope","$rootScope","$location","$stateParams","$state","channalService","contextService",function($scope,$rootScope,$location,$stateParams,$state,channalService,contextService){
	
	/**
	 * 数据加载
	 */
	contextService.getContext($stateParams.id).success(function(data){
		$scope.context = data;
	})
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	channalService.getOptions().success(function(data){
		$scope.channalList = data;
	})
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_context');
	}
	
	/**
	 * 图片数据加载
	 */
	contextService.getAdverImg($stateParams.id).success(function(data){
		var initialPreview = [];
		var initialPreviewConfig = [];
		if(data != undefined){
			angular.forEach(data, function(img, index){
				initialPreview.push(img.uploadimage_url);
				initialPreviewConfig.push({
					caption: img.uploadimage_name, 
					width: "120px", 
					url: "/upload/deleteFile/" + img.id, 
					key: index
				})
			});
		}
		util.$initFileinput(1, initialPreview, initialPreviewConfig);
	})
	
	/**
	 * 保存数据
	 */
	$scope.update = function(){
		var imageIds = $("#imageIds").val();
		contextService.update($scope.context, imageIds).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_context');
			}else{
				util.$alert(data.message);
			}
		})
	}
	
}])