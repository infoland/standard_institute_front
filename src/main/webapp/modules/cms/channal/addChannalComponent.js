/**
 * 栏目管理新增
 */
var util = Common;
appController.controller('addChannalController',["$scope","$rootScope","$location","$stateParams","$state","channalService",function($scope,$rootScope,$location,$stateParams,$state,channalService){
	
	/**
	 * 数据清空
	 */
	$scope.channal = [];
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	channalService.getOptions().success(function(data){
		$scope.channalList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.save = function(){
		channalService.save($scope.channal).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_channal',{}, {reload: true});
			}else{
				util.$alert(data.message);
			}
		})
	}
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_channal');
	}
	
}])