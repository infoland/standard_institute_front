/**
 * 栏目管理编辑
 */
var util = Common;
appController.controller('editChannalController',["$scope","$rootScope","$location","$stateParams","$state","channalService",function($scope,$rootScope,$location,$stateParams,$state,channalService){
	
	/**
	 * 数据加载
	 */
	channalService.getChannal($stateParams.id).success(function(data){
		$scope.channal = data;
	})
	
	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_channal');
	}
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	channalService.getOptions().success(function(data){
		$scope.channalList = data;
	})
	
	/**
	 * 更新数据
	 */
	$scope.update = function(){
		channalService.update($scope.channal).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_channal',{}, {reload: true});
			}else{
				util.$alert(data.message);
			}
		})
	}
}])