/**
 * 文章内容新增
 */
var util = Common;
appController.controller('addContextController',["$scope","$rootScope","$location","$stateParams","$state","channalService","contextService",function($scope,$rootScope,$location,$stateParams,$state,channalService,contextService){
	
	/**
	 * 加载fileinput
	 * 
	 */
    util.$initFileinput(1, [], []);
    
	/**
	 * 数据清空
	 */
	$scope.context = [];
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	channalService.getOptions().success(function(data){
		$scope.channalList = data;
	})
	
	/**
	 * 保存数据
	 */
	$scope.save = function(){
		var imageIds = $("#imageIds").val();
		contextService.save($scope.context, imageIds).success(function(data){
			if(data.success){
				util.$alert(data.message);
				$state.go('list_context');
			}else{
				util.$alert(data.message);
			}
		})
	}

	/**
	 * 返回列表页
	 */
	$scope.back = function(){
		$state.go('list_context');
	}
}])