/**
 * 栏目管理列表
 */
var util = Common;
appController.controller('listChannalController',["$scope","$rootScope","$location","$stateParams","$state","channalService",function($scope,$rootScope,$location,$stateParams,$state,channalService){
	
	/**
	 * 列表数据加载
	 */
	util.pageNumber = 1;
	util.pageSize = 10;
	channalService.list().success(function(data){
		$scope.pageList = data;
		$scope.pageCountList = util.pageFun(data);
		$scope.pageUrl = "/channal/list";
		$scope.queryFormId = "myForm"
	});
	
	$scope.toggleCheckBox = function(id){
		$scope.ids = util.toggleCheckBox(id)
	}
	
	/**
	 * 重置
	 */
	$scope.reset = function(){
		 $scope.channal = [];
	}
	
	/**
	 * 批量删除
	 */
	$scope.mDel = function(){
		if($scope.ids!=undefined && $scope.ids.length>0){
			util.$confirm("确认要删除吗？",function(){
				channalService.del($scope.ids).success(function(data){
    				if(data.success){
    					$state.go("list_channal",{}, {reload: true});
    				}else{
    					util.$alert(data.message);
    				}
	    		})
			});
		}else{
			util.$alert("请选择要删除的数据");
		}
	}
	
	/**
	 * 单项删除
	 */	
	$scope.del = function(id){
		util.$confirm("确认要删除吗？",function(){
			channalService.del(id).success(function(data){
				if(data.success){
					$state.go("list_channal",{}, {reload: true});
				}else{
					util.$alert(data.message);
				}
    		})
		});
	}
	
	/**
	 * 搜索
	 */	
	$scope.search = function(){
		util.pageNumber = 1;
		util.pageSize = 10;
		channalService.list().success(function(data){
			$scope.pageList = data;
			$scope.pageCountList = util.pageFun(data);
			$scope.pageUrl = "/channal/list";
			$scope.queryFormId = "myForm"
		})
	}
	
	/**
	 * 新增
	 */	
	$scope.add = function(){
		$state.go('add_channal');
	}
	
	/**
	 * 编辑
	 */	
	$scope.edit = function(channalId){
		$state.go("edit_channal",{id:channalId});
	}
}])
