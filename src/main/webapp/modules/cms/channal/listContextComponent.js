/**
 * 文章内容
 */
var util = Common;
appController.controller('listContextController',["$scope","$rootScope","$location","$stateParams","$state","channalService","contextService",function($scope,$rootScope,$location,$stateParams,$state,channalService,contextService){
	
	/**
	 * 列表数据加载
	 */
	util.pageNumber = 1;
	util.pageSize = 10;
	contextService.list().success(function(data){
		$scope.pageList = data;
		$scope.pageCountList = util.pageFun(data);
		$scope.pageUrl = "/context/list";
		$scope.queryFromId = 'myForm';
	})
	
	$scope.toggleCheckBox = function(id){
		$scope.ids = util.toggleCheckBox(id)
	}

	/**
	 * 重置
	 */
	$scope.reset = function(){
		 $scope.context = [];
	}
	
	/**
	 * 上级分类下拉菜单数据加载
	 */
	channalService.getOptions().success(function(data){
		$scope.channalList = data;
	})
	
	/**
	 * 批量删除
	 */	
	$scope.mDel = function(){
		if($scope.ids!=undefined && $scope.ids.length>0){
    		util.$confirm("确认要删除吗？",function(){
    			contextService.del($scope.ids).success(function(data){
    				if(data.success){
    					$state.go("list_context", {}, {reload: true});
    				}else{
    					util.$alert(data.message);
    				}
	    		})
			})
		}else{
			util.$alert("请选择要删除的数据");
		}
	}
	
	/**
	 * 搜索
	 */
	$scope.search = function(){
		util.pageNumber = 1;
		util.pageSize = 10;
		$("#channal_id").val($scope.context.channal_id);
		contextService.list().success(function(data){
			$scope.pageList = data;
			$scope.pageCountList = util.pageFun(data);
			$scope.pageUrl = "/context/list";
			$scope.queryFromId = 'myForm';
		})
	}
	
	/**
	 * 新增
	 */	
	$scope.add = function(){
		$state.go('add_context');
	}
	
	/**
	 * 编辑
	 */
	$scope.edit = function(id){
		$state.go("edit_context",{id: id});
	}
	
	/**
	 * 单项删除
	 */	
	$scope.del = function(id){
		util.$confirm("确认要删除吗？",function(){
			contextService.del(id).success(function(data){
				if(data.success){
					$state.go("list_context", {}, {reload: true});
				}else{
					util.$alert(data.message);
				}
    		})
		});
	}
	
}])
