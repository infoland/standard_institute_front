 appController.controller('orderController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	/**
	 * 时间控件
	 */
	c.$date();
	
	/**
	 * 待处理订单数据列表
	 */
	$scope.pageCount = 1;
	orderService.handleOrderList().success(function(data){
		if(data){
			$scope.handleOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		}else{
			c.$msg("您没有待处理订单");
		}
	})
	
	
	 /**
	  * 撤销订单
	  */
	 $scope.cancleOrder = function (code) {
		c.$confirm("真的要撤销订单吗？", function () {
		 	orderService.cancleOrder(code).success(function (data) {
		         if (data.success) {
		             c.$msg(data.message, 1000, 'success');
		         } else {
		             c.$msg(data.message, 1000, 'error');
		         }
		         $state.reload();
		     })
		})
	 };
	 
	 /**
	  * tab分页
	  */
	$scope.tabs = [true,false,false];
	$scope.i = 0;//给默认值为0,
	$scope.tab = function(i){
		$scope.i = i;
		angular.forEach($scope.tabs,function(item,index){
			$scope.tabs[index] = false;
		})
		$scope.tabs[i] = true;
	}
	
    /**
     * 查询
     */
    $scope.search = function(){
    	var start = $("#myStartDate").val();
    	var end = $("#myEndDate").val();
    	console.info($scope.startDate);
    	orderService.handleOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:start,endDate:end}).success(function(data){
			$scope.handleOrderList = data;
			console.log($scope.handleOrderList)
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    }
	
	/**
	 * 翻页
	 */
	$scope.onPageChange = function () {
		orderService.handleOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:$scope.startDate,endDate:$scope.endDate}).success(function(data){
			$scope.handleOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    };
    
}])

.controller('addressController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	/**
	 * 保存
	 */
	$scope.saveAddress = function(){
		$scope.consigneeInfo.province = $("#sel1").val().split("-")[0];
		$scope.consigneeInfo.city = $("#sel1").val().split("-")[1];
		if($("#checkbox_c").attr("checked")){
			$scope.consigneeInfo.is_default = true
		}else{
			$scope.consigneeInfo.is_default = false
		}
		
		if($scope.consigneeList.length == 5){
			c.$msg("您已经添加了5条地址!",1000,'error');
			return false;
		}else{
			orderService.saveConsignee($scope.consigneeInfo).success(function(data){
				if(data.scuuess){
					c.$msg(data.message,1000,'success');
				}else{
					c.$msg(data.message,1000,'error');
				}
				
				$state.reload();	
			})
		}
	}
	$scope.title="收获地址管理"
		
	$scope.editAddress = function(id){
		$state.go("adderss_edit",{id:id})
	}
			
	/**
	 * 设置默认地址
	 */
	$scope.isDefault = function(id){
		orderService.isDefault(id).success(function(data){
			if(data.success){
				c.$msg(data.message,1000,'success');
			}else{
				c.$msg(data.message,1000,'error');
			}
			$state.reload();
		})
	}
	
	
	
	/**
	 * 删除地址
	 */
 $scope.delAddress = function (id) {
 	orderService.deleteAddress(id).success(function (data) {
         if (data.success) {
             c.$msg(data.message, 1000, 'success');
         } else {
             c.$msg(data.message, 1000, 'error');
         }
         $state.reload();
     })
 };
 
	 
	/**
	 * 查询地址
	 */
	orderService.findIsDefaultAddress(4).success(function(data){
		$scope.consigneeList = data
	})
	
}])
	
.controller('adderssEditController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	/**
	 * 获取编辑数据
	 */
	orderService.findById($stateParams.id).success(function (data) {
        $scope.consigneeInfo = data;
    });
	
	/**
	 * 更新
	 */
	$scope.updateAddress = function(){
		$scope.consigneeInfo.province = $("#sel1").val().split("-")[0];
		$scope.consigneeInfo.city = $("#sel1").val().split("-")[1];
		if($("#checkbox_c").attr("checked")){
			$scope.consigneeInfo.is_default = true
		}else{
			$scope.consigneeInfo.is_default = false
		}
		orderService.updateConsignee($scope.consigneeInfo).success(function(data){
			if(data.success){
				c.$msg(data.message,1000,"success")
			}else{
				c.$msg(data.message,1000,"error")
			}
			
			$state.go("adderss")
		})
	}
	
	$scope.back = function(){
		$state.go("adderss")
	}
	
}])


.controller('orderAbandonController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	c.$date();
	
	/**
	 * 已废弃订单列表
	 */
	$scope.pageCount = 1
	orderService.abandonOrderList().success(function(data){
		$scope.abandonOrderList = data;
		$scope.pageCount = data.totalPage;
		$scope.totalRow = data.totalRow;
	})
	
	$scope.searchAbandonOrder = function(){
		var start = $("#myStartDate").val();
    	var end = $("#myEndDate").val();
    	orderService.abandonOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:start,endDate:end}).success(function(data){
			$scope.abandonOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    }
	
	/**
	 * 翻页
	 */
	$scope.onPageChange = function () {
		orderService.abandonOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:$scope.startDate,endDate:$scope.endDate}).success(function(data){
			$scope.abandonOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    };

}])

.controller('orderProccessController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	
	
	c.$date();
	/**
	 * 已处理订单列表
	 */
	$scope.pageCount = 1
	orderService.proccessOrderList().success(function(data){
		console.log(data);
		$scope.processOrderList = data;
		$scope.pageCount = data.totalPage;
		$scope.totalRow = data.totalRow;
	})
	
	$scope.searchProcessOrder = function(){
		var start = $("#myStartDate").val();
    	var end = $("#myEndDate").val();
		orderService.proccessOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:start,endDate:end}).success(function(data){
			$scope.processOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    }
	
	/**
	 * 翻页
	 */
	$scope.onPageChange = function () {
		orderService.proccessOrderList({pageNumber:$scope.currentPage,orderCode:$scope.orderCode,startDate:$scope.startDate,endDate:$scope.endDate}).success(function(data){
			$scope.processOrderList = data;
			$scope.pageCount = data.totalPage;
			$scope.totalRow = data.totalRow;
		})
    };
}])

.controller('orderItemController',  ["$scope","$rootScope","$stateParams","$state","orderService",function ($scope,$rootScope,$stateParams,$state,orderService) {
	
	/**
	 * 计算总价
	 */
	$scope.totalPrice = function(){
		var total = 0;
		angular.forEach($scope.orderItemList, function(item) {
			total += parseFloat(item.standard_price * item.standard_count);
		})
		return total.toFixed(2)	;
	}
	/**
	 * 查看订单详情
	 */
	orderService.orderItemList($stateParams.code).success(function(data){
		$scope.orderItemList = data
		if(data){
			$scope.address = data[0].connect_address
		}
	 })
}])

appService.service('orderService', ['$http', function ($http) {
    return {
        saveConsignee:function (consigneeInfo) {
            return $http.post('/order/saveConsignee',consigneeInfo);
        },
        deleteOneOrder:function (orderCode) {
            return $http.post('/order/deleteOneOrder',{orderCode:orderCode});
        },
        deleteBacthOrder:function (ids) {
            return $http.post('/order/deleteBacthOrder',{ids:ids});
        },
        AuditList:function (query) {
            return $http.post('/order/AuditList',query);
        },
        itemAuditList:function (query) {
            return $http.post('/order/itemAuditList',query);
        },
        isDefault:function (id) {
            return $http.post('/order/isDefault/'+id);
        },
        findIsDefaultAddress:function () {
            return $http.post('/order/findIsDefaultAddress');
        },
        deleteAddress:function (id) {
            return $http.post('/order/deleteAddress/'+id);
        },
        findById:function (id) {
            return $http.post('/order/findById/'+id);
        },
        updateConsignee:function (consigneeInfo) {
            return $http.post('/order/updateConsignee/',consigneeInfo);
        },
        handleOrderList:function (query) {
            return $http.post('/order/handleOrderList',query);
        },
        proccessOrderList:function (query) {
            return $http.post('/order/proccessOrderList',query);
        },
        abandonOrderList:function (query) {
            return $http.post('/order/abandonOrderList',query);
        },
        orderItemList:function (code) {
            return $http.post('/order/findOrderId',{code:code});
        },
        cancleOrder:function (code) {
            return $http.post('/order/cancleOrder',{code:code});
        },
    }
}])