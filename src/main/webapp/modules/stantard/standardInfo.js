appController.controller('standardInfoController', ['$scope', '$state', '$stateParams', 'standardDynamicService', 'standardInfoService', 'memberService', 'conllectionService', 'standardTrackService', 'standardService',function($scope, $state, $stateParams, standardDynamicService, standardInfoService, memberService, conllectionService, standardTrackService,standardService){
	$scope.standardInfo = {}; //储存标准信息
	$scope.standardAdopt = {}; //采用
	$scope.adoptedStandard = {}; //被采用
	$scope.standarQuote = {}; //引用
	$scope.auotedStandard = {}; //被引用
	$scope.standardReplace = {}; //代替
	$scope.bereplaceStandard = {}; //被代替
	$scope.ccsList = {}; //中标值
	$scope.icsList = {}; //国际值
	
	$scope.check = function(){
		//判断用户是否登陆 来判断是否收藏 是否跟踪
		memberService.getSessionUserInfo().success(function(dataU){
			if(dataU.success){
				//检测该用户是否收藏该标准
				standardInfoService.checkCollection(dataU.object.user_name, $scope.standardInfo[0].A100).success(function(data){
					if(data.success){
						$scope.collectionText = "已收藏";
					}else{
						$scope.collectionText = "收藏";
					}
				});
				standardInfoService.checkTracking(dataU.object.user_name, $scope.standardInfo[0].A100).success(function(data){
					if(data.success){
						$scope.trackingText = "已跟踪";
					}else{
						$scope.trackingText = "跟踪";
					}
				});
			}else{
				$scope.collectionText = "收藏";
				$scope.trackingText = "跟踪";
			}
		});
	}
	
	
	/**
	 * 预览
	 */
	$scope.getElecInfo = function(identifier){
		standardInfoService.getStandardElec({code:identifier}).success(function(data){
			if(data.success){
				$scope.elec = data.object[0];
				console.log($scope.elec)
			}else{
				c.$msg(data.message, 1000, 'error');
			}
			if(data.success){
				window.open('/dynamic/getElecFileByBefore?code='+data.object[0].a100+"&filename="+data.object[0].fileName);
			}else{
				c.$msg(data.message, 1000, 'error');
			}
		})
	}
	
	/**
	 * 初始加载标准信息 代替被代替 采用被采用
	 */
	if($stateParams.id != "" || $stateParams.id != null){
		//显示加载层
		c.$load();
		standardDynamicService.getStandardInfoById($stateParams.id).success(function(data){
			if(data.success){
				//判断用户是否登陆 来判断是否收藏 是否跟踪
				$scope.check();
				//将后台获取的json字符串转换成对象数组
				$scope.standardInfo = angular.fromJson(data.object);
				/**
				 * 同一中国标准分类下的其他标准 CCS中国 ICS国际 接口规定传值
				 */
				standardInfoService.getCcsOrIcsStandard("CCS", $scope.standardInfo[0].A825).success(function(data){
					if(data.success){
						$scope.ccsList = data.object;
					}
				});
				
				/**
				 * 同一国际标准分类下的其他标准
				 */
				standardInfoService.getCcsOrIcsStandard("ICS", $scope.standardInfo[0].A826).success(function(data){
					if(data.success){
						$scope.icsList = data.object;
						//关闭加载层
						c.$closeLoad();
					}
				});
			}else{
				c.$msg(data.message, 1000, 'error');
			}
		});
		/**
		 * 代替 A461代替
		 */
		standardInfoService.getReplaceORBereplaceStandard($stateParams.id, "A461").success(function(data){
			if(data.success){
				$scope.standardReplace = data.object;
			}
		});
		/**
		 * 被代替 A462被代替
		 */
		standardInfoService.getReplaceORBereplaceStandard($stateParams.id, "A462").success(function(data){
			if(data.success){
				$scope.bereplaceStandard = data.object;
			}
		});
		/**
		 * 引用 A502引用
		 */
		standardInfoService.getQuoteORQuotedStandard($stateParams.id, "A502").success(function(data){
			if(data.success){
				$scope.standarQuote = data.object;
			}
		});
		/**
		 * 被引用 An502被引用
		 */
		standardInfoService.getQuoteORQuotedStandard($stateParams.id, "An502").success(function(data){
			if(data.success){
				$scope.auotedStandard = data.object;
			}
		});
		/**
		 * 代替 A800代替
		 */
		standardInfoService.getAdoptORAdoptedStandard($stateParams.id, "A800").success(function(data){
			if(data.success){
				$scope.standardAdopt = data.object;
			}
		});
		/**
		 * 被代替 An800被代替
		 */
		standardInfoService.getAdoptORAdoptedStandard($stateParams.id, "An800").success(function(data){
			if(data.success){
				$scope.adoptedStandard = data.object;
			}
		});
	}else{
		c.$msg('出错了，请重试！', 2000);
	}
	
	/**
	 * 收藏方法 standardId动态标识符 standardName动态名
	 */
	$scope.collection = function(standardId, standardName, text){
		if(text == "已收藏"){
			return ;
		}
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				//如果用户以及登陆执行收藏方法 data.object.id 用户id
				c.$load();
				conllectionService.save({id : data.object.id, standard_id:standardId}).success(function(dataCollection){
					if(dataCollection.success){
						//判断用户是否登陆 来判断是否收藏 是否跟踪
						$scope.check();
						c.$threeBtnConfirm("您已经将《" + standardName + "》收藏了", "查看标准收藏列表", function(){
							$state.go('standard_conllection');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	/**
	 * 跟踪方法 standardId动态标识符 standardName动态名
	 */
	$scope.tracking = function(standardId, standardName, text){
		if(text == "已跟踪"){
			return ;
		}
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				c.$load();
				//如果用户以登陆执行跟踪方法 
				standardTrackService.saveTrackingInfo(standardId).success(function(dataCollection){
					if(dataCollection.success){
						//判断用户是否登陆 来判断是否收藏 是否跟踪
						$scope.check();
						c.$threeBtnConfirm(dataCollection.message, "查看标准跟踪列表", function(){
							$state.go('standard_track');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		});
	}
	
	/**
	 * 添加一个数量
	 */
    $scope.count = 1;//默认电子文献数量为1
	$scope.plus = function(elec) {
		++$scope.count;
	}
	
	/**
	 * 减少一个数量
	 */
	$scope.minus = function(elec) {
		--$scope.count;
	}
	
	/**
	 * 加入购物车
	 */
	$scope.getCartElec = function(a001){
		memberService.getSessionUserInfo().success(function(dataUser){
			if(dataUser.success){
				$scope.title = "加入购物车";
				$('.cd-popup3').addClass('is-visible3');
				standardInfoService.getStandardElec({code:a001}).success(function(data){
					if(data.success){
						$scope.elec = data.object[0];
					}else{
						c.$msg(data.message, 1000, 'error');
					}
					/*if(data.success){
						standardInfoService.getElecFileByBefore({code : data.object[0].standardNo, filename : data.object[0].fileName}).success(function(dataElec){
						});
					}else{
						c.$msg(data.message, 1000, 'error');
					}*/
				});
			}else{
				
				$('.cd-popup4').addClass('is-visible4');
			}
		})
	}
	
	/**
	 * 点击立即购买,获取电子全文
	 */
	$scope.getElec = function(a001){
		memberService.getSessionUserInfo().success(function(dataUser){
			if(dataUser.success){
				$scope.title = "立即购买";
				$('.cd-popup3').addClass('is-visible3');
				standardInfoService.getStandardElec({code:a001}).success(function(data){
					if(data.success){
						$scope.elec = data.object[0];
					}else{
						c.$msg(data.message, 1000, 'error');
					}
					console.log(data);
					/*if(data.success){
						standardInfoService.getElecFileByBefore({code : data.object[0].standardNo, filename : data.object[0].fileName}).success(function(dataElec){
						});
					}else{
						c.$msg(data.message, 1000, 'error');
					}*/
				});
			}else{
				$('.cd-popup4').addClass('is-visible4');
			}
		})
	}
	$scope.elecInfo = function(code){
				//获取该标准电子文档信息
				standardInfoService.getStandardElec({code:code}).success(function(data){
					console.log(data);
					if(data.success){
						standardInfoService.getElecFileByBefore({code : data.object[0].standardNo, filename : data.object[0].fileName}).success(function(dataElec){
							console.log(dataElec);
						});
					}else{
						c.$msg(data.message, 1000, 'error');
					}
				});
	}

	/**
     * 立即购买
     */
    var array = [];
    $scope.instantaneousBuy = function(elec,count){
    	if(elec){
    		if($scope.title == "立即购买"){
    			elec.count = count
    			array.push(elec);
    			$state.go("shop_process",{cartList:array});
    		}else{
    			memberService.getSessionUserInfo().success(function(data){
    				if(data.success){
    					elec.count = count
    					standardService.addCart(elec).success(function(data){
    						if(data.success){
    							c.$msg(data.message,1000,"success");
    						}else{
    							c.$msg(data.message,1000,"error");
    						}
    					})
    				}else{
    					$('.cd-popup4').addClass('is-visible4');
    				}
    			});
    		}
    	}else{
    		c.$msg("没有获得当前标准号相关信息,无法加入购物车",1000,"error");
    	}
    } 
}])

appService.service('standardInfoService', ['$http', function ($http) {
	return {
		getReplaceORBereplaceStandard:function (id, type) {
    		return $http.post('/dynamic/getReplaceORBereplaceStandard', {recordStateInt : id, type : type});
    	},
    	getQuoteORQuotedStandard:function(id, type){
    		return $http.post('/dynamic/getQuoteORQuotedStandard', {recordStateInt : id, type : type});
    	},
    	getAdoptORAdoptedStandard:function(id, type){
    		return $http.post('/dynamic/getAdoptORAdoptedStandard', {recordStateInt : id, type : type});
    	},
    	getCcsOrIcsStandard:function(type, code){
    		return $http.post('/dynamic/getCcsOrIcsStandard', {type : type, code : code});
    	},
    	getStandardElec:function(query){
    		return $http.post('/dynamic/getStandardElec', query);
    	},
    	getElecFileByBefore:function(query){
    		return $http.post('/dynamic/getElecFileByBefore', query);
    	},
    	checkCollection:function(userName, code){
    		return $http.post('/dynamic/checkCollection', {userName : userName, code : code});
    	},
    	checkTracking:function(userName, code){
    		return $http.post('/dynamic/checkTracking', {userName : userName, code : code});
    	}
    	
	}
}])