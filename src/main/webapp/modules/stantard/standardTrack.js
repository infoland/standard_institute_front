appController.controller('standardTrackController', ['$scope', '$state', '$stateParams', '$cookieStore', 'standardDynamicService', 'standardInfoService', 'memberService', 'conllectionService', 'standardTrackService', function($scope, $state, $stateParams, $cookieStore, standardDynamicService, standardInfoService, memberService, conllectionService, standardTrackService){
	//用户信息
	$scope.userInfo = {};
	/**
	 * 初始获取用户信息
	 */
	memberService.getSessionUserInfo().success(function(data){
		if(data.success){
			$scope.userInfo = data.object;
		}else{
			c.$msg('您未登陆账号，请先登录！', 2000, "error");
			$state.go('main');
		}
	});
	
	/**
	 * 跟踪列表
	 */
	var list = function(currentPage){
		c.$load();
		standardTrackService.list(currentPage).success(function(data){
			if(data.success){
				$scope.pageList = data.object.list;
				$scope.pageCount = data.object.totalPage;
	    		$scope.totalRow = data.object.totalRow;
			}else{
				c.$msg(data.message, 2000, "error");
				$scope.pageList = {};
				$scope.pageCount = 1;
	    		$scope.totalRow = 0;
			}
			c.$closeLoad();
		});
	}
	list(1);
	
	/**
	 * 导出跟踪列表
	 */
	$scope.exportData = function(){
		c.$postDownloadFile('/tracking/exportTrackingList', $scope.pageList);
	}
	
	/**
	 * 主页搜索--简单搜索
	 */
	var arrs = $cookieStore.get("searchHistory") ? angular.fromJson($cookieStore.get("searchHistory")) : [];//将搜索历史添加到cookie
	$scope.search = function(){
		if($scope.condation){
			c.$load();
			arrs.push($scope.condation);
			$cookieStore.put("searchHistory",angular.toJson(arrs,true));
			$state.go("simple_search_result",{condation:$scope.condation,searchHistoryList:arrs})
		}else{
			c.$msg("请输入查询条件",1000,"error");
			return false;
		}
	}
	
	/**
	 * 翻页 由于限制是十条所以不用翻页
	 */
	/*$scope.onPageChange = function(){
		list($scope.currentPage, $scope.standardType);
	}*/
}])

appService.service('standardTrackService', ['$http', function ($http) {
	return {
		list : function(pageNumber){
			return $http.post('/tracking/findAllList', {pageNumber : pageNumber});
		},
		saveTrackingInfo : function(standard_id){
			return $http.post('/tracking/saveTrackingInfo', {standard_id : standard_id});
		},
		listUserStandardPushList : function(type){
			return $http.post('/tracking/trackingInfoPushList', {type : type});
		},
		exportData : function(dataArr){
			return $http.post('/tracking/exportTrackingList', {dataArr : dataArr});
		}
	}
}])