 appController.controller('standardValidityController',  ["memberService","$scope","$rootScope","$stateParams","$state","specialValidityService",function (memberService,$scope,$rootScope,$stateParams,$state,specialValidityService) {
 
     /**
	  * 复选框
	  */
	  $scope.toggleCheckBox = function (id) {
	     $scope.ids = c.toggleCheckBox(id);
	  }
	  
	 /**
	  * 提交
	  */
	 var temp = [];
	 var failtemp = [];
	 $scope.submit = function(){
		 memberService.getSessionUserInfo().success(function(dataUser){ 
			 if(dataUser.success){
				 specialValidityService.saveValidity($scope.standardCode).success(function(data){
					 $scope.validityList = data.object;
					 if(data.success && data.object.list8){
						 temp = data.object.list8;
					 }else{
						 c.$msg(data.message);
					 }
					 if(data.success && data.object.list9){
						 var s = data.object.list9[0].toString();
						 failtemp = s.split(",");
					 }else{
						 c.$msg(data.message);
					 }
				 })
				 $("#ss").modal('show');
				 $(".modal-backdrop").hide();//去除遮罩层
			 }else{
				 $('.cd-popup4').addClass('is-visible4');
			 }
		 })
	 }
	 
	 /**
	  * 重置
	  */
	 $scope.reset = function(){
		$scope.standardCode = "";
	 }
	 $scope.closeSuccess = function(){
		 $("#ss").modal('hide');
		 }

	 $scope.shoudong = function(){
		 $scope.title = "请输入您需要跟踪的标准号，一行请列出一条标准，区分大小写。例如：";
		 $scope.context = "ISO 11398-2012,ISO/IEC 29178-2012"; 
	 }
	 
 	/**
	 * 批量导入
	 */
	$scope.importTxt = function(){
		$scope.title = "请选择文件为txt的格式文件进行上传,文件内容格式如下(区分大小写)";
		$scope.context = "ISO 11398-2012,ISO/IEC 29178-2012";
		$scope.boo = false;
		$("#myUpload").modal('show');
		$scope.confirm = function(file,id){
			specialValidityService.uploadValidationItem(file,id).success(function(data){
				if(data.success){
					$scope.standardCode = "";
					for(var i=0;i<data.object.length;i++){
						/* modified by lihd 20171101  start ("/n"改成",")*/
						$scope.standardCode += data.object[i].standard_code+",";
						/* modified by lihd 20171101  end */
					}
					c.$msg(data.message,1000,'success');
				}else{
					c.$msg(data.message,1000,'error');
				}
				$("#myUpload").modal('hide');
			})
		}
	}
	
	/**
	 * 跳转到失败页面
	 */
	$scope.goFail = function(){
		specialValidityService.importFailValidity({record:failtemp}).success(function(data){
			if(data.success){
				c.$msg(data.message,1000,'success');
			}else{
				c.$msg(data.message,1000,'error');
			}
		})
		$state.go("fail_list",{failList:failtemp,code:$scope.standardCode});
	}
	
	/**
	 * 跳转到成功页面
	 */
	$scope.goSuccess = function(){
		specialValidityService.importSuccessValidity({record:temp}).success(function(data){
			if(data.success){
				c.$msg(data.message,1000,'success');
			}else{
				c.$msg(data.message,1000,'error');
			}
		})
		$state.go("success_list",{list:temp,code1:$scope.standardCode});
	}
	
	
	
 }])
 appController.controller('successValidityController',  ["$window","$scope","$rootScope","$stateParams","$state","specialValidityService",function ($window,$scope,$rootScope,$stateParams,$state,specialValidityService) {
	 specialValidityService.saveValidity($stateParams.code1).success(function(data){
		 if(data.success && data.object.list8){
			 $scope.successList = data.object.list8
		 }else{
			 c.$msg(data.message);
		 }
	 })
	 
	 
	$scope.exportTree  =function(){
		if(t){
			$window.localStorage["record"] = JSON.stringify(t);
			console.log($window.localStorage["record"])
			specialValidityService.exportExcel()
		}
//		specialValidityService.exportExcel({record:t}).success(function(data){
//			 if(data.success){
//				 c.$msg(data.message,1000,'success')
//			 }else{
//				 c.$msg(data.message,1000,'error')
//			 }
//		 })
	}
 }])
 
 appController.controller('failValidityController',  ["$scope","$rootScope","$stateParams","$state","specialValidityService",function ($scope,$rootScope,$stateParams,$state,specialValidityService) {
	 specialValidityService.saveValidity($stateParams.code).success(function(data){
		 if(data.success && data.object.list9){
			 var s = data.object.list9[0].toString();
			 failtemp = s.split(",");
			 $scope.list = failtemp
		 }else{
			 c.$msg(data.message); 
		 }
	 })
	 
	 
	 /**
	  *单个删除 
	  */
	 var index = -1;
	 $scope.del = function (id) {
 		c.$confirm("确认删除？", function () {
 			angular.forEach($scope.list,function(item,key){
 				if(item.id === id){
 					index = key;
 				}
 			})
 			
 			if(index !== -1){
 				$scope.list.splice(index,1);
 			}
 			
 			
         })
     }
	 
	 /**
	  * 批量删除
	  */
	 $scope.deleteAll = function(){
 		if($scope.ids !== undefined){
 			if($scope.ids.length > 0){
 				c.$confirm("确认要删除吗？",function(){
 					specialValidityService.delValidationById({id:$scope.ids}).success(function(data){
 						 if(data.success){
 							 c.$msg(data.message,1000,'success');
 						 }else{
 							 c.$msg(data.message,1000,'error');
 						 }
 					 })
 				})
 			}else{
 				c.$msg("请选择要删除的数据",1000,'error');
 			}
 		}else{
 			c.$msg("请勾选至少一条要删除的数据",1000,'error');
 		}
 	}
	 
 	/**
	 * 重试(重新调接口)
	 */
	$scope.tryAgain = function(standardCode){
		specialValidityService.saveValidity(standardCode).success(function(data){
			 $scope.validityList = data.object;
			 if(data.success){
				 c.$msg(data.message,1000,'success')
			 }else{
				 c.$msg(data.message,1000,'error')
			 }
		 })
	}
 }])

appService.service('specialValidityService', ['$http',"Upload", function ($http,Upload) {
    return {
    	saveValidity:function (standardCode) {
            return $http.post('/standard/saveValidity',{standardCode:standardCode});
        },
        getValidity:function (query) {
            return $http.post('/standard/getValidation',query);
        },
        delValidationById:function (query) {
            return $http.post('/standard/delValidationById',query);
        },
        uploadValidationItem:function(file,id){
			return  Upload.upload({
			      url: "/standard/uploadValidationItem/"+id,
			      method: 'POST',
			      data: {
			        'Content-Type': file.type === null || file.type === '' ? 'application/octet-stream' : file.type,
			        filename: file.name,
			        file: file,
			      }
			
			 });
		},
		importSuccessValidity:function (record) {
            return $http.post('/standard/importSuccessValidity',record);
        },
        importFailValidity:function (record) {
            return $http.post('/standard/importFailValidity',record);
        },
        exportExcel:function (record) {
        	$("#myForm").attr("action","/standard/exportExcel");
			$("#myForm").submit();
          // return $http.post('/standard/exportExcel',record);
        },
    }
}])