 appController.controller('standardIntroduceController',  ["$scope","$rootScope","$stateParams","$state","standardIntroduceService","standardInfoService",function ($scope,$rootScope,$stateParams,$state,standardIntroduceService,standardInfoService) {
	 /**
	  * 标准服务下数据显示
	  */
	 standardIntroduceService.findContextByService({id:$stateParams.id}).success(function(data){
		 $scope.serviceList = data
		 $scope.titleM = data[0].c_title;
		 $scope.contextDetail = data[0].c_context;
		 $scope.create_date = data[0].create_date;
		 $scope.c_source = data[0].c_source;
		 $scope.c_author = data[0].c_author;
		 for(var i = 0; i < $scope.serviceList.length; i++){
			 $scope.serviceList[i].cla = "";
		 }
		 $scope.serviceList[0].cla = "d_top_cur";
	 })
	 
	 /**
	  * 点击标题查看具体介绍内容
	  */
	 $scope.goIntroduce = function(index){
		 var id = $scope.serviceList[index].id;
		 for(var i = 0; i < $scope.serviceList.length; i++){
			 if(i == index){
				 $scope.serviceList[i].cla = "d_top_cur";
			 }else{
				 $scope.serviceList[i].cla = "";
			 }
		 }
		 angular.forEach($scope.serviceList,function(item,index,data){
			 if(id == item.id){
				 $scope.titleM = item.c_title;
				 $scope.contextDetail = item.c_context;
				 $scope.create_date = item.create_date;
				 $scope.c_source = item.c_source;
				 $scope.c_author = item.c_author;
			 }
		 })
	 }
	 
 }])

appService.service('standardIntroduceService', ['$http', function ($http) {
    return {
    	findContextByService:function (id) {
            return $http.post('/context/findContextByService',id);
        },
        findChannalByPId:function (id) {
            return $http.post('/channal/findChannalByPId',id);
        },
    }
}])