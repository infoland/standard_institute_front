appController.controller('standardDynamicController',  ["$scope","$rootScope","$stateParams","$state","standardDynamicService","standardInfoService",function ($scope,$rootScope,$stateParams,$state,standardDynamicService,standardInfoService) {
	$scope.standardType = $stateParams.id; //NEW_PUBLISH 新发布 NEW_WITHDRAW 新作废 NEW_IMPLEMEWNT 新实施
	/**
	 * 标准动态列表
	 */
	var list = function(currentPage, type){
		c.$load();
		standardDynamicService.list({pageNumber:currentPage, standard_type:type}).success(function(data){
			if(data.success){
				$scope.pageList = data.object.list;
				$scope.pageCount = data.object.totalPage;
	    		$scope.totalRow = data.object.totalRow;
			}else{
				$scope.pageList = {};
				c.$msg(data.message, 1000, "error");
			}
			c.$closeLoad();
		});
	}
	list($scope.currentPage, $stateParams.id);
	/**
	 * 根据类型获取标准动态列表
	 */
	$scope.listByType = function(type){
		$state.go('sdandard_dynamic',{'id' : type});
	}
	/**
	 * 翻页
	 */
	$scope.onPageChange = function(){
		list($scope.currentPage, $scope.standardType);
	}
}])

appService.service('standardDynamicService', ['$http', function ($http) {
	return {
		list:function (query) {
    		return $http.post('/dynamic/getStandardAvisoByType', query);
    	},
    	getStandardInfoById:function(id){
    		return $http.post('/dynamic/getStandardInfoById', {recordStateInt : id});
    	}
	}
}])