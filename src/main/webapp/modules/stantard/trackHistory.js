appController.controller('trackHistoryController', ['$scope', '$state', '$stateParams', 'standardDynamicService', 'standardInfoService', 'memberService', 'conllectionService', 'standardTrackService', 'trackHistoryService', function($scope, $state, $stateParams, standardDynamicService, standardInfoService, memberService, conllectionService, standardTrackService, trackHistoryService){
	//用户信息
	$scope.userInfo = {};
	//初始化显示div trackingInfo跟踪信息 trackingHistiry 跟踪历史
//	$scope.step == 'trackingInfo'; 
	
	c.$date();
	/**
	 * 初始获取用户信息
	 */
	memberService.getSessionUserInfo().success(function(data){
		if(data.success){
			$scope.userInfo = data.object;
		}else{
			c.$msg('您未登陆账号，请先登录！', 2000, "error");
			$state.go('main');
		}
	});
	
	/**
	 * 跟踪列表
	 */
	var list = function(){
		c.$load();
		trackHistoryService.listUserStandardPushList('PUSH').success(function(data){
			if(data.success){
				$scope.pageList = data.object.list;
				$scope.pageCount = data.object.totalPage;
	    		$scope.totalRow = data.object.totalRow;
			}else{
				$scope.pageList = {};
				$scope.pageCount = 1;
	    		$scope.totalRow = 1;
			}
			c.$closeLoad();
		});
	}
	list();
	
	/**
	 * 跟踪历史列表或跟踪信息列表
	 */
	$scope.listInfoOrListHistory = function(type){
		if(type == 'trackingInfo'){
			c.$load();
			trackHistoryService.listUserStandardPushList('PUSH').success(function(data){
				$scope.step = 'trackingInfo';
				
				if(data.success){
					$scope.pageList = data.object.list;
					$scope.pageCount = data.object.totalPage;
		    		$scope.totalRow = data.object.totalRow;
				}else{
					$scope.pageList = {};
					$scope.pageCount = 1;
		    		$scope.totalRow = 1;
				}
				c.$closeLoad();
			});
		}else if(type == 'trackingHistory'){
			c.$load();
			//清空id数据
			initArray = [];
			$scope.ids = undefined;
			$scope.startDate = "";
			$scope.endDate = "";
			trackHistoryService.getUserStandardPushListByFlag('PUSH', '', '').success(function(data){
				$scope.step = 'trackingHistory';
				if(data.success){
					$scope.pageHistoryList = data.object.list;
					$scope.pageCount = data.object.totalPage;
		    		$scope.totalRow = data.object.totalRow;
				}else{
					$scope.pageList = {};
					$scope.pageCount = 1;
		    		$scope.totalRow = 1;
				}
				c.$closeLoad();
			});
		}
	}
	
	/**
	 * 复选框
	 */
	 $scope.toggleCheckBox = function (id) {
         $scope.ids = c.toggleCheckBox(id);
     }
	 
	 /**
	  * 我知道按钮
	  */
	 var initArray = new Array();
	 $scope.iKnow = function(){
		Array.prototype.push.apply(initArray,$scope.ids);
		if(initArray !== undefined){
			if(initArray.length > 0){
				trackHistoryService.setUserStandardPushFlag(initArray).success(function(data){
					if(data.success){
						//重新获取列表
						$scope.listInfoOrListHistory('trackingInfo');
						$scope.ids = undefined;
					}else{
						c.$msg(data.message, '1000', 'error');
					}
					c.$closeLoad();
					initArray = [];
				});
			}else{
				c.$msg("请选择数据",1000,'error');
			}
		}else{
			c.$msg("请勾选至少一条数据",1000,'error');
		}
	 }
	 
	 /**
	  * 查询按钮
	  */
	 $scope.searchTrackingHistory = function(){
		 c.$load();
		 trackHistoryService.getUserStandardPushListByFlag('PUSH', $scope.startDate, $scope.endDate).success(function(data){
			$scope.step = 'trackingHistory';
			if(data.success){
				$scope.pageHistoryList = data.object.list;
				$scope.pageCount = data.object.totalPage;
	    		$scope.totalRow = data.object.totalRow;
			}else{
				$scope.pageList = {};
				$scope.pageCount = 1;
	    		$scope.totalRow = 1;
			}
			c.$closeLoad();
		});
	 }
	 var trackingHistoryArr = new Array();
	 /**
	  * 导出跟踪历史
	  */
	 $scope.exportTrackingHistory = function(){
		 Array.prototype.push.apply(initArray,$scope.ids);
		if(initArray !== undefined && initArray.length > 0){
			//循环遍历选中动态id
			angular.forEach(initArray, function(id, ind, arr){
				//根据动态id遍历到选中的对象然后将对象存入数组
				angular.forEach($scope.pageHistoryList, function(data, index, array){
					if(id == data.id){
						trackingHistoryArr.push(data);
					}
				});
			});
			c.$postDownloadFile('/tracking/exportTrackingData', trackingHistoryArr);
			//重置数据数组
			initArray = [];
			trackingHistoryArr = [];
		}else{
			c.$msg('请勾选需要导出的数据', '1000', 'error');
		}
	 }
	 
	 var trackingArr = new Array();
	 /**
	  * 导出跟踪定制
	  */
	 $scope.exportTracking = function(){
		 Array.prototype.push.apply(initArray,$scope.ids);
		if(initArray !== undefined && initArray.length > 0){
			//循环遍历选中动态id
			angular.forEach(initArray, function(id, ind, arr){
				//根据动态id遍历到选中的对象然后将对象存入数组
				angular.forEach($scope.pageList, function(data, index, array){
					if(id == data.id){
						trackingArr.push(data);
					}
				});
			});
			c.$postDownloadFile('/tracking/exportTrackingData', trackingArr);
			//重置数据数组
			initArray = [];
			trackingArr = [];
		}else{
			c.$msg('请勾选需要导出的数据', '1000', 'error');
		}
	 }
}])

appService.service('trackHistoryService', ['$http', function ($http) {
	return {
		listUserStandardPushList : function(type){
			return $http.post('/tracking/trackingInfoPushList', {type : type});
		},
		getUserStandardPushListByFlag : function(type, startDate, endDate){
			return $http.post('/tracking/trackingInfoPushFlag', {type : type, startDate : startDate, endDate : endDate});
		},
		setUserStandardPushFlag : function(ids){
			return $http.post('/tracking/trackingInfoPush', {ids : ids});
		},
		exportTrackingData : function(trackingArr){
			return $http.post('/tracking/exportTrackingData', {dataArr : trackingArr});
		},
	}
}])