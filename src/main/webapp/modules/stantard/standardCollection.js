/**
 * 标准收藏
 */
appController.controller('conllectionController', ['$scope', '$state', 'conllectionService', 'memberService', 'standardTrackService',"standardInfoService", function($scope, $state, conllectionService, memberService, standardTrackService,standardInfoService){
	//用户信息
	$scope.userInfo = {};
	/**
	 * 初始获取用户信息
	 */
	memberService.getSessionUserInfo().success(function(data){
		if(data.success){
			$scope.userInfo = data.object;
			list(1);
		}else{
			c.$msg('您未登陆账号，请先登录！', 1000, "error");
			$state.go('main');
		}
	});
	
	/**
	 * 收藏列表
	 */
	var list = function(currentPage){
		c.$load();
		conllectionService.list(currentPage).success(function(data){
			if(data.success){
				$scope.pageList = data.object.list;
				$scope.pageCount = data.object.totalPage;
	    		$scope.totalRow = data.object.totalRow;
			}else{
				$scope.pageList = {};
				$scope.pageCount = 1;
	    		$scope.totalRow = 1;
				console.log(data.message);
			}
			c.$closeLoad();
		});
	}
	
	 /**
	 * 复选框
	 */
	 $scope.toggleCheckBox = function (id) {
         $scope.ids = c.toggleCheckBox(id);
     }
	
	/**
	 * 跟踪方法 standardId动态标识符 standardName动态名
	 */
	$scope.tracking = function(standardId, standardName){
		c.$load();
		//判断用户是否登陆 
		memberService.getSessionUserInfo().success(function(data){
			if(data.success){
				//如果用户以登陆执行跟踪方法 
				standardTrackService.saveTrackingInfo(standardId).success(function(dataCollection){
					if(dataCollection.success){
						c.$threeBtnConfirm(dataCollection.message, "查看标准跟踪列表", function(){
							$state.go('standard_track');
						});
					}else{
						c.$msg(dataCollection.message, '1000', 'error');
					}
					c.$closeLoad();
				});
			}else{
				c.$confirm('您未登录账号，不能进收藏操作。是否前往主页面进行登陆？', function(){
					$state.go('main');
				});
			}
		});
	}
	
	$scope.onPageChange = function(){
		list($scope.currentPage);
	}
	/**
	 * 删除收藏
	 */
	$scope.delCollection = function(id, code){
		c.$confirm('确定要删除该条标准吗？', function(){
			c.$load();
			conllectionService.del(id, code).success(function(data){
				if(data.success){
					c.$msg(data.message, '1000', 'success');
					list($scope.currentPage);
				}else{
					c.$msg(data.message, '1000', 'error');
				}
				c.$closeLoad();
			});
		});
	}
	
	/**
	 * 批量删除
	 */
	var initArray = new Array(); //存放选中id
	var standardArr = new Array(); //存放需要删除的标准数组
	$scope.delSelect = function(){
		Array.prototype.push.apply(initArray,$scope.ids);
		if(initArray !== undefined){
			if(initArray.length > 0){
				//循环遍历选中动态id
				angular.forEach(initArray, function(id, ind, arr){
					//根据动态id遍历到选中的对象然后将对象存入数组
					angular.forEach($scope.pageList, function(data, index, array){
						if(id == data.id){
							standardArr.push(data);
						}
					});
				});
				c.$confirm("您确定要删除吗？",function(){
					c.$load();
					conllectionService.delSelect(standardArr).success(function(data){
						if(data.success){
							c.$msg(data.message, '1000', 'success');
							list($scope.currentPage);
							$scope.ids = undefined;
							$("input:checkbox").attr("checked",false);
						}else{
							c.$msg(data.message, '1000', 'error');
						}
						c.$closeLoad();
						//重置数据数组
						initArray = [];
						standardArr = [];
					});
				})
				
			}else{
				c.$msg("请选择要删除的数据",1000,'error');
			}
		}else{
			c.$msg("请勾选至少一条要删除的数据",1000,'error');
		}
	}
	
	//将数据导出为Excel
	$scope.exportDate = function(){
		Array.prototype.push.apply(initArray,$scope.ids);
		if(initArray != undefined && initArray.length > 0){
			//循环遍历选中动态id
			angular.forEach(initArray, function(id, ind, arr){
				//根据动态id遍历到选中的对象然后将对象存入数组
				angular.forEach($scope.pageList, function(data, index, array){
					if(id == data.id){
						standardArr.push(data);
					}
				});
			});
			c.$postDownloadFile('/collection/exportDate', standardArr);
//			conllectionService.exportDate(standardArr).success(function(data){
//				if(data.success){
//					c.$msg(data.message, '1000', 'success');
//					$scope.ids = undefined;
//				}else{
//					c.$msg(data.message, '1000', 'error');
//				}
//				c.$closeLoad();
//				$("input:checkbox").attr("checked",false);
//			});
			//重置数据数组
			initArray = [];
			standardArr = [];
		}else{
			c.$msg('请勾选需要导出的数据', '1000', 'error');
		}
	}
	
	
	/**
	 * 添加一个数量
	 */
    $scope.count = 1;//默认电子文献数量为1
	$scope.plus = function(elec) {
		++$scope.count;
	}
	
	/**
	 * 减少一个数量
	 */
	$scope.minus = function(elec) {
		--$scope.count;
	}
	
	/**
	 * 购买
	 */
	$scope.getElec = function(id){
		standardInfoService.getStandardElec({code:id}).success(function(data){
			if(data.success){
				$('.cd-popup3').addClass('is-visible3');
				$scope.elec = data.object[0];
			}else{
				c.$msg(data.message, 1000, 'error');
			}
		});
	}
	
	var array = [];
	var eprice = "";
    $scope.instantaneousBuy = function(elec,count){
    	console.log(elec);
    	if(elec){
    		eprice = elec.price;
			elec.count = count
			array.push(elec);
			$state.go("shop_process",{cartList:array});
    	}else{
    		c.$msg("没有获得当前标准号相关信息,无法加入购物车",1000,"error");
    	}
    }
    var ecount = "";
    $scope.buyChecked = function(){
    	var arrs = [];
		 if($scope.ids == null){
			 c.$msg("请选择您购买的文献",1000,"error");
			 return false;
		 }else{
			 for(var i=0;i < $scope.pageList.length;i++){
				 for(var j = 0;j < $scope.ids.length;j++){
					 if($scope.pageList[i].id == $scope.ids[j]){
						 $scope.pageList[i].count = 1;
						 $scope.pageList[i].price = eprice
						 arrs.push($scope.pageList[i])
						 $state.go("shop_process",{cartList:arrs})
					 }
				 }
			 }
		 }
    }
}])

appService.service('conllectionService', ['$http', function($http){
	return{
		list : function(pageNumber){
			return $http.post('/collection/getCollectList', {pageNumber : pageNumber});
		},
		save : function(query){
			return $http.post('/collection/saveCollect', query);
		},
		del : function(id, code){
			return $http.post('/collection/delCollect', {standard_id : id, standard_code : code});
		},
		delSelect : function(standardArr){
			return $http.post('/collection/delSelectCollect', {standardArr : standardArr});
		},
		exportDate : function(standardArr){
			return $http.post('/collection/exportDate', {standardArr : standardArr});
		}
	}
}])