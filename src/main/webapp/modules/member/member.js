appController.controller('memberController', ['$scope', '$rootScope', '$state', "$stateParams", '$interval', 'memberService', function($scope, $rootScope, $state, $stateParams, $interval, memberService){
	/**
	 * 会员注册
	 */
	$scope.user = {
			sex : 1,
	}; //初始化会员注册信息
	$scope.realUser = {
			sex : 1,
	}; //真实会员注册信息
	$scope.step = 1;  //控制会员注册步骤 1第一步填写用户名 密码 邮箱 2个人信息 3个人信息 4是否注册成功
	/**
	 * 会员忘记密码
	 */
	$scope.updateUser = {} //初始化会员修改信息
	$scope.realUpdateUser = {}; //真实会员修改信息
	$scope.updateStep = 1; //控制会员密码修改步骤 1第一步填写用户名 2第二部填写邮箱 3显示发送成功
	
	/**
	 * 忘记密码验证成功
	 */
	$scope.forget = {};
	
	$scope.countdown = 10; //倒计时初始化10s
	
	//时间控件
	c.$date();
	/**
	 * 获取教育字典字典列表
	 */
	memberService.listDictByType('education').success(function(data){
		$scope.educationList = data;
	});
	/**
	 * 获取证件类型字典列表
	 */
	memberService.listDictByType('id_type').success(function(data){
		$scope.idTypeList = data;
	});
	/**
	 * 会员注册
	 */
	$scope.register = function(){
		c.$load();
		memberService.registered($scope.realUser).success(function(data){
			$scope.regMessage = data;
			$scope.step = 4;
//			countdownTime();
			c.$closeLoad();
		});
	}
	
	/**
	 * 会员信息修改
	 */
	$scope.update = function(){
		c.$load();
		memberService.updateUser($scope.realUpdateUser).success(function(data){
			$scope.updateMessage = data.message;
			$scope.updateStep = 4;
//			countdownTime();
			c.$closeLoad();
		});
	}
	/**
	 * 成功后倒计时
	 */
	var countdownTime = function(){
		var timer = $interval(function(){
			$scope.countdown--;
			if($scope.countdown == 0){
				$state.go('main');
			}
		}, 1000);
	}
	
	/**
	 * 会员用户名重复检测
	 */
	$scope.checkName = function(){
		memberService.checkName($scope.user.userName).success(function(data){
			if(data.success){
				$scope.realUser = {
					user_name : $scope.user.userName,
				}
			}else{
				$scope.isCheck = false;
			}
		});
	}
	/**
	 * 注册下一步按钮
	 */
	$scope.nextStep = function(step){
		if(step == 1){
			//会员用户名重复检测
			c.$load();
			memberService.checkName($scope.user.userName).success(function(data){
				if(data.success){
					$scope.realUser.user_name = $scope.user.userName;
					$scope.realUser.user_password = $scope.user.password;
					$scope.realUser.email = $scope.user.email;
					$scope.step = 2;
				}else{
					c.$msg(data.message, 1000, "error");
				}
				c.$closeLoad();
			});
		}else if(step == 2){
			$scope.realUser.real_name = $scope.user.realName,
			$scope.realUser.company_name = $scope.user.comName,
			$scope.realUser.connect_address = $scope.user.connectAddress,
			$scope.realUser.postcode = $scope.user.postcode,
			$scope.realUser.phone_num = $scope.user.phoneNum,
			$scope.realUser.telephone = $scope.user.telePhone,
			$scope.realUser.fax = $scope.user.fax,
			$scope.realUser.province = $scope.cBox.provinceArr[$scope.province.id];
			$scope.realUser.city = $scope.cBox.cityArr[$scope.province.id][$scope.city.id];
			$scope.step = 3;
		}else if(step == 3){
			$scope.realUser.id_type_v = $scope.user.idTypeV,
			$scope.realUser.id_num = $scope.user.idNum,
			$scope.realUser.sex = $scope.user.sex,
			$scope.realUser.birth_date = $scope.user.birthDate,
			$scope.realUser.education_v = $scope.user.educationV,
			$scope.register();
		}
	}
	
	/**
     * 点击更新验证码
     */
	$scope.changeCaptcha = function(){
		var timestamp = (new Date()).valueOf();
        var imageSrc = $("#captchaImage").attr("src");
        if (imageSrc.indexOf("?") >= 0) {
            imageSrc = imageSrc.substring(0, imageSrc.indexOf("?"));
        }
        imageSrc = imageSrc + "?timestamp=" + timestamp + "&width=100&height=45&fontsize=35";
        $("#captchaImage").attr("src", imageSrc);
	}
	/**
	 * 忘记密码验证码更新
	 */
	$scope.forChangeCaptcha = function(){
		var timestamp = (new Date()).valueOf();
        var imageSrc = $("#forCaptchaImage").attr("src");
        if (imageSrc.indexOf("?") >= 0) {
            imageSrc = imageSrc.substring(0, imageSrc.indexOf("?"));
        }
        imageSrc = imageSrc + "?timestamp=" + timestamp + "&width=100&height=45&fontsize=35";
        $("#forCaptchaImage").attr("src", imageSrc);
	}
	
	/**
	 * 忘记密码下一步按钮
	 */
	$scope.nextUpdateStep = function(updateStep){
		if(updateStep == 1){
			//验证码验证
			memberService.validLoginCaptchar($scope.forget.loginCaptcha).success(function(data){
				if(data.success){
					//用户名验证
					c.$load();
					memberService.checkName($scope.forget.userName).success(function(data){
						if(data.success){
							c.$msg('用户不存在！', 1000, 'error');
						}else{
							//验证成功后跳转到第二步
							memberService.getUser($scope.forget.userName).success(function(data){
								if(data.success){
									$scope.email = data.object.email;
									$scope.updateStep = 2;
								}else{
									c.$msg(data.message, 1000, 'error');
								}
							});
						}
						c.$closeLoad();
						$scope.forChangeCaptcha();
					});
				}else{
					c.$msg(data.message, 1000, 'error');
				}
			});
		}else if(updateStep == 2){
			c.$load();
			memberService.sendUpdateEmail($scope.forget.userName, $scope.forget.email).success(function(data){
				if(data.success){
					$scope.updateStep = 3;
				}else{
					c.$msg(data.message, 1000, 'error');
				}
				c.$closeLoad();
			});
		}
	}
	
	/**
	 * 忘记密码验证成功后步骤控制
	 */
	$scope.nextUpdateSuccessStep = function(){
		memberService.successValid($stateParams.userName, $scope.updatePassword).success(function(data){
			console.log(data);
			$scope.forgetMessage = data;
			$scope.updateSuccessStep = 2;
		});
	}
	
	/**
	 * 返回首页
	 */
	$scope.goBackMain = function(){
		//移除移除定时器
		$scope.$on('destroy',function(){ 
		    $interval.cancel(timer); 
		  })
		$state.go('main');
	}
	
	$scope.reload = function(){
		$state.reload();
	}
	
	
	/**
	 * 省市二级连动
	 */
	$scope.cBox = this;
	$scope.cBox.provinceArr = provinceArr ; //省份数据
	$scope.cBox.cityArr = cityArr;    //城市数据
	$scope.cBox.getCityArr = $scope.cBox.cityArr[0]; //默认为省份
	$scope.cBox.getCityIndexArr = ['0','0'] ; //这个是索引数组，根据切换得出切换的索引就可以得到省份及城市

    //改变省份触发的事件 [根据索引更改城市数据]
	$scope.cBox.provinceChange = function(index)
    {
		$scope.cBox.getCityArr = $scope.cBox.cityArr[index] ; //根据索引写入城市数据
		$scope.cBox.getCityIndexArr[1] = '0' ; //清除残留的数据
		$scope.cBox.getCityIndexArr[0] = index ;
    }
    //改变城市触发的事件
	$scope.cBox.cityChange = function(index)
    {
		$scope.cBox.getCityIndexArr[1] = index ;
    }
}])

		
appService.service('memberService', ['$http', function($http){
	return {
		checkName:function(name){
			return $http.post('/members/loginNameCheck', {userName : name});
		},
		registered:function(user){
			return $http.post('/members/registered', {userInfo : user});
		},
		login:function(userName, password, loginCaptcha){
			return $http.post('/members/login', {username : userName, password : password, loginCaptcha : loginCaptcha});
		},
		updateUser:function(user){
			return $http.post('/members/updateUserInfo', {userInfo : user});
		},
		getUser:function(userName){
			return $http.post('/members/getUserInfo', {userName : userName});
		},
		validLoginCaptchar:function(loginCaptcha){
			return $http.post('/members/validLoginCaptcha', {loginCaptcha : loginCaptcha});
		},
		sendUpdateEmail:function(userName, email){
			return $http.post('/members/sendUpdateEmail', {userName : userName, email : email});
		},
		successValid:function(userName, updatePassword){
			return $http.post('/members/successValid', {userName : userName, updatePassword : updatePassword});
		},
		getSessionUserInfo:function(){
			return $http.post('/members/getSessionUserInfo');
		},
		listDictByType:function(typeName){
			return $http.post('/dict/listDictByType', {typeName : typeName});
		},
		exit:function(){
			return $http.post('/members/exitUser');
		}
	}
}])

