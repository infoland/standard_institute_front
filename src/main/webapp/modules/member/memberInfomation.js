appController.controller('memberInfoController', ['$scope', '$state', 'memberService', 'memberInfoService', function($scope, $state, memberService, memberInfoService){
	//储存用户信息
	$scope.userInfo = {};
	//判断是不是修改
	$scope.isUpdate = false;
	
	
	c.$date();
	/**
	 * 获取教育字典字典列表
	 */
	memberService.listDictByType('education').success(function(data){
		$scope.educationList = data;
	});
	/**
	 * 获取证件类型字典列表
	 */
	memberService.listDictByType('id_type').success(function(data){
		$scope.idTypeList = data;
	});
	
	$scope.province = {};
	$scope.city = {};
	/**
	 * 省市二级连动
	 */
	$scope.cBox = this;
	$scope.cBox.provinceArr = provinceArr ; //省份数据
	$scope.cBox.cityArr = cityArr;    //城市数据
	
	
	
	/**
	 * 初始获取用户信息
	 */
	memberService.getSessionUserInfo().success(function(data){
		if(data.success){
			$scope.userInfo = data.object;
			//遍历省
			angular.forEach($scope.cBox.provinceArr, function(dataProvinceArr, index, array){
				if(dataProvinceArr == $scope.userInfo.province){
					$scope.province.id = index;
					$scope.province.id = $scope.province.id + "";
					//这是默认省份为用户所在省份
					$scope.cBox.getCityArr = $scope.cBox.cityArr[$scope.province.id]; //默认为省份
					//遍历市
					angular.forEach($scope.cBox.getCityArr, function(dataCityArr, ind, arr){
						if(dataCityArr == $scope.userInfo.city){
							$scope.city.id = ind;
							$scope.city.id = $scope.city.id + "";
						}
					});
				}
			});
		}else{
			c.$msg('您未登陆账号，请先登录！', 2000, "error");
			$state.go('main');
		}
	});
	
	
	$scope.cBox.getCityIndexArr = ['0','0'] ; //这个是索引数组，根据切换得出切换的索引就可以得到省份及城市
	
    //改变省份触发的事件 [根据索引更改城市数据]
	$scope.cBox.provinceChange = function(index)
    {
		$scope.cBox.getCityArr = $scope.cBox.cityArr[index] ; //根据索引写入城市数据
		$scope.cBox.getCityIndexArr[1] = '0' ; //清除残留的数据
		$scope.cBox.getCityIndexArr[0] = index ;
    }
    //改变城市触发的事件
	$scope.cBox.cityChange = function(index)
    {
		$scope.cBox.getCityIndexArr[1] = index ;
    }
	/**
	 * 修改用户信息
	 */
	$scope.update = function(){
		c.$load();
		$scope.userInfo.province = $scope.cBox.provinceArr[$scope.province.id];
		$scope.userInfo.city = $scope.cBox.cityArr[$scope.province.id][$scope.city.id];
		memberService.updateUser($scope.userInfo).success(function(data){
			if(data.success){
				c.$msg(data.message, 1000, 'success');
				$scope.isUpdate = false;
				$state.reload();
			}else{
				c.$msg(data.message, 1000, 'error');
			}
			c.$closeLoad();
		});
	}
	
	/**
	 * 跳转修改密码页面
	 */
	$scope.goUpdatePassword = function(){
		$state.go('update_password');
	}
	
	/**
	 * 修改密码
	 */
	$scope.updateUserPassword = function(){
		c.$load();
		memberInfoService.checkPassword($scope.userInfo.user_name, $scope.oldPassword).success(function(data){
			if(data.success){
				$scope.userInfo.user_password = $scope.newPassword;
				memberService.updateUser($scope.userInfo).success(function(data){
					if(data.success){
						c.$msg('密码修改成功！', 1000, 'success');
						$state.go('register_information');
					}else{
						c.$msg('密码修改失败！', 1000, 'error');
					}
				});
			}else{
				c.$msg(data.message, 1000, 'error');
			}
			c.$closeLoad();
		});
	}
	
	$scope.reseatInfo = function(){
		$state.reload();
	}
	
	/**
	 * 用户退出登陆
	 */
	$scope.exitUser = function(){
		memberService.exit().success(function(data){
			if(data.success){
				c.$msg(data.message, '2000', 'success');
				$state.go('main');
			}
		});
	}
}])

appService.service('memberInfoService', ['$http', function($http){
	return {
		checkPassword:function(userName, password){
			return $http.post('/members/checkPassword', {userName : userName, password: password});
		},
	}
}])
