#namespace("updateSql")

	#sql("updateSelect")
		update `bzy_order` set flag = #p(flag) and flag_v  = #p(flagV) where id = #p(id)
	#end
	
#end


#namespace("childOrderSql")

	#sql("childOrderSelect")
		select * from  bzy_order_item where  order_id = #p(orderId)
	#end
	
#end

#namespace("bzyOrdersql")

	#sql("bzyOrderSelect")
		select *
	#end
	
	#sql("bzyOrderFrom")
		from bzy_order where flag=1 and flag_v = '交易中' and pay_flag = 0	and pay_flag_v = '未支付' and user_info_id = #p(userInfoId)
		
		#if(code != null)
			and order_code like #p(code)
		#end
		
		
		#if(startDate != null && endDate != null)
			and order_date between #p(startDate) and #p(endDate)
		#end
		
		order by order_date desc
	#end
	
#end

#namespace("payOrdersql")

	#sql("payOrderSelect")
		select *
	#end
	
	#sql("payOrderFrom")
		from bzy_order where flag=2  and flag_v = '已完成' and pay_flag = 1 and pay_flag_v = '已支付'	and user_info_id = #p(userInfoId)
		
		#if(code != null)
			and order_code like #p(code)
		#end
		
		
		#if(startDate != null && endDate != null)
			and order_date between #p(startDate) and #p(endDate)
		#end
		
		order by order_date desc
	#end
	
#end


#namespace("abandonOrdersql")

	#sql("abandonOrderSelect")
		select *
	#end
	
	#sql("abandonOrderFrom")
		from bzy_order where flag = 0  and flag_v = '交易关闭' 	and pay_flag = 2 and pay_flag_v = '已废弃' and user_info_id = #p(userInfoId)
		
		#if(code != null)
			and order_code like #p(code)
		#end
		
		#if(payFlagV != null)
			and pay_flag_v like #p(payFlagV)
		#end
		
		#if(startDate != null && endDate != null)
			and order_date between #p(startDate) and #p(endDate)
		#end
		
		order by order_date desc
	#end
	
#end
#namespace("upOrdersql")

	#sql("upOrderSelect")
		select *
	#end
	
	#sql("upOrderFrom")
		from bzy_order where flag=0  and flag_v = '交易关闭' 	
		
		#if(orderCode != null)
			and order_code like #p(orderCode)
		#end
		
		#if(payFlagV != null)
			and pay_flag_v like #p(payFlagV)
		#end
		
		#if(startDate != null && endDate != null)
			and order_date between #p(startDate) and #p(endDate)
		#end
		
		order by order_date desc
	#end
	
#end

#namespace("detailOrdersql")

	#sql("detailOrderSelect")
		select o.order_code,o.order_date,o.flag_v,o.user_name,
        o.total_amount,oi.standard_code, oi.standard_name,oi.standard_count,oi.payment_time,
		oi.standard_price,ci.consignee_name,ci.connect_address,ci.telephone 
		from  bzy_order o,bzy_order_item oi,bzy_consignee_info ci where o.order_code = oi.order_code and o.consignee_id = ci.`id` 
  		and o.order_code = #p(orderCode)
		order by o.order_date desc 
	#end
	
#end

#namespace("orderItemSql")

	#sql("orderItemSelect")
		select * from bzy_order_item where order_code = #p(orderCode)
	#end
	
#end

#namespace("orderSelect")

	#sql("mainOrderSelect")
		select * from bzy_order where order_code = #p(orderCode)
	#end
	
#end



