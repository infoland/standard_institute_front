
package com.infoland.app;

import java.io.File;

import com.infoland.modules.cms.model.Adver;
import com.infoland.modules.cms.model.AdverCategory;
import com.infoland.modules.cms.model.Channal;
import com.infoland.modules.cms.model.Context;
import com.infoland.modules.cms.model.Navigation;
import com.infoland.modules.cms.model.NavigationCategory;
import com.infoland.modules.main.model.Admin;
import com.infoland.modules.main.model.AdminLog;
import com.infoland.modules.main.model.Configuation;
import com.infoland.modules.main.model.Icon;
import com.infoland.modules.main.model.Menu;
import com.infoland.modules.main.model.Permission;
import com.infoland.modules.main.model.Role;
import com.infoland.modules.maintenance.model.CopyRight;
import com.infoland.modules.maintenance.model.Dict;
import com.infoland.modules.maintenance.model.FriendlinkClasses;
import com.infoland.modules.maintenance.model.FriendlinkInfo;
import com.infoland.modules.maintenance.model.HotWords;
import com.infoland.modules.maintenance.model.NewsCarousel;
import com.infoland.modules.maintenance.model.TaskInfo;
import com.infoland.modules.members.model.CollectionInfo;
import com.infoland.modules.members.model.ConfirmHistory;
import com.infoland.modules.members.model.ConsigneeInfo;
import com.infoland.modules.members.model.TrackingInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.orders.model.Cart;
import com.infoland.modules.orders.model.Order;
import com.infoland.modules.orders.model.OrderItem;
import com.infoland.modules.standardClass.model.ChinastandardClasses;
import com.infoland.modules.standardClass.model.InterstandardClasses;
import com.infoland.modules.standardClass.model.SpecialstandarClasses;
import com.infoland.modules.standardClass.model.UpdatestandardClass;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

public class MappingKit {

	private static final String SQL_TEMPLATE_PATH = "sql";

	private static final String SQL_TEMPLATE_MODULE_STANDARD_PATH = "standard";
	private static final String SQL_TEMPLATE_MODULE_ORDERS_PATH = "orders";
	private static final String SQL_TEMPLATE_MODULE_CHANNAL_PATH = "channal";
	private static final String SQL_TEMPLATE_MODULE_CONTEXT_PATH = "context";
    private static final String SQL_TEMPLATE_MODULE_TRACKING_PATH = "trackingInfo";
    private static final String SQL_TEMPLATE_MODULE_CONSIGNEEINFO_PATH = "consigneeInfo";
    private static final String SQL_TEMPLATE_MODULE_CART_PATH = "cart";
    private static final String SQL_TEMPLATE_MODULE_DICT_PATH = "dict";
    private static final String SQL_TEMPLATE_MODULE_MEMBER_PATH = "member";
    private static final String SQL_TEMPLATE_MODULE_COLLECTION_PATH = "collect";
    private static final String SQL_TEMPLATE_MODULE_HOTWORDS_PATH = "hotWords";
    private static final String SQL_TEMPLATE_MODULE_COPYRIGHT_PATH = "copyRight";
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("si_admin", "id", Admin.class);
		arp.addMapping("si_admin_log", "id", AdminLog.class);
		arp.addMapping("si_configuation", "id", Configuation.class);
		arp.addMapping("si_dict", "id", Dict.class);
		arp.addMapping("si_menu", "id", Menu.class);
		arp.addMapping("si_permission", "id", Permission.class);
		arp.addMapping("si_role", "id", Role.class);
		arp.addMapping("si_icon", "id", Icon.class);
		
		arp.addMapping("bzy_adver", "id", Adver.class);
		arp.addMapping("bzy_adver_category", "id", AdverCategory.class);
		arp.addMapping("bzy_channal", "id", Channal.class);
		arp.addMapping("bzy_cart_info", "id", Cart.class);
		arp.addMapping("bzy_chinastandard_classes", "id", ChinastandardClasses.class);
		arp.addMapping("bzy_collection_info", "id", CollectionInfo.class);
		arp.addMapping("bzy_confirm_history", "id", ConfirmHistory.class);
		arp.addMapping("bzy_copy_right", "id", CopyRight.class);
		arp.addMapping("bzy_consignee_info", "id", ConsigneeInfo.class);
		arp.addMapping("bzy_context", "id", Context.class);
		arp.addMapping("bzy_friendlink_classes", "id", FriendlinkClasses.class);
		arp.addMapping("bzy_friendlink_info", "id", FriendlinkInfo.class);
		arp.addMapping("bzy_hot_words", "id", HotWords.class);
		arp.addMapping("bzy_interstandard_classes", "id", InterstandardClasses.class);
		arp.addMapping("bzy_navigation", "id", Navigation.class);
		arp.addMapping("bzy_navigation_category", "id", NavigationCategory.class);
		arp.addMapping("bzy_news_carousel", "id", NewsCarousel.class);
		arp.addMapping("bzy_order", "id", Order.class);
		arp.addMapping("bzy_order_item", "id", OrderItem.class);
		arp.addMapping("bzy_specialstandar_classes", "id", SpecialstandarClasses.class);
		arp.addMapping("bzy_task_info", "id", TaskInfo.class);
		arp.addMapping("bzy_tracking_info", "id", TrackingInfo.class);
		arp.addMapping("bzy_updatestandard_class", "id", UpdatestandardClass.class);
		arp.addMapping("bzy_user_info", "id", UserInfo.class);
		sqlMapping(arp);
	}

	private static void sqlMapping(ActiveRecordPlugin arp) {
		arp.setBaseSqlTemplatePath(PathKit.getRootClassPath() + File.separator + SQL_TEMPLATE_PATH);
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_STANDARD_PATH + "/standard.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_ORDERS_PATH + "/order.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_TRACKING_PATH + "/trackingInfo.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_CHANNAL_PATH + "/channal.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_CONTEXT_PATH + "/context.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_CONSIGNEEINFO_PATH  + "/consigneeInfo.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_CART_PATH  + "/cart.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_DICT_PATH  + "/dict.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_MEMBER_PATH + "/member.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_COLLECTION_PATH + "/collection.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_HOTWORDS_PATH + "/hotWords.sql");
		arp.addSqlTemplate(SQL_TEMPLATE_MODULE_COPYRIGHT_PATH + "/copyRight.sql");
	}
}
