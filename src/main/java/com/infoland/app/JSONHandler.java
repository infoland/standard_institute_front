package com.infoland.app;


import com.jfinal.handler.Handler;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Created by c on 2017/3/28.
 */
public class JSONHandler extends Handler {

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        String contentType = request.getContentType();
        if (StrKit.notBlank(contentType) && contentType.contains("application/json")) {
            request.setAttribute("body", HttpKit.readData(request));
        }
        next.handle(target, request, response, isHandled);
    }
}
