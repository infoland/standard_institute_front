
/**   
 * @Title: JFinalModelCase.java 
 * @Package: com.aft.jlgh.modules.test 
 * @Description: TODO
 * @author Mandarava  
 * @date 2016年5月18日 下午2:21:02 
 * @version 1.3.1 
 * @Email wuyan1688@qq.com
 */

package com.infoland.app;

import java.util.List;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;

/**
 * JFinal 针对Model的单元测试
 * 
 * @author Mandarava
 * @date 2016年5月18日 下午2:21:02
 * @version V1.3.1
 */

public class JFinalModelCase {

	protected static DruidPlugin dp;
	protected static ActiveRecordPlugin activeRecord;

	/**
	 * 数据连接地址
	 */
	private static final String URL = "jdbc:mysql://60.205.216.138:3307/ccb_dice?characterEncoding=UTF8&zeroDateTimeBehavior=convertToNull";

	/**
	 * 数据库账号
	 */
	private static final String USERNAME = "root";

	/**
	 * 数据库密码
	 */
	private static final String PASSWORD = "root@1234";

	/**
	 * 数据库驱动
	 */
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	/**
	 * 数据库类型（如mysql，oracle）
	 */
	private static final String DATABASE_TYPE = "mysql";

	/**
	 * @throws java.lang.Exception
	 */
	// @BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dp = new DruidPlugin(URL, USERNAME, PASSWORD, DRIVER);

		dp.addFilter(new StatFilter());

		dp.setInitialSize(3);
		dp.setMinIdle(2);
		dp.setMaxActive(5);
		dp.setMaxWait(60000);
		dp.setTimeBetweenEvictionRunsMillis(120000);
		dp.setMinEvictableIdleTimeMillis(120000);

		WallFilter wall = new WallFilter();
		wall.setDbType(DATABASE_TYPE);
		dp.addFilter(wall);

		dp.getDataSource();
		dp.start();

		activeRecord = new ActiveRecordPlugin(dp);
		activeRecord.setDialect(new MysqlDialect()).setDevMode(true).setShowSql(true) // 是否打印sql语句
		;

		// 映射数据库的表和继承与model的实体
		// 只有做完该映射后，才能进行junit测试
		// MappingKit.mapping(activeRecord);

		activeRecord.start();
	}

	public static void main(String[] args) throws Exception {
		setUpBeforeClass();
//		 for (int i = 0; i < 1000; i++) {
//		 Record wechat = new Record();
//		 wechat.set("w_nickname", "user"+i);
//		 wechat.set("createDate", new Date());
//		wechat.set("modifyDate", new Date());
//		 Db.save("d_wechat", wechat);
//		 }
//		
//		 String sql = "select * from d_wechat where id not in ('113','114')";
//		 List<Record> wechat = Db.find(sql);
//		 int i = 0;
//		 for (Record record : wechat) {
//		 Record user = new Record();
//		 user.set("createDate", new Date());
//		 user.set("d_user_name", record.get("w_nickname"))
//		 .set("d_wechat_id", record.get("id"))
//		 .set("d_game_frequency_sum", 100)
//		 .set("d_play_game_frequency", 100)
//		 .set("d_phone", 1375610020+(i++));
//		 Db.save("d_user", user);
//		 }

		String sql1 = "SELECT * FROM d_user WHERE d_play_game_frequency >0 AND id > 76";
		final List<Record> wechat1 = Db.find(sql1);
		String url = "http://192.168.3.10:8080/game/startGame";

		url = "http://www.changcl.com/game/gameCache";
		String res = HttpKit.get(url);
		System.out.println(res + wechat1);
//		for (final Record record : wechat1) {
//			final Integer canNum = record.getInt("d_game_frequency_sum");
////			for (int j = 0; j < 5; j++) {
////				new Thread() {
////					@Override
////					public void run() {
//						for (int z = 0; z < canNum; z++) {
//
//							Map<String, String> headers = new HashMap<String, String>();
//							headers.put("Cookie", "loginUser=" + record.getInt("d_wechat_id"));
//							String result = HttpKit.post(url, "", headers);
//							System.out.println(result);
//						}
//
//					}
////				}.start();
////				;
////			}
////		}
//		
//		sql1 = "SELECT d.id ,d.user_id,(SELECT w.id FROM d_wechat w JOIN d_user u ON w.`id` = u.`d_wechat_id` WHERE u.`id` = d.user_id) wId FROM d_game_details  d";
//		List<Record> gameList = Db.find(sql1);
//		for (Record record : gameList) {
//			Map<String, String> headers = new HashMap<String, String>();
//			headers.put("Cookie", "loginUser=" + record.getInt("wId"));
//			String result = HttpKit.post(url, "gdId="+record.getInt("id"), headers);
//			System.out.println(result);
//		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	// @After
	// public void tearDown() throws Exception {
	// activeRecord.stop();
	// dp.stop();
	// }

}
