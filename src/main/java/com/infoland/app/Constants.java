
/**   
 * @Title: Constants.java 
 * @Package: com.aft.jlgh.app 
 * @Description: TODO
 * @author Mandarava  
 * @date 2016年5月17日 上午9:38:26 
 * @version 1.3.1 
 * @Email wuyan1688@qq.com
 */

package com.infoland.app;

import com.jfinal.kit.PathKit;

/**
 * 系统常量表
 * 
 * @author Mandarava
 * @date 2016年5月17日 上午9:38:26
 * @version V1.3.1
 */

public final class Constants {

	/**
	 * jdbc配置文件
	 */
	public static final String RES_JDBC_FILE_NAME = "jdbc.properties";

	/**
	 * 数据源连接地址
	 */
	public static final String JDBC_URL = "jdbc.url";

	/**
	 * 数据库用户名
	 */
	public static final String JDBC_USER_NAME = "jdbc.username";

	/**
	 * 数据库密码
	 */
	public static final String JDBC_PASSWORD = "jdbc.password";

	/**
	 * 数据库驱动
	 */
	public static final String JDBC_DRIVER_CLASS = "jdbc.driverClass";

	/**
	 * 数据库最大连接数
	 */
	public static final String JDBC_MAX_ACTIVE = "jdbc.maxActive";

	/**
	 * 过滤器
	 */
	public static final String JDBC_FILTERS = "jdbc.filters";

	/**
	 * 是否打印sql
	 */
	public static final String IS_SHOW_SQL = "showsql";

	public static final String DEBUG = "debug";

	public static final String REDIS_DB = "redis.db";

	public static final String REDIS_DB_PASSWORD = "redis.db.password";

	public static final String REDIS_IP = "redis.redis.ip";

	public static final String REDIS_PORT = "redis.redis.port";

	public static final String SUPER_MANAGER_ROLE = "ADMIN_ROLE,SYS_ADMIN";

	/**
	 * ecache 全局公用缓存名<br>
	 * 用于存放公共信息，<br>
	 * 如：字典等
	 */
	public static final String GLOBAL_CACHE_NAME = "common";

	public static final String LOGIN_URL = "plugn.shiro.loginUrl";

	public static final String SUCCESS_URL = "plugn.shiro.successUrl";

	public static final String UNAUTHURL = "plugn.shiro.unauthorizedUrl";

	/**
	 * beetl &shiro 自定义函数名
	 */
	public static final String BEETL_FUNC_SHIRO_NAME = "shiro";

	/**
	 * shiro 的 guest标签 ：验证当前用户是否为“访客”，即未认证（包含未记住）的用户。
	 */
	public static final String BEETL_SHIRO_GUEST_TAG_NAME = "guest";

	/**
	 * shiro 的user标签 ：认证通过或已记住的用户。
	 */
	public static final String BEETL_SHIRO_USER_TAG_NAME = "user";

	/**
	 * shiro 的authenticated标签 ：已认证通过的用户。不包含已记住的用户，这是与user标签的区别所在。
	 */
	public static final String BEETL_SHIRO_AUTHENTICATED_TAG_NAME = "authenticated";

	/**
	 * shiro 的notAuthenticated标签
	 * ：未认证通过用户，与authenticated标签相对应。与guest标签的区别是，该标签包含已记住用户。
	 */
	public static final String BEETL_SHIRO_NOTAUTHENTICATED_TAG_NAME = "notAuthenticated";

	/**
	 * shiro 的principal 标签 ：输出当前用户信息，通常为登录帐号信息。
	 */
	public static final String BEETL_SHIRO_PRINCIPAL_TAG_NAME = "principal";

	/**
	 * shiro 的hasRole标签 ：验证当前用户是否属于该角色。
	 */
	public static final String BEETL_SHIRO_HASROLE_TAG_NAME = "hasRole";

	/**
	 * shiro 的lacksRole标签 ：与hasRole标签逻辑相反，当用户不属于该角色时验证通过。
	 */
	public static final String BEETL_SHIRO_LACKSROLE_TAG_NAME = "lacksRole";

	/**
	 * shiro 的hasAnyRole标签 ：验证当前用户是否属于以下任意一个角色。
	 */
	public static final String BEETL_SHIRO_HASANYROLES_TAG_NAME = "hasAnyRoles";

	/**
	 * shiro 的hasPermission标签 ：验证当前用户是否拥有指定权限。
	 */
	public static final String BEETL_SHIRO_HASPERMISSION_TAG_NAME = "hasPermission";

	/**
	 * shiro 的lacksPermission标签 ：与hasPermission标签逻辑相反，当前用户没有制定权限时，验证通过。
	 */
	public static final String BEETL_SHIRO_LACKSPERMISSION_TAG_NAME = "lacksPermission";
	
	public static final String V2 = "v2";
	
	
	public static final String TEMP_DIRECTORY = PathKit.getWebRootPath() + "/" + "temp/";
	
	public static final String SESSION_NAME = "loginUser";
	
	
	public static final String EMAIL_HOST = "smtp.163.com"; //网易163邮箱的 SMTP 服务器地址为
    public static final String EMAIL_PROTOCOL = "smtp";   //使用的协议（JavaMail规范要求）
    /**
     *  SMTP 服务器的端口 (非 SSL 连接的端口一般默认为 25, 可以不添加, 如果开启了 SSL 连接,
     *                    需要改为对应邮箱的 SMTP 服务器的端口, 具体可查看对应邮箱服务的帮助,
     *                    QQ邮箱的SMTP(SLL)端口为465或587, 其他邮箱自行去查看)
     */
    public static final String EMAIL_PORT = "25";
    public static final String EMAIL_FROM = "546799220@163.com"; //发件人的email
    public static final String EMAIL_FROM_ALIAS = "鹰路科技";
    public static final String EMAIL_PWD = "a123456"; //发件人密码
    public static final String EMAIL_THEME = "标准文献检索平台——密码重置验证";
    
    /**
     * 标准检索存放到cookie中的id和文献名称
     */
    public static final String SEARCH_NAME = "historys";
    
}
