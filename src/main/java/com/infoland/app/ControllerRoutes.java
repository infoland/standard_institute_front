
package com.infoland.app;

import com.infoland.modules.cms.controller.ChannalController;
import com.infoland.modules.cms.controller.ContextController;
import com.infoland.modules.cms.controller.NavigationController;
import com.infoland.modules.main.controller.AdminController;
import com.infoland.modules.maintenance.controller.CopyRightController;
import com.infoland.modules.maintenance.controller.DictController;
import com.infoland.modules.members.controller.CollectionController;
import com.infoland.modules.members.controller.ConfirmController;
import com.infoland.modules.members.controller.LiteratureController;
import com.infoland.modules.members.controller.MembersController;
import com.infoland.modules.members.controller.TrackingController;
import com.infoland.modules.orders.controller.CartController;
import com.infoland.modules.orders.controller.OrderController;
import com.infoland.modules.search.controller.SearchController;
import com.infoland.modules.standardClass.controller.StandardClassController;
import com.infoland.modules.standardDynamic.controller.StandardDynamicController;
import com.jfinal.config.Routes;

public class ControllerRoutes extends Routes {

	@Override
	public void config() {
		add("/", AdminController.class);
		add("/dict", DictController.class);
		add("/standard", StandardClassController.class);
		add("/members",MembersController.class);
		add("/order", OrderController.class);
		add("/cms", NavigationController.class);
		add("/literature", LiteratureController.class);
		add("/collection",CollectionController.class);
		add("/confirm",ConfirmController.class);
		add("/tracking",TrackingController.class);
		add("/channal",ChannalController.class);
		add("/context",ContextController.class);
		add("/dynamic",StandardDynamicController.class);
		add("/cart",CartController.class);
		add("/search",SearchController.class);
		add("/copyRight",CopyRightController.class);
		
	}

}