
package com.infoland.app;

import javax.servlet.http.HttpServletRequest;

import com.infoland.modules.basic.SystemConfiguration;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.template.Engine;

public class AppConfig extends JFinalConfig {

	Routes routes;
	
	@Override
	public void configRoute(Routes routes) {
		this.routes = routes;
		routes.add(new ControllerRoutes());
	}

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("jdbc.properties");
		me.setDevMode(getPropertyToBoolean("debug"));
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new DruidStatViewHandler("/druid", new IDruidStatViewAuth() {
			@Override
			public boolean isPermitted(HttpServletRequest arg0) {
				return true;
			}
		}));
		me.add(new JSONHandler());
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configPlugin(Plugins me) {
		/*druid数据源*/
		String jdbcUrl = getProperty("jdbc.url");
		String jdbcUser = getProperty("jdbc.username");
		String jdbcPassword = getProperty("jdbc.password");
		String jdbcDriver = getProperty("jdbc.driver");
		String jdbcFilter = getProperty("jdbc.filters");
		Integer maxActive = getPropertyToInt("jdbc.maxActive");
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, jdbcUser, jdbcPassword, jdbcDriver, jdbcFilter);
		druidPlugin.setMaxActive(maxActive);
		me.add(druidPlugin);
		/*arp*/
		boolean showSql = getPropertyToBoolean("showSql");
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setShowSql(showSql);
		
		MappingKit.mapping(arp);
		me.add(arp);
	}

	@Override
	public void configEngine(Engine me) {
		me.addSharedMethod(new StrKit());
	}

	@Override
	public void afterJFinalStart() {
		SystemConfiguration.initConfiguration();
	}
	
}
