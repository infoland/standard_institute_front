package com.infoland.common.util;

import com.infoland.modules.basic.SystemConfiguration;

import ws.client.Cniswsbase;
import ws.client.CniswsbasePortType;

/**
 * 中标院接口调用工具类
 * @author lixp
 * @time 20170725 
 *
 */
public class CniswsUtil {
	
	/**
	 * 此方法用于返回直接调用方法之前的对象
	 * 使用时需要try catch
	 * @return
	 */
	public static CniswsbasePortType getCniswsbasePortType() {
		// 创建CniswsbasePortType对象，用来调用中标院接口
		Cniswsbase cniswsbase = new Cniswsbase();
		CniswsbasePortType cpt = cniswsbase.getCniswsbaseSOAP11PortHttp();
		return cpt;
	}
	
	/**
	 * 此方法用于返回调用接口参数中的用户名
	 * @return
	 */
	public static String getUserName() {
		return SystemConfiguration.getMap().get("userName");
	}
	
	/**
	 * 此方法用户返回调用接口参数中的密码
	 * @return
	 */
	public static String getPassword() {
		return SystemConfiguration.getMap().get("password");
	}
	
	/**
	 * 此方法用于返回调用接口参数中的每页显示条数
	 * @return
	 */
	public static String getPageSize() {
		return SystemConfiguration.getMap().get("pageSize");
	}

	/**
	 * 
	 * @Description  用于返回企业码
	 * @author Wsy
	 * @return
	 */
	public static String getOrgCode() {
		return SystemConfiguration.getMap().get("orgCode");
	}
	
	public static String getWsdl() {
		return SystemConfiguration.getMap().get("wsdl");
	}
	
	public static String getNameSpace() {
		return SystemConfiguration.getMap().get("nameSpace");
	}
}
