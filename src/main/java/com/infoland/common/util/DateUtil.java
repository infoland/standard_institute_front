package com.infoland.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jfinal.plugin.activerecord.Db;

public class DateUtil {

	/**
	 * 当前时间加任意几分钟
	 * 
	 * @author Mandarava
	 * @param minute
	 * @return
	 */
	public static Date addArbitrarilyMinute(int minute) {
		long currentTime = System.currentTimeMillis();
		currentTime += minute * 60 * 1000;
		Date date = new Date(currentTime);
		return date;
	}

	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static String formatTime(String date) {
		SimpleDateFormat sdfhsm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdfhsm.format(date);
	}

	/**
	 * 参数(0,1)，就是当月第一天，(1,0)，就是当月最后一天,(1,1)就是下个月的1号
	 * 
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getDay(Integer month, Integer day) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(c.getTime());
	}

	/**
	 * 返回当前时间毫秒
	 * 
	 * @author Mandarava
	 * @return
	 */
	public static Long nowTime() {
		return new Date().getTime();
	}

	/**
	 * 返回两个时间相差的秒数
	 * 
	 * @author Mandarava
	 * @param start
	 * @param end
	 * @return
	 */
	public static Integer minus(Long start, Long end) {
		if (start == null || end == null)
			return -1;
		if (end > start) {
			return (int) ((end - start) / 1000);
		} else if (start > end) {
			return (int) (start - end) / 1000;
		}
		return 0;
	}

	/**
	 * * 例如想获得2015-12-30的6个月后的日期，参数就是(0,6,0,2015-12-30)，返回的就是2016-06-30
	 * 如果参数day不为0，会在当前日期+day天，如果参数是(0,6,3,2015-12-30)，返回的结果就会是2016-07-03
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param date
	 * @return
	 */
	public static String getDate(Integer year, Integer month, Integer day, Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, year);
		c.add(Calendar.MONTH, month);
		c.add(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(c.getTime());
	}

	/**
	 * 传进来的date加对应的年月日
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param date
	 * @return
	 */
	public static String addDay(Integer year, Integer month, Integer day, Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, year);
		c.add(Calendar.MONTH, month);
		c.add(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(c.getTime());
	}

	/**
	 * 获取今日开始时间
	 * 
	 * @return
	 */
	public static String nowDateBegin() {
		return "'" + now() + " 00:00:00'";
	}

	/**
	 * 获取今日结束时间
	 * 
	 * @return
	 */
	public static String nowDateEnd() {
		return "'" + now() + " 23:59:59'";
	}

	/**
	 * 系统当前事件格式为(yyyy-mm-dd)
	 * 
	 * @return
	 */
	public static String now() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 系统当前事件格式为(yyyy-MM-dd HH:mm:ss)
	 * 
	 * @return
	 */
	public static String nowYMDHMS() {
		Date date = new Date();
		SimpleDateFormat sdfhsm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdfhsm.format(date);
	}

	public static Date date() {
		return new Date();
	}

	public static java.sql.Timestamp sqlDate() {
		return new java.sql.Timestamp(date().getTime());
	}

	public static java.sql.Timestamp sqlTime(Date data) throws ParseException {
		return new java.sql.Timestamp(data.getTime());
	}

	public static java.sql.Timestamp sqlTime(String data) throws ParseException {
		SimpleDateFormat sdfhsm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return new java.sql.Timestamp(sdfhsm.parse(data).getTime());
	}

	public static java.sql.Timestamp sqlTimestamp() {
		return new java.sql.Timestamp(date().getTime());
	}

	/**
	 * 根据时间戳返回字符串
	 */
	public static String getTimestamp() {
		String formt = "yyyyMMddHHmmssSSS";
		SimpleDateFormat sdf = new SimpleDateFormat(formt);
		return sdf.format(new Date());
	}

	/**
	 * 根据 数据库函数返回字符串
	 */
	public static String nextval(String key) {
		Integer num = Db.queryInt("select nextval(?)", key);
		return num.toString();
	}

	/**
	 * 计算出两个时间相差多少天
	 * 
	 * @param smdate
	 *            开始时间
	 * @param bdate
	 *            结束时间
	 * @return
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate) {
		if (smdate == null || bdate == null)
			return 0;
		long between_days = (bdate.getTime() - smdate.getTime()) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	}

	public static int getYear() {
		Calendar a = Calendar.getInstance();
		return a.get(Calendar.YEAR);
	}

	/**
	 * 字符串的日期格式的计算
	 */
	public static int daysBetween(String smdate, String bdate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long between_days = 0;
		try {
			between_days = (sdf.parse(bdate).getTime() - sdf.parse(smdate).getTime()) / (1000 * 3600 * 24);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * @author Mr.Ni
	 * @Title: findYearMonth
	 * @Description: 获取年月
	 * @return String
	 */
	public static String findYearMonth() {
		int year;
		int month;
		String date;
		Calendar calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH) + 1;
		date = year + "" + (month < 10 ? "0" + month : month);
		return date;
	}

	public static Date formatStringToDate(String source) {
		SimpleDateFormat sdfhsm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = sdfhsm.parse(source);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String lastMonthMaxDay(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return sdf.format(c.getTime());
	}
	
	/**
	 * 传进来的时间加或减天数
	 * 
	 * @author lixp
	 * @param date    传进来的时间
	 * @param day     若正为传值时间后，若负为传值时间前
	 * @return Date   返回更改后的时间
	 */
	public static Date getUpdateDateTime(Date date, Integer day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}
}
