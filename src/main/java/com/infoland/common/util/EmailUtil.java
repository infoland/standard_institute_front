package com.infoland.common.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.digest.DigestUtils;

import com.infoland.app.Constants;

/**
 * 
 * @Description 邮件工具类
 * @author yinyg
 * @date 2017年8月26日 上午9:51:30 
 * @version V1.3.1
 */
public class EmailUtil {
	/**
	 * 
	 * @Description 获取邮件回话
	 * @author yinyg
	 * @return
	 */
	private static Session getEmaillSession(){
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", Constants.EMAIL_PROTOCOL);   // 使用的协议（JavaMail规范要求）
        props.setProperty("mail.smtp.host", Constants.EMAIL_HOST);   // 发件人的邮箱的 SMTP 服务器地址
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", Constants.EMAIL_PORT);
        //邮件用户名密码验证
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Constants.EMAIL_FROM, Constants.EMAIL_PWD);
            }

        };
        Session session = Session.getDefaultInstance(props , authenticator);
        //开启session的调试模式，可以查看当前邮件发送状态
        session.setDebug(true);
        return session;
	}
	/**
	 * 
	 * @Description 邮件发送
	 * @author yinyg
	 * @param recipient 收件人
	 * @param content 邮件内容
	 * @return true 发送成功
	 */
	public static boolean sendMeail(String recipient, String content){
		Session session = getEmaillSession();
		//创建邮件
		Message msg = new MimeMessage(session);
		System.setProperty("mail.mime.charset","UTF-8");
        try {
        	//设置邮件基本信息
        	//发件人
        	msg.setFrom(new InternetAddress(Constants.EMAIL_FROM, Constants.EMAIL_FROM_ALIAS, "UTF-8"));
        	//收件人
            InternetAddress[] address = {new InternetAddress(recipient)};
            msg.setRecipients(Message.RecipientType.TO, address);
            //邮件主题
            msg.setSubject(Constants.EMAIL_THEME);
            //邮件发送时间
			msg.setSentDate(new Date());
			//邮件内容
			msg.setContent(content , "text/html;charset=utf-8");
			//发送邮件
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @Description 随机加密 
	 * @author yinyg
	 * @return 
	 */
	public static String rundomMD5ValidCode(){
		StringBuilder validCode = new StringBuilder();
		for(int i = 0; i < 10; i++){
			validCode.append(Math.random());
		}
		return DigestUtils.md5Hex(validCode.toString());
	}
	
	
	public static void main(String[] args) {
//		EmailUtil.sendMeail("a546799220@163.com", "test", "啊啊啊啊啊啊啊啊啊啊");
//		System.out.println(EmailUtil.rundomMD5ValidCode());
	}
}
