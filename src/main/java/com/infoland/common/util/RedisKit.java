package com.infoland.common.util;

import com.infoland.app.Constants;
import com.infoland.modules.members.model.UserInfo;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;

public class RedisKit {

	private RedisKit() {

	}

	private static Cache cache;

	private static String dbName;

	static {
		dbName = PropKit.use(Constants.RES_JDBC_FILE_NAME).get(Constants.REDIS_DB);
		cache = use(dbName);
	}

	/**
	 * 获取用户信息
	 * 
	 * @author Mandarava
	 * @param userId
	 * @return
	 */
	public static UserInfo getUser(String userId) {
		if (cache == null)
			cache = Redis.use(dbName);
		return cache.get(userId);
	}


	private static Cache use(String dbName) {
		return (cache = com.jfinal.plugin.redis.Redis.use(dbName));
	}

	public static Boolean ping(Cache cache) {
		try {
			if (cache.ping().equals("PONG"))
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static void del(String key) {
		if (cache == null)
			cache = Redis.use(dbName);
		if (ping(cache)) {
			cache.del(key);
		}
	}

	public static void rpush(String key, Object value) {
		if (cache == null)
			cache = Redis.use(dbName);
		if (ping(cache)) {
			cache.rpush(key, value);
		}
	}

	public static Cache getCache() {
		if (cache == null)
			cache = Redis.use(dbName);
		return cache;
	}
}
