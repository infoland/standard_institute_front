package com.infoland.common.interceptor;


import com.infoland.modules.basic.SystemConfiguration;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class OverAllParamsInterceptor implements Interceptor {

	/**
	 * @author lixp
	 * 初始化为js文件加载时间戳
	 */
	@Override
	public void intercept(Invocation invocation) {
		Controller action = invocation.getController();
		action.setAttr("version", SystemConfiguration.getMap().get("version"));
		invocation.invoke();
	}
	
}
