package com.infoland.common.fastdfs;

import java.io.Serializable;

/**
 * @author zpy 2015-12-9
 */
public interface FileManagerConfig extends Serializable {

	public static final String FILE_DEFAULT_WIDTH = "120";
	public static final String FILE_DEFAULT_HEIGHT = "120";
	public static final String FILE_DEFAULT_AUTHOR = "zpy";

	public static final String PROTOCOL = "http://";
	public static final String SEPARATOR = "/";

	public static final String CLIENT_CONFIG_FILE = "fdfs_client.conf";
}