package com.infoland.common.fastdfs;

import java.io.File;
import java.util.List;

import com.infoland.modules.cms.model.Uploadimage;
import com.jfinal.kit.PropKit;

/**
 * 图片上传处理
 * 
 * @author Janel.Han
 * @date 2015年12月27日 下午7:09:19
 * @version V1.3.1
 */
public class UploadImageUtil {

	/**
	 * 图片上传至fastdfs并存入图片表
	 * 
	 * @author Janel.Han
	 * @param foreignId
	 * @param uploadimageType
	 * @param owenType
	 * @param path
	 * @param sort
	 * @throws Exception
	 */
	public static void upload(Long foreignId, int uploadimageType, int owenType, String path, int sort)
			throws Exception {
		int start = 0;
		String fileName = null;
		boolean status = false;
		try {
			start = path.lastIndexOf("/");
			if (start == -1) {
				start = path.lastIndexOf("\\");
			}
			fileName = path.substring(start + 1, path.length());
			status = saveImage(foreignId, uploadimageType, owenType, path, fileName, sort);
		} catch (Exception e) {
			status = saveImage(foreignId, uploadimageType, owenType, path, fileName, sort);
			e.printStackTrace();
		}
		if (status) {
			new File(path).delete();
		} else {
			/*ImagesUploadFaild image = new ImagesUploadFaild();
			image.set("faild_id", "").set("faild_table_name", owenType).set("faild_column_name", "")
					.set("download_url", path).set("error_message", "图片保存失败").save();*/
		}
	}

	/**
	 * 更新图片上传至fastdfs并存入图片表
	 * 
	 * @author Janel.Han
	 * @param foreignId
	 * @param uploadimageType
	 * @param owenType
	 * @param path
	 * @param sort
	 * @throws Exception
	 */
	public static void updateUpload(Uploadimage uploadImage, Long foreignId, int uploadimageType, int owenType,
			String path, int sort) throws Exception {
		int start = 0;
		String fileName = null;
		boolean status = false;
		try {
			start = path.lastIndexOf("/");
			if (start == -1) {
				start = path.lastIndexOf("\\");
			}
			fileName = path.substring(start + 1, path.length());
			status = updateImage(uploadImage, foreignId, uploadimageType, owenType, path, fileName, sort);
		} catch (Exception e) {
			status = updateImage(uploadImage, foreignId, uploadimageType, owenType, path, fileName, sort);
			e.printStackTrace();
		}
		if (status) {
			new File(path).delete();
		} else {
			/*ImagesUploadFaild image = new ImagesUploadFaild();
			image.set("faild_id", "").set("faild_table_name", owenType).set("faild_column_name", "")
					.set("download_url", path).set("error_message", "图片保存失败").save();*/
		}
	}

	/**
	 * 处理上传的图片
	 * 
	 * @author Janel.Han
	 * @param foreignId
	 * @param uploadimageType
	 * @param owenType
	 * @param path
	 * @param fileName
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	private static boolean saveImage(Long foreignId, int uploadimageType, int owenType, String path, String fileName,
			int sort) throws Exception {
		String[] result = FileManager.upload(path, fileName);
		int start = 0;
		start = result[1].lastIndexOf("/");
		if (start == -1) {
			start = result[1].lastIndexOf("\\");
		}
		fileName = result[1].substring(start + 1, result[1].length());
		Uploadimage image = new Uploadimage();
		return image.set("uploadimage_name", fileName)
				.set("uploadimage_url", PropKit.use("jdbc.properties").get("file.image") + result[0] + "/" + result[1])
				.set("uploadimage_type", uploadimageType).set("uploadimage_order", sort).set("groupName", result[0])
				.set("remoteName", result[1]).set("owen_type", owenType).set("foreign_id", foreignId).saved();
	}
	
	/**
	 * 更新上传的图片
	 * 
	 * @author Janel.Han
	 * @param foreignId
	 * @param uploadimageType
	 * @param owenType
	 * @param path
	 * @param fileName
	 * @param sort
	 * @return
	 * @throws Exception
	 */

	public static boolean updateImageNull(Uploadimage uploadImage, int uploadimageType, int owenType, int sort)
			throws Exception {
		Uploadimage image = uploadImage;
		return image.set("uploadimage_type", uploadimageType).set("uploadimage_order", sort).set("owen_type", owenType)
				.update();
	}

	private static boolean updateImage(Uploadimage uploadImage, Long foreignId, int uploadimageType, int owenType,
			String path, String fileName, int sort) throws Exception {
		String[] result = FileManager.upload(path, fileName);
		int start = 0;
		start = result[1].lastIndexOf("/");
		if (start == -1) {
			start = result[1].lastIndexOf("\\");
		}
		fileName = result[1].substring(start + 1, result[1].length());
		Uploadimage image = uploadImage;
		return image.set("uploadimage_name", fileName)
				.set("uploadimage_url", PropKit.use("jdbc.properties").get("file.image") + result[0] + "/" + result[1])
				.set("uploadimage_type", uploadimageType).set("uploadimage_order", sort).set("groupName", result[0])
				.set("remoteName", result[1]).set("owen_type", owenType).set("foreign_id", foreignId).update();
	}

	/**
	 * 删除图片
	 * 
	 * @author Janel.Han
	 * @param id
	 * @throws Exception
	 */
	public static void deleteImage(Long id) throws Exception {
		Uploadimage uploadImage = Uploadimage.dao.findById(id);
		if (uploadImage != null) {
			String groupName = uploadImage.getStr("groupName");
			String remoteName = uploadImage.getStr("remoteName");
			try {
				FileManager.deleteFile(groupName, remoteName);
			} catch (Exception e) {
				FileManager.deleteFile(groupName, remoteName);
				e.printStackTrace();
			}
			Uploadimage.dao.deleteById(id);
		}
	}

	/**
	 * 删除图片
	 * 
	 * @author Janel.Han
	 * @param id
	 * @throws Exception
	 */
	public static void deleteImageByForeignId(Long id) throws Exception {
		List<Uploadimage> uploadImageList = Uploadimage.dao.findByForeignId(id);
		for(Uploadimage uploadImage : uploadImageList){
			if (uploadImage != null) {
				String groupName = uploadImage.getStr("groupName");
				String remoteName = uploadImage.getStr("remoteName");
				try {
					FileManager.deleteFile(groupName, remoteName);
				} catch (Exception e) {
					FileManager.deleteFile(groupName, remoteName);
					e.printStackTrace();
				}
				Uploadimage.dao.deleteById(uploadImage.getId());
			}
		}
	}
}