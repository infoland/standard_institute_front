package com.infoland.modules.members.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.poi.ss.usermodel.Row;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.infoland.common.util.CniswsUtil;
import com.infoland.common.util.CommonUtil;
import com.infoland.common.util.EmailUtil;
import com.infoland.common.util.ExportExcel;
import com.infoland.modules.basic.Message;
import com.infoland.modules.basic.SystemConfiguration;
import com.infoland.modules.members.model.CollectionInfo;
import com.infoland.modules.members.model.TrackingInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.members.service.MembersService;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;

import ws.client.StanardSort;
import ws.client.WebApi;

/**
 * 会员管理服务层
 *
 * @author Mandarava
 * @date 20170622
 * @version V1.3.1
 */

public class MembersServiceImpl implements MembersService {

	/**
	 * 
	 * @Description 跟踪定制，同时将跟踪信息插入到数据库
	 * @author Wsy
	 * @param standardId
	 *            标准标识符
	 * @return 跟踪定制状态
	 */
	public Message saveTrackingInfo(final Integer id, final String userName, final String standardId) {
		final Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		if (StrKit.notBlank(userName) && StrKit.notBlank(standardId)) {
			// 获取本地库用户定制总条数
			Integer count = TrackingInfo.dao.countTracking(id);
			if (count == 10) {
				message.setMessage("已经定制10条，无法继续定制");
				return message;
			}
			// 事务
			boolean txFlag = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					String flag = ""; // 1.成功 2.一定制 0.失败
					//实例化连接配置对象
					WebApi api = new WebApi();
					//调用中标院接口
					String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "A001"};
					String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, standardId};
					OMElement qs = null;
					try {
						// 调中标院的接口，返回可用不可用Boolean值
						qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"setUserTracking",params,paramsValue);
						flag = api.getString(qs);
					} catch (Exception e) {
						// 跟中标院的链接出现问题了，进行信息提示返回前台
						message.setMessage("请求出错了！");
						return false;
					}
					// 跟踪成功后，调中标院的接口，将当前跟踪信息保存到本地数据库
					if ("1".equals(flag)) {
						// 创建跟踪对象
						TrackingInfo trackingInfo = new TrackingInfo();
						// 创建list对象接收中标院的数据
						List<Map<String, Object>> stList = new ArrayList<Map<String, Object>>();
						//调用中标院接口
						params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "iPageIndex", "iPageSize "};
						paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, "1", "100"};
						try {
							// 调用中标院接口，放回list集合
							qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserStandardTracking",params,paramsValue);
							stList = api.getNodeList(qs);
						} catch (Exception e) {
							message.setMessage("请求出错了！");
							return false;
						}
						if (stList != null && stList.size() > 0) {
							// 循环中标院返回的list
							for (Map<String, Object> standardTracking : stList) {
								// 判断是否是当前跟踪的对象，进行赋值
								if (standardTracking.get("a001").equals(standardId)) {
									trackingInfo.setUserInfoId(id);
									trackingInfo.setUserName(userName);
									trackingInfo.setStandardCode((String) standardTracking.get("a100"));
									String standardName = standardTracking.get("a298a302").toString().replace("<br>", " ");
									trackingInfo.setStandardName(standardName);
									trackingInfo.setTrackingDate((String) standardTracking.get("createDate"));
									// 判断标准状态，转换成自己的值
									if ("A".equals(standardTracking.get("a000"))
											|| "N".equals(standardTracking.get("a000"))) {
										trackingInfo.setStandardStatus(0);
										trackingInfo.setStandardStatusV("现行");
									} else {
										trackingInfo.setStandardStatus(1);
										trackingInfo.setStandardStatusV("已废除");
									}
									// 判断定制状态，准换成自己的值
									if ("NEW".equals(standardTracking.get("status"))) {
										trackingInfo.setCustomStatus(0);
										trackingInfo.setCustomStatusV("免费");
									} else if ("PAY".equals(standardTracking.get("status"))) {
										trackingInfo.setCustomStatus(1);
										trackingInfo.setCustomStatusV("已支付");
									} else {
										trackingInfo.setCustomStatus(2);
										trackingInfo.setCustomStatusV("未支付");
									}
								}
							}
							// 将当前跟踪的对象存到数据库中
							try {
								if (trackingInfo.save()) {
									return true;
								}
							} catch (Exception e) {
								message.setMessage("定制失败");
								e.printStackTrace();
								return false;
							}
						}
						message.setMessage("定制失败");
					} else if ("2".equals(flag)) {
						message.setMessage("已定制");
					} else {
						message.setMessage("定制失败");
					}
					return false;
				}
			});
			if (txFlag) {
				count++;
				message.setMessage("添加成功 添加条目限制数量（10）条 您已经添加了" + count + "条");
				message.setSuccess(Boolean.TRUE);
			}
		}
		return message;
	}

	/**
	 * 
	 * Description 查看跟踪列表，调中标院接口实现
	 * 
	 * @param pageNumber
	 *            每页数据记录数
	 * @return 跟踪对象集合
	 */
	@Override
	public Message findTrackList(String userName, String pageNumber) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 新定义一个TrackingInfo类的集合
		Page<TrackingInfo> pagelist = new Page<TrackingInfo>();
		List<TrackingInfo> newList = new ArrayList<TrackingInfo>();
		List<Map<String, Object>> stList = new ArrayList<Map<String, Object>>();
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName","iPageIndex","iPageSize"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, pageNumber, "10"};
		OMElement qs = null;
		try {
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserStandardTracking",params,paramsValue);
			stList = api.getNodeList(qs);
		} catch (Exception e) {
			message.setMessage("连接失败，请重试！");
			return message;
		}
		// 将中标院返回的数组进行遍历，设置成自己的字段返回前台
		if (stList.size() > 0 && stList.get(0) != null) {
			TrackingInfo trackingInfo = null;
			for (Map<String, Object> standardTracking : stList) {
				trackingInfo = new TrackingInfo();
				newList.add(trackingInfo);
				trackingInfo.setId(Integer.parseInt((String) standardTracking.get("id")));
				trackingInfo.setStandardCode((String) standardTracking.get("a100"));
				trackingInfo.setStandardName((String) standardTracking.get("a298a302"));
				trackingInfo.setStandardStatusV((String) standardTracking.get("a000"));
				trackingInfo.setCustomStatusV((String) standardTracking.get("status"));
				trackingInfo.setTrackingDate((String) standardTracking.get("createDate"));
			}
			// 获取分页信息
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) stList.get(0).get("pagination");
			Integer pageNum = Integer.parseInt((String) map.get("pageNo"));
			Integer pageSize = Integer.parseInt((String) map.get("pageSize")); 
			Integer pageCount = Integer.parseInt((String) map.get("totalPageNum"));
			// 重置newlist分页信息返回前台(
			pagelist = new Page<TrackingInfo>(newList, pageNum, pageSize, pageCount, stList.size());
			// 设置Message对象信息
			message.setObject(pagelist);
			message.setMessage("查询成功！");
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("查询失败，请重试！");
		}
		return message;
	}

	/**
	 * 
	 * Description 标准跟踪付费
	 * 
	 * @param standardList
	 *            标准标识符数组
	 * @param orgName
	 *            公司名称
	 * @param address
	 *            通讯地址
	 * @param contact
	 *            联系人
	 * @param email
	 *            邮箱
	 * @param phone
	 *            电话
	 * @return 返回付费状态：成功或失败
	 */
	@Override
	public Message trackingInfoOrder(List<String> standardList, String orgName, String address, String contact,
			String email, String phone) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 定义个空字符串，接收中标院返回的参数
		String flag = "";
		//定义一个字符串，用String的join方法拼接list，传入接口
		String str = "";
		if	(standardList != null && standardList.size() > 0) {
			for (int i=0;i<standardList.size();i++) {
				if (i == (standardList.size() - 1)) {
					str = str.concat(standardList.get(i)); 
				} else {
					str = str.concat(standardList.get(i)).concat(" "); 
				}
			}
		}
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "A001", "orgName", "address", "contact", "email", "phone"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(),"", str, orgName, address, contact, email, phone};
		OMElement qs = null;
		if (StrKit.notBlank(SystemConfiguration.getMap().get("userName"))) {
			try {
				// 调用中标院的接口，flag为0是付费失败，成功返回订单号
				qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"paidTracking",params,paramsValue);
				flag = api.getString(qs);
			} catch (AxisFault e) {
				message.setMessage("付费失败，请重新尝试！");
				return message;
			}
			if (StrKit.notBlank(flag)) {
				if ("0".equals(flag)) {
					message.setMessage("付费失败！请查看您支付的账户余额是否充足！");
				} else {
					message.setMessage("付费成功！");
					message.setSuccess(Boolean.TRUE);
				}
			}
		}
		return message;
	}

	/**
	 * 
	 * @Description 推送知道标记
	 * @param pashId
	 *            推送记录ID号数组
	 * @return
	 */
	@Override
	public Message trackingInfoPush(String userName, List<String> pashId) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 定义个空字符串，接收中标院返回的参数
		String flag = "";
		//定义一个字符串，用来接收数组值
		String pashIdStr = "";
 		//遍历String[]数组，拼接给字符串pashIdStr
		if	(pashId != null && pashId.size() > 0) {
			for (int i=0;i<pashId.size();i++) {
				if (i == (pashId.size() - 1)) {
					pashIdStr = pashIdStr.concat(pashId.get(i)); 
				} else {
					pashIdStr = pashIdStr.concat(pashId.get(i)).concat(" "); 
				}
			}
		}
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "pushId"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, pashIdStr};
		OMElement qs = null;
		if (pashId.size() > 0 && StrKit.notBlank(userName)) {
			try {
				// 调用中标院接口，返回标记状态参数，“1”是成功
				qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"setUserStandardPushFlag",params,paramsValue);
				flag = api.getString(qs);
			} catch (Exception e) {
				message.setMessage("标记失败，请重新尝试！");
				return message;
			}
			if (StrKit.notBlank(flag)) {
				if ("1".equals(flag)) {
					message.setMessage("标记成功！");
					message.setSuccess(Boolean.TRUE);
				} else {
					message.setMessage("标记失败！");
				}
			}
		}
		return message;
	}

	/**
	 * 
	 * @Description 获取跟踪推送消息列表
	 * @return 跟踪推送消息列表
	 */
	@Override
	public Message trackingInfoPushList(String userName, String type, String pageNumber) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 新定义一个StandardPush类的集合
		Page<Map<String, Object>> pageList = new Page<Map<String, Object>>();
		List<Map<String, Object>> newList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName","pushType"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, type};
		OMElement qs = null;
		try {
			// 调取中标院跟踪接口
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserStandardPushList",params,paramsValue);
			newList = api.getNodeList(qs);
		} catch (Exception e) {
			message.setMessage("访问出错，请重试！");
			return message;
		}
		if (newList.get(0).size() > 0) {
			// 重新封装查询信息
			List<Map<String, Object>> newListRes = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < newList.size(); i++) {
				Map<String, Object> standardPush = newList.get(i);
				map = new HashMap<String, Object>();
				newListRes.add(map);
				map.put("oA001", standardPush.get("oA001"));
				map.put("oA100", standardPush.get("oA100"));
				map.put("oA000", standardPush.get("oA000"));
				map.put("oA206", standardPush.get("oA206"));
				map.put("oA298_A302", standardPush.get("oA298_A302"));
				map.put("nA001", standardPush.get("nA001"));
				map.put("nA100", standardPush.get("nA100"));
				map.put("nA000", standardPush.get("nA000"));
				map.put("nA101", standardPush.get("nA101"));
				map.put("n205", standardPush.get("n205"));
				map.put("nA298_A302", standardPush.get("nA298_A302"));
				map.put("relation", standardPush.get("relation"));
				map.put("flag", standardPush.get("flag"));
				map.put("orgLoginName", standardPush.get("orgLoginName"));
				map.put("loginName", standardPush.get("loginName"));
				map.put("pushType", standardPush.get("pushType"));
				map.put("pushStatus", standardPush.get("pushStatus"));
				map.put("pushDate", standardPush.get("pushDate"));
				map.put("remark", standardPush.get("remark"));
				map.put("id", standardPush.get("id"));
			}
			// 获取分页信息
			@SuppressWarnings("unchecked")
			Map<String, Object> mapPage = (Map<String, Object>) newList.get(0).get("pagination");
			Integer pageNum = Integer.parseInt((String) mapPage.get("pageNo"));
			Integer pageSize = Integer.parseInt((String) mapPage.get("pageSize"));
			Integer pageCount = Integer.parseInt((String) mapPage.get("totalPageNum"));
			Integer pageRow = Integer.parseInt((String) mapPage.get("recTotal"));
			// 重置newtarckingPush分页信息放回前台
			pageList = new Page<Map<String, Object>>(newListRes, pageNum, pageSize, pageCount, pageRow);
			// 将分页信息添加到Message对象中
			message.setObject(pageList);
			message.setMessage("查询成功！");
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("查询失败，请重试！");
		}
		return message;
	}

	/**
	 * 
	 * @Description 查询推送历史信息
	 * @param pageNumber
	 *            页数
	 * @return 跟踪推标记息列表
	 */
	@Override
	public Message trackingInfoPushFlag(String userName, String type, String startTime, String endTime,
			Integer pageNumber) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 创建两个空对象，调接口和放回用
		Page<Map<String, Object>> spList = new Page<Map<String, Object>>();
		List<Map<String, Object>> newList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName","pushType", "beginDate", "endDate"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, type, startTime, endTime};
		OMElement qs = null;
		try {
			// 调取中标院跟踪接口，获得list集合
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserStandardPushListByFlag",params,paramsValue);
			newList = api.getNodeList(qs);
		} catch (Exception e) {
			message.setMessage("连接失败，请重试！");
			return message;
		}
		if (newList.get(0).size() > 0) {
			// 重新封装查询信息
			List<Map<String, Object>> newListRes = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < newList.size(); i++) {
				Map<String, Object> standardPush = newList.get(i);
				map = new HashMap<String, Object>();
				newListRes.add(map);
				map.put("oA001", standardPush.get("oa001"));
				map.put("oA100", standardPush.get("oA100"));
				map.put("oA000", standardPush.get("oA000"));
				map.put("oA206", standardPush.get("oA206"));
				map.put("oA298_A302", standardPush.get("oA298_A302"));
				map.put("nA001", standardPush.get("nA001"));
				map.put("nA100", standardPush.get("nA100"));
				map.put("nA000", standardPush.get("nA000"));
				map.put("nA101", standardPush.get("nA101"));
				map.put("n205", standardPush.get("n205"));
				map.put("nA298_A302", standardPush.get("nA298_A302"));
				map.put("relation", standardPush.get("relation"));
				map.put("flag", standardPush.get("flag"));
				map.put("orgLoginName", standardPush.get("orgLoginName"));
				map.put("loginName", standardPush.get("loginName"));
				map.put("pushType", standardPush.get("pushType"));
				map.put("pushStatus", standardPush.get("pushStatus"));
				map.put("pushDate", standardPush.get("pushDate"));
				map.put("remark", standardPush.get("remark"));
				map.put("id", standardPush.get("id"));
			}
			// 获取分页信息
			@SuppressWarnings("unchecked")
			Map<String, Object> mapPage = (Map<String, Object>) newList.get(0).get("pagination");
			Integer pageNum = Integer.parseInt((String) mapPage.get("pageNo"));
			Integer pageSize = Integer.parseInt((String) mapPage.get("pageSize"));
			Integer pageCount = Integer.parseInt((String) mapPage.get("totalPageNum"));
			Integer pageRow = Integer.parseInt((String) mapPage.get("recTotal"));
			// 重置newtarckingPushFlag分页信息返回前台
			spList = new Page<Map<String, Object>>(newList, pageNum, pageSize, pageCount, pageRow);
		}
		message.setObject(spList);
		message.setSuccess(Boolean.TRUE);
		return message;
	}

	/**
	 * 
	 * Description 保存标准收藏
	 * 
	 * @param standardId
	 *            标准标识符
	 * @param pageNumber
	 *            页数
	 * @param id
	 *            用户id
	 * @return 保存成功或失败的状态
	 */
	@Override
	public Message saveCollect(String userName, String standardId, Integer id) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 创建一个空的字符串，来接收中标院的返回值
		String flag = "";
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "A001"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, standardId};
		OMElement qs = null;
		if (StrKit.notBlank(standardId)) {
			try {
				// 调中标院的接口，返回可用不可用Boolean值
				qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"saveCollect",params,paramsValue);
				flag = api.getString(qs);
			} catch (Exception e) {
				message.setMessage("访问出错，请重试！");
				return message;
			}
			if ("2".equals(flag)) {
				message.setMessage("该标准已收藏");
				return message;
			} else if ("1".equals(flag)) {
				// 创建跟踪对象
				CollectionInfo collectionInfo = new CollectionInfo();
				// 创建list对象接收中标院的数据
				Map<String, Object> stMap = new HashMap<String, Object>();
				//调用中标院接口
				params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "A001"};
				paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), standardId};
				try {
					// 调用中标院接口，放回list集合
					qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getStandardDetail",params,paramsValue);
					System.out.println(qs);
					stMap = api.getReturn(qs);
				} catch (Exception e) {
					message.setMessage("请求出错了！");
					return message;
				}
				if (stMap != null) {
					// 将json转为对象
					String detailNew = stMap.get("return").toString().replace("[", "").replace("]", "");
					JSONObject jo = JSON.parseObject(detailNew);
					System.out.println(jo);
					// 设置标准id,标准号和标准名
					collectionInfo.setStandardId(jo.getString("A001"));
					collectionInfo.setStandardCode(jo.getString("A100"));
					collectionInfo.setStandardName(jo.getString("A298"));
					// 设置收藏状态以及状态值
					collectionInfo.setCollectionStatus(0);
					collectionInfo.setCollectionStatusV("已收藏");
					// 设置收藏时间
					collectionInfo.setCollectionTime(new Date());
					collectionInfo.setUserInfoId(id);
					// 将当前跟踪的对象存到数据库中
					collectionInfo.save();
					message.setMessage("收藏成功！");
					message.setSuccess(Boolean.TRUE);
					return message;
				}
			}
		}
		message.setMessage("收藏失败！");
		return message;
	}
	
	/**
	 * 
	 * Description 获得收藏信息列表
	 * 
	 * @param pageNumber
	 *            页数
	 * @return
	 */
	@Override
	public Message getCollectList(String userName, String pageNumber) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		Page<CollectionInfo> pageList = new Page<CollectionInfo>();
		if (StrKit.notBlank(userName)) {
			Integer userId = UserInfo.dao.getLocalUserId(userName);
			Integer pageNum = Integer.parseInt(pageNumber);
			Integer pageSize = Integer.parseInt(CniswsUtil.getPageSize());
			pageList = CollectionInfo.dao.getList(userId,pageNum,pageSize);
		}
		if (pageList.getList().size() > 0) {
			message.setObject(pageList);
			message.setSuccess(Boolean.TRUE);
		}
		return message;
	}

	/**
	 * 
	 * Description 删除收藏记录
	 * 
	 * @param standardId
	 *            标准标识符
	 * @return 删除成功或失败的状态
	 */
	@Override
	@Before(Tx.class)
	public Message delCollect(final Integer id, final String userName, final String standardId,
			final String standardCode) {
		final Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		if (StrKit.notBlank(standardId)) {
			Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					// 创建空字符串接收标准院的返回信息
					String flag = ""; // 1 删除成功
					//实例化连接配置对象
					WebApi api = new WebApi();
					//调用中标院接口
					String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "A001"};
					String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, standardId};
					OMElement qs = null;
					if (1 == CollectionInfo.dao.updateStatus(id, standardCode)) {
						try {
							qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"delCollect",params,paramsValue);
							flag = api.getString(qs);
						} catch (Exception e) {
							message.setMessage("链接失败，请重试！");
							return false;
						}
					}
					if ("1".equals(flag)) {
						message.setMessage("删除成功！");
						message.setSuccess(Boolean.TRUE);
						return true;
					} else {
						message.setMessage("删除失败，请重试！");
					}
					return false;
				}
			});
		}
		return message;
	}

	/**
	 * 
	 * @Description 删除选中收藏记录 批量删除
	 * @author yinyg
	 * @param id
	 *            本地库用户id
	 * @param userName
	 *            登陆用户名
	 * @param standard
	 *            需要删除的标准实体
	 * @return
	 */
	@Override
	public Message delSelectCollect(final Integer id, final String userName,
			final List<CollectionInfo> collectionInfos) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		if (StrKit.notBlank(userName) && collectionInfos.size() > 0) {
			// 事务处理
			boolean flagTx = Db.tx(new IAtom() {
				@Override
				public boolean run() throws SQLException {
					// 遍历岱删除收藏列表
					for (int i = 0; i < collectionInfos.size(); i++) {
						// 存放收藏信息
						final CollectionInfo collectionInfo = collectionInfos.get(i);
						// 执行本地删除
						if (1 == CollectionInfo.dao.updateStatus(id, collectionInfo.getStandardCode())) {
							String flag = "";
							//实例化连接配置对象
							WebApi api = new WebApi();
							//调用中标院接口
							String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName", "A001"};
							String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName, collectionInfo.getId().toString()};
							OMElement qs = null;
							try {
								// 执行接口删除
								qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"delCollect",params,paramsValue);
								flag = api.getString(qs);
							} catch (Exception e) {
								return false;
							}
							if (!"1".equals(flag)) {
								return false;
							}
						} else {
							return false;
						}
					}
					return true;
				}
			});
			if (flagTx) {
				message.setSuccess(Boolean.TRUE);
				message.setMessage("删除成功，共删除" + collectionInfos.size() + "条收藏");
				return message;
			}
		}
		message.setMessage("删除失败！");
		return message;
	}

	/**
	 * 
	 * Description 验证用户名是否可用
	 * 
	 * @param userName
	 *            用户名
	 * @return 可用不可用信息
	 */
	@Override
	public Message loginNameCheck(String userName) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 初始化boolean值check
		boolean check = false;
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "loginNameCheck"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName};
		OMElement qs = null;
		try {
			// 调中标院的接口，返回可用不可用Boolean值
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"loginNameCheck",params,paramsValue);
			check = api.getString(qs).equals("true");
		} catch (Exception e) {
			message.setMessage("链接失败，请重试！");
			return message;
		}
		if (check) {
			message.setMessage("用户名没有重复，可以使用！");
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("用户名有重复，请更改用户名！");
		}
		return message;
	}

	/**
	 * 
	 * Description 用户注册
	 * 
	 * @param UserInfo
	 *            用户对象
	 * @return 注册成功或失败状态
	 */
	@Override
	public Message registered(final UserInfo userInfo) {
		final Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		//设置为启用
		userInfo.setStartFlag(1);
		// 将对象中属性为null的置为空
		judgeNull(userInfo);
		userInfo.setCreateDate(new Date());
		userInfo.setModifyDate(new Date());
		Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				// 初始化字符串flag，用来接收中标院返回值
				String sex = "先生";
				if (userInfo.getSex() != null) {
					sex = (userInfo.getSex() == 0 ? "女士" : "先生");
				}
				String flag = "";
				if (userInfo.save()) {
					//实例化连接配置对象
					WebApi api = new WebApi();
					//调用中标院接口
					String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "reg_loginName", "reg_loginName", "reg_realName", "reg_orgName", "reg_email", "reg_province", "reg_city", "reg_address", "reg_postCode", "reg_telephone", "reg_fax", "reg_cellphone", "reg_identificationType", "reg_identificationNum", "reg_userSex", "reg_userBirthday", "reg_educLevel"};
					String[] paramsValue = new String[] {CniswsUtil.getUserName(), CniswsUtil.getPassword(), "",
							CniswsUtil.getOrgCode(), userInfo.getUserName(), userInfo.getUserPassword(),
							userInfo.getRealName(), userInfo.getCompanyName(), userInfo.getEmail(),
							userInfo.getProvince(), userInfo.getCity(), userInfo.getConnectAddress(),
							userInfo.getPostcode(), userInfo.getTelephone(), userInfo.getFax(),
							userInfo.getPhoneNum(), userInfo.getIdTypeV(), userInfo.getIdNum(), sex,
							userInfo.getBirthDate(), userInfo.getEducationV()};
					OMElement qs = null;
					try {
						qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"register",params,paramsValue);
						flag = api.getString(qs);
					} catch (Exception e) {
						message.setMessage("链接失败，请重试！");
						return false;
					}
					if ("1".equals(flag)) {
						message.setMessage("注册成功！");
						message.setSuccess(Boolean.TRUE);
						return true;
					}
				}
				message.setMessage("注册失败，请重试！");
				return false;
			}
		});
		return message;
	}

	/**
	 * 
	 * Description 验证用户登录
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @return 登录成功失败状态
	 */
	@Override
	public Message login(String userName, String password, String ip) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 初始化Boolean值check，用来接收中标院接口返回参数a
		Integer result = UserInfo.dao.checkStartFlag(userName);
		//实例化连接配置对象
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "loginNameValidation","loginNamePasswordValidation"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName,  password};
		OMElement qs = null;
		if(result == 3){
			message.setMessage("用户名或密码错误，请重试！");
		}else if(result == 1){
			boolean check = false;
			try {
				qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"loginNameValidation",params,paramsValue);
				check = api.getString(qs).equals("true");
			} catch (AxisFault e) {
				e.printStackTrace();
				message.setMessage("链接失败，请重试！");
				return message;
			}
			if (check) {
				if(UserInfo.dao.updateUserInfoByName(userName, new Date(), ip)){
					message.setMessage("登录成功！");
					message.setSuccess(Boolean.TRUE);
					return message;
				}else{
					message.setMessage("登录时间与登录ip获取失败！");
				}
			} else {
				message.setMessage("用户名或密码错误，请重试！");
			}
		}else{
			message.setMessage("该用户已被禁用！");
		}
		return message;
	}

	/**
	 * 
	 * Description 更新用户信息
	 * 
	 * @param userName
	 *            用户名
	 * @return 更新状态，成功或失败
	 */
	@Override
	@Before(Tx.class)
	public Message updateUserInfo(UserInfo userInfo) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 将用户信息为null设置为空
		judgeNull(userInfo);
		// 初始化空字符串接收中标院返回值
		String flag = "";
		String sex = "先生";
		if (userInfo.getSex() != null) {
			sex = (userInfo.getSex() == 0 ? "女士" : "先生");
		}
		// 设置本地数据库用户id
		Integer id = UserInfo.dao.getLocalUserId(userInfo.getUserName());
		userInfo.setId(id);

		// 创建用户对象，进行本地数据库更新
		if (userInfo.update()) {
			//实例化连接配置对象
			WebApi api = new WebApi();
			//调用中标院接口
			String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "reg_loginName", "reg_loginName", "reg_realName", "reg_orgName", "reg_email", "reg_province", "reg_city", "reg_address", "reg_postCode", "reg_telephone", "reg_fax", "reg_cellphone", "reg_identificationType", "reg_identificationNum", "reg_userSex", "reg_userBirthday", "reg_educLevel"};
			String[] paramsValue = new String[] {CniswsUtil.getUserName(), CniswsUtil.getPassword(), "",
					CniswsUtil.getOrgCode(), userInfo.getUserName(), userInfo.getUserPassword(),
					userInfo.getRealName(), userInfo.getCompanyName(), userInfo.getEmail(),
					userInfo.getProvince(), userInfo.getCity(), userInfo.getConnectAddress(),
					userInfo.getPostcode(), userInfo.getTelephone(), userInfo.getFax(),
					userInfo.getPhoneNum(), userInfo.getIdTypeV(), userInfo.getIdNum(), sex,
					userInfo.getBirthDate(), userInfo.getEducationV()};
			OMElement qs = null;
			try {
				qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"updateUserInfo",params,paramsValue);
				flag = api.getString(qs);
			} catch (AxisFault e) {
				e.printStackTrace();
				message.setMessage("链接失败，请重试！");
				return message;
			}
		}
		if ("1".equals(flag)) {
			message.setMessage("修改成功！");
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("修改失败，请重试！");
		}
		return message;
	}
	
	/**
	 * 
	 * Description 获取用户信息
	 * 
	 * @param userName
	 *            用户名
	 * @return 用户对象
	 */
	@Override
	public Message getUserInfo(String userName) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		UserInfo userInfo = null;
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode", "userLoginName"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode(), userName};
		OMElement qs = null;
		Map<String, Object> map = new HashMap<String,Object>();
		try {
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserAccountInfo",params,paramsValue);
			map = api.getElement(qs);
		} catch (AxisFault e) {
			e.printStackTrace();
			message.setMessage("链接失败，请重试！");
			return message;
		}
		if (map != null) {
			userInfo = new UserInfo();
			message.setObject(userInfo);
			message.setSuccess(Boolean.TRUE);
			// 设置用户信息
			// 从本地数据库获取用户的id
			Integer id = UserInfo.dao.getLocalUserId(userName);
			if(id != null){
				userInfo.setId(id);
				userInfo.setUserName(map.get("userLoginName").toString());
				userInfo.setRealName(map.get("userRealName").toString());
				userInfo.setCompanyName(map.get("userOrgName").toString());
				userInfo.setProvince(map.get("userProvince").toString());
				userInfo.setCity(map.get("userCity").toString());
				userInfo.setConnectAddress(map.get("userAddress").toString());
				userInfo.setFax(map.get("userFax").toString());
				userInfo.setEmail(map.get("userEmail").toString());
				userInfo.setTelephone(map.get("userTelephone").toString());
				userInfo.setSex(map.get("userUserSex").toString().equals("女士") ? 0 : 1);
				userInfo.setPostcode(map.get("userPostCode").toString());
				userInfo.setEducationV(map.get("userEducLevel").toString());
				userInfo.setIdTypeV(map.get("userIdentificationType").toString());
				userInfo.setIdNum(map.get("userIdentificationNum").toString());
				userInfo.setBirthDate(map.get("userUserBirthday").toString());
				userInfo.setPhoneNum(map.get("userCellphone").toString());
			}else{
				message.setSuccess(Boolean.FALSE);
				message.setMessage("用户名不存在");
			}
		} else {
			message.setMessage("修改失败");
		}
		return message;
	}

	/**
	 * 
	 * @Description 显示用户能访问的标准品种
	 * @author Wsy
	 * @param pageNumber
	 *            页数
	 * @return 标准品种对象
	 */
	@Override
	public Message getUserStandardSortList(Integer pageNumber) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		// 初始化list集合和分页对象
		List<Map<String, Object>> stanardSortList = new ArrayList<Map<String, Object>>();
		Page<StanardSort> pageList = new Page<StanardSort>();
		//实例化调用接口工具类
		WebApi api = new WebApi();
		//调用中标院接口
		String[] params = new String[] { "orgLoginName", "password", "ukeyId", "orgCode"};
		String[] paramsValue = new String[] {CniswsUtil.getUserName(),CniswsUtil.getPassword(), "", CniswsUtil.getOrgCode()};
		OMElement qs = null;
		try {
			// 调用总标院的接口，返回标准权限
			qs = api.callMethod(CniswsUtil.getWsdl(),CniswsUtil.getNameSpace(),"getUserStandardSortList",params,paramsValue);
			stanardSortList = api.getNodeList(qs);
		} catch (AxisFault e) {
			message.setMessage("链接失败，请重试！");
			return message;
		}
		if (!stanardSortList.isEmpty() && stanardSortList.size() > 0) {
			// 将页面类型转换下，然后重置分页对象
			List<StanardSort> listNew = new ArrayList<StanardSort>();
			for (Map<String, Object> stanardSortMap : stanardSortList) {
				StanardSort stanardSort = new StanardSort();
				stanardSort = (StanardSort) stanardSortMap;
				listNew.add(stanardSort);
			}
			//获取分页信息
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) stanardSortList.get(0).get("pagination");
			Integer pageNum = Integer.parseInt((String) map.get("pageNo"));
			Integer pageSize = Integer.parseInt((String) map.get("pageSize"));
			Integer pageCount = Integer.parseInt((String) map.get("totalPageNum"));
			Integer pageRow = Integer.parseInt((String) map.get("recTotal"));
			pageList = new Page<StanardSort>(listNew, pageNum, pageSize, pageCount, pageRow);
			message.setObject(pageList);
			message.setMessage("获取成功！");
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("获取失败，请重试！");
		}
		return message;
	}

	/**
	 * 
	 * Description 发送邮件
	 * 
	 * @param username
	 *            用户名
	 * @param email
	 *            用户邮箱
	 * @param controller
	 *            当前Controller
	 * @return
	 */
	@Override
	public Message sendUpdateEmail(String username, String email, Controller controller) {
		Message message = new Message();
		message.setSuccess(Boolean.TRUE);
		// 获取服务器ip
		String serverIp = controller.getRequest().getServerName();
		// 获取服务器端口
		Integer serverPort = controller.getRequest().getServerPort();
		// 获取传输协议
		String scheme = controller.getRequest().getScheme();
		// 服务器项目根地址
		StringBuilder basePath = new StringBuilder(scheme + "://" + serverIp + ":" + serverPort + "/");
		String originalCode = EmailUtil.rundomMD5ValidCode();
		StringBuffer sb = new StringBuffer("点击下面链接重置密码，链接只能使用一次，请尽快激活！</br>");
		sb.append("<a href=" + basePath + "members/validEmail/");
		sb.append(username);
		sb.append("-");
		sb.append(originalCode);
		sb.append(">" + basePath + "members/validEmail/");
		sb.append(username);
		sb.append("-");
		sb.append(originalCode);
		sb.append("</a>");
		if (EmailUtil.sendMeail(email, sb.toString())) {
			JFinal.me().getServletContext().setAttribute(username, originalCode);
			message.setMessage("邮件发送成功！");
		} else {
			message.setSuccess(Boolean.FALSE);
			message.setMessage("邮件发送失败");
		}
		return message;
	}

	/**
	 * 
	 * Description 验证邮件
	 * 
	 * @param validCode
	 *            验证码
	 * @return
	 */
	@Override
	public Message validEmail(String userName, String validCode, Controller controller) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		String originalCode = (String) JFinal.me().getServletContext().getAttribute(userName);
		if (originalCode == null || "".equals(originalCode)) {
			message.setMessage("邮件验证失败！");
		}
		if (validCode.equals(originalCode)) {
			message.setSuccess(Boolean.TRUE);
			message.setMessage("密码重置成功！");
		}
		JFinal.me().getServletContext().removeAttribute(userName);
		return message;
	}

	/**
	 * 
	 * @Description 将对象中属性为null的置为空
	 * @author yinyg
	 * @param userInfo
	 *            用户实体
	 */
	private void judgeNull(UserInfo userInfo) {
		if (userInfo.getUserPassword() == null) {
			userInfo.setUserPassword(UserInfo.dao.getPasswordByName(userInfo.getUserName()));
		}
		if (userInfo.getRealName() == null) {
			userInfo.setRealName("");
		}
		if (userInfo.getCompanyName() == null) {
			userInfo.setCompanyName("");
		}
		if (userInfo.getEmail() == null) {
			userInfo.setEmail("");
		}
		if (userInfo.getProvince() == null) {
			userInfo.setProvince("省份");
		}
		if (userInfo.getCity() == null) {
			userInfo.setCity("市县");
		}
		if (userInfo.getConnectAddress() == null) {
			userInfo.setConnectAddress("");
		}
		if (userInfo.getPostcode() == null) {
			userInfo.setPostcode("");
		}
		if (userInfo.getPhoneNum() == null) {
			userInfo.setPhoneNum("");
		}
		if (userInfo.getFax() == null) {
			userInfo.setFax("");
		}
		if (userInfo.getTelephone() == null) {
			userInfo.setTelephone("");
		}
		if (userInfo.getIdTypeV() == null) {
			userInfo.setIdTypeV("");
		}
		if (userInfo.getIdNum() == null) {
			userInfo.setIdNum("");
		}
		if (userInfo.getEducationV() == null) {
			userInfo.setEducationV("");
		}
		if (userInfo.getBirthDate() == null) {
			userInfo.setBirthDate("");
		}
	}

	/**
	 * 
	 * @Description 验证用户密码是否正确
	 * @author yinyg
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @return
	 */
	@Override
	public Message checkPassword(String userName, String password) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		boolean check = false;
		check = UserInfo.dao.checkPassword(userName, password);
		if (check) {
			message.setSuccess(Boolean.TRUE);
		} else {
			message.setMessage("旧密码输入错误！");
		}
		return message;
	}

	/**
	 * 
	 * @Description 导出收藏的数据
	 * @author yinyg
	 * @param collectionInfos
	 *            导出的数据
	 * @return
	 */
	@Override
	public void exportDate(List<CollectionInfo> collectionInfos, Controller controller) {
		if (collectionInfos != null && collectionInfos.size() > 0) {
			// 存放Excel头信息
			List<String> headerList = Lists.newArrayList();
			headerList.add("编号");
			headerList.add("标准号");
			headerList.add("标准名");
			headerList.add("收藏时间");
			ExportExcel ee = new ExportExcel("表格标题", headerList);
			for (int i = 0; i < collectionInfos.size(); i++) {
				Row row = ee.addRow();
				String standardCode = collectionInfos.get(i).getStandardCode();
				Date collectionTime = CollectionInfo.dao.getCollectionDate(standardCode);
				ee.addCell(row, 0, i + 1);
				ee.addCell(row, 1, standardCode);
				ee.addCell(row, 2, collectionInfos.get(i).getStandardName());
				ee.addCell(row, 3, collectionTime);
			}
			try {
				ee.write(controller.getResponse(), CommonUtil.getUUID() + ".xlsx");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				ee.dispose();
			}
		}
	}

	/**
	 * 
	 * @Description 导出跟踪数据到excel
	 * @author yinyg
	 * @param collectionInfos
	 *            导出的数据
	 * @return
	 */
	@Override
	public void exportTrackingData(JSONArray jsonArray, Controller controller) {
		if (jsonArray != null && jsonArray.size() > 0) {
			// 存放Excel头信息
			List<String> headerList = Lists.newArrayList();
			headerList.add("编号");
			headerList.add("新标准号");
			headerList.add("新标准状态");
			headerList.add("新标准名称");
			headerList.add("新标准实施日期");
			headerList.add("原标准号");
			headerList.add("原标准名称");
			headerList.add("原标准状态");
			headerList.add("原标准作废日期");
			ExportExcel ee = new ExportExcel("表格标题", headerList);
			for (int i = 0; i < jsonArray.size(); i++) {
				Row row = ee.addRow();
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) jsonArray.get(i);
				ee.addCell(row, 0, i + 1);
				ee.addCell(row, 1, map.get("nA100"));
				Object nState = map.get("nA000");
				// 将标准标示转为对应的意思
				if ("A".equals(nState) || "N".equals(nState)) {
					ee.addCell(row, 2, "现行");
				} else if ("D".equals(nState)) {
					ee.addCell(row, 2, "作废");
				} else {
					ee.addCell(row, 2, "");
				}
				// 有的标准名为null
				if (map.get("nA298_A302") == null) {
					ee.addCell(row, 3, "");
				} else {
					ee.addCell(row, 3, map.get("nA298_A302").toString().substring(0,
							(map.get("nA298_A302").toString().indexOf("<br>"))));
				}
				ee.addCell(row, 4, map.get("nA101"));
				ee.addCell(row, 5, map.get("oA100"));
				ee.addCell(row, 6, map.get("oA298_A302"));
				Object oState = map.get("oA000");
				if ("A".equals(oState) || "N".equals(oState)) {
					ee.addCell(row, 7, "现行");
				} else if ("D".equals(oState)) {
					ee.addCell(row, 7, "作废");
				} else {
					ee.addCell(row, 7, "");
				}
				ee.addCell(row, 8, map.get("oA206"));
			}
			try {
				ee.write(controller.getResponse(), CommonUtil.getUUID() + ".xlsx");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				ee.dispose();
			}
		}
	}

	@Override
	public void exportTrackingList(List<TrackingInfo> trackingInfos, Controller controller) {
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		if (trackingInfos != null && trackingInfos.size() > 0) {
			// 存放Excel头信息
			List<String> headerList = Lists.newArrayList();
			headerList.add("编号");
			headerList.add("标准号");
			headerList.add("标准名称");
			headerList.add("标准状态");
			headerList.add("订阅日期");
			headerList.add("定制状态");
			ExportExcel ee = new ExportExcel("表格标题", headerList);
			for (int i = 0; i < trackingInfos.size(); i++) {
				Row row = ee.addRow();
				TrackingInfo trackingInfo = trackingInfos.get(i);
				ee.addCell(row, 0, i + 1);
				ee.addCell(row, 1, trackingInfo.getStandardCode());
				ee.addCell(row, 2, trackingInfo.getStandardName());
				String nState = trackingInfo.getStandardStatusV();
				// 将标准标示转为对应的意思
				if ("A".equals(nState) || "N".equals(nState)) {
					ee.addCell(row, 3, "现行");
				} else if ("D".equals(nState)) {
					ee.addCell(row, 3, "作废");
				} else {
					ee.addCell(row, 3, "");
				}
				ee.addCell(row, 4, trackingInfo.getTrackingDate());
				// 将定制状态转为对应的意思
				String cState = trackingInfo.getCustomStatusV();
				if ("NEW".equals(cState)) {
					ee.addCell(row, 5, "免费");
				} else if ("PAY".equals(cState)) {
					ee.addCell(row, 5, "已付费");
				} else if ("UNPAY".equals(cState)) {
					ee.addCell(row, 5, "未付费");
				} else {
					ee.addCell(row, 5, "");
				}
			}
			try {
				ee.write(controller.getResponse(), CommonUtil.getUUID() + ".xlsx");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				ee.dispose();
			}
		}
	}
}