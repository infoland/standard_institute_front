package com.infoland.modules.members.service;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.CollectionInfo;
import com.infoland.modules.members.model.TrackingInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.members.service.impl.MembersServiceImpl;
import com.jfinal.aop.Duang;
import com.jfinal.core.Controller;

/**
 * 会员管理  20170622
 * @author Mandarava
 *
 */
public interface MembersService{

	public static final MembersService service = Duang.duang(MembersServiceImpl.class);
	
	/**
	 * 
	 * @Description 	跟踪定制
	 * @author Wsy
	 * @param userName	用户名
	 * @param password	密码
	 * @param A001		标准标识符
	 * @return			成功失败的信息
	 */
	Message saveTrackingInfo(final Integer id, final String userName, final String standardId);

	/**
	 * 
	 * @Description	查询跟踪记录
	 * @author Wsy
	 * @param pageNumber	页数
	 * @param userName	当前用户
	 * @return				跟踪对象集合
	 */
	Message findTrackList(String userName, String pageNumber);
	
	/**
	 * 
	 * @Description 	跟踪标准付费
	 * @author Wsy
	 * @param A001		标准标识符数组
	 * @param orgName	单位名称
	 * @param address	通讯地址
	 * @param contact	联系人	
	 * @param email		邮箱
	 * @param phone		电话
	 * @return			返回付费状态：成功或失败
	 */
	Message trackingInfoOrder(List<String> A001,String orgName,String address,String contact,String email,String phone);
	
	/**
	 * 
	 * @Description		推送知道标记
	 * @author Wsy
	 * @param pashId	推送记录ID号数组
	 * @return			标记状态，成功或失败
	 */
	Message trackingInfoPush(String userName, List<String> pashId);
	
	/**
	 * 
	 * @Description		获取跟踪推送消息列表
	 * @param userName 用户名
	 * @param type 调用接口传入的推送类型，类型分三种：定制领域品种(SUBSCRIPTION)，定制标准跟踪（PUSH）购买历史跟踪(BUY)
	 * @author Wsy
	 * @return			跟踪推送消息列表
	 */
	Message trackingInfoPushList(String userName, String type, String pageNumber);
	
	/**
	 * 
	 * @Description		获取跟踪标记消息列表
	 * @author Wsy
	 * @return			跟踪标记消息列表
	 */
	Message trackingInfoPushFlag(String userName, String type, String startTime, String endTime, Integer pageNumber);
	
	/**
	 * 
	 * @Description 		保存收藏信息
	 * @author Wsy
	 * @param standardId	标准标识符
	 * @param id			用户id
	 * @return
	 */
	Message saveCollect(String userName, String standardId, Integer id);
	
	/**
	 * 
	 * @Description			获取收藏信息列表
	 * @author Wsy			
	 * @param pageNumber	页数
	 * @return				
	 */
	Message getCollectList(String userName, String pageNumber);
	
	/**
	 * 
	 * @Description 删除收藏记录
	 * @author Wsy
	 * @param A001	标准标识符
	 * @param id	本地库用户id
	 * @param userName	用户名
	 * @return		删除成功或失败的状态
	 */
	Message delCollect(final Integer id, final String userName, final String standardId, final String standardCode);
	
	/**
	 * 
	 * @Description 	验证登录名是否可用
	 * @author Wsy
	 * @param userName	用户名
	 * @return			可用不可用的信息
	 */
	Message loginNameCheck(String userName);
	
	/**
	 * 
	 * @Description 	用户注册
	 * @author Wsy	
	 * @param userName	用户名
	 * @param password	密码
	 * @return			注册成功或失败
	 */
	Message registered(final UserInfo userInfo);
	
	/**
	 * 
	 * @Description 	用户登录验证
	 * @author Wsy
	 * @param userName	用户名
	 * @param password	密码
	 * @return			验证通过不通过的状态
	 */
	Message login(String userName, String password, String ip);
	
	/**
	 * 
	 * @Description		更新用户信息 
	 * @author Wsy
	 * @param userName	用户名
	 * @return			返回更新状态，成功或失败
	 */
	Message updateUserInfo(UserInfo userInfo);
	
	/**
	 * 
	 * @Description 	获取用户信息
	 * @author Wsy
	 * @param userName	用户名
	 * @return			用户信息对象
	 */
	Message getUserInfo(String userName);
	
	/**
	 * 
	 * @Description			显示用户能访问的标准品种
	 * @author Wsy
	 * @param pageNumber	页数
	 * @return				标准品种对象
	 */
	Message getUserStandardSortList(Integer pageNumber);
	
	/**
	 * 
	 * @Description 邮件发送
	 * @author yinyg
	 * @return
	 */
	Message sendUpdateEmail(String username, String email, Controller controller);
	
	/**
	 * 
	 * @Description 验证邮件链接
	 * @author yinyg
	 * @param validCode 验证码
	 * @return
	 */
	Message validEmail(String userName, String validCode, Controller controller);
	
	/**
	 * 
	 * @Description 验证用户密码是否正确
	 * @author yinyg 
	 * @param userName 用户名
	 * @param password 密码
	 * @return
	 */
	Message checkPassword(String userName, String password);
	
	/**
	 * 
	 * @Description 删除选中收藏记录 批量删除
	 * @author yinyg
	 * @param userName 登陆用户名
	 * @param id 用户本地id
	 * @param standard 需要删除的标准实体
	 * @return
	 */
	Message delSelectCollect(final Integer id, final String userName, final List<CollectionInfo> collectionInfos);
	
	/**
	 * 
	 * @Description 导出收藏的数据
	 * @author yinyg
	 * @param collectionInfos 导出的数据
	 * @return
	 */
	void exportDate(List<CollectionInfo> collectionInfos, Controller controller);
	
	/**
	 * 
	 * @Description 导出跟踪数据到excel
	 * @author yinyg
	 * @param jsonArray 导出的数据
	 * @return
	 */
	void exportTrackingData(JSONArray jsonArray, Controller controller);
	
	/**
	 * 
	 * @Description 导出跟踪列表数据到excel
	 * @author yinyg
	 * @param jsonArray 导出的数据
	 * @return
	 */
	void exportTrackingList(List<TrackingInfo> trackingInfos, Controller controller);
	
	
}

