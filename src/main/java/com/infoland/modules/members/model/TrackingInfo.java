package com.infoland.modules.members.model;

import com.infoland.modules.members.model.base.BaseTrackingInfo;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 
 * @Description 前台标准跟踪模块
 * @author Wsy
 * @date 2017年7月17日 下午5:03:09 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class TrackingInfo extends BaseTrackingInfo<TrackingInfo> {
	public static final TrackingInfo dao = new TrackingInfo().dao();
	
	
	/**
	 * 
	 * @Description  统计收藏个数
	 * @author yinyg
	 * @param id 用户id
	 * @param code 标识符
	 * @return
	 */
	public Integer countTracking(Integer id){
		Kv kv = new Kv();
		kv.set("id", id);
		SqlPara sql = getSqlPara("collectionSql.countTracking", kv);
		Record record = Db.findFirst(sql);
		Long count = record.get("count");
		return new Long(count).intValue();
		
	}
}
