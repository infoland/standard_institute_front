package com.infoland.modules.members.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseConsigneeInfo<M extends BaseConsigneeInfo<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setConsigneeName(java.lang.String consigneeName) {
		set("consignee_name", consigneeName);
	}

	public java.lang.String getConsigneeName() {
		return get("consignee_name");
	}

	public void setCompanyName(java.lang.String companyName) {
		set("company_name", companyName);
	}

	public java.lang.String getCompanyName() {
		return get("company_name");
	}

	public void setEmail(java.lang.String email) {
		set("email", email);
	}

	public java.lang.String getEmail() {
		return get("email");
	}

	public void setProvince(java.lang.String province) {
		set("province", province);
	}

	public java.lang.String getProvince() {
		return get("province");
	}

	public void setCity(java.lang.String city) {
		set("city", city);
	}

	public java.lang.String getCity() {
		return get("city");
	}

	public void setConnectAddress(java.lang.String connectAddress) {
		set("connect_address", connectAddress);
	}

	public java.lang.String getConnectAddress() {
		return get("connect_address");
	}

	public void setPostcode(java.lang.String postcode) {
		set("postcode", postcode);
	}

	public java.lang.String getPostcode() {
		return get("postcode");
	}

	public void setPhoneNum(java.lang.String phoneNum) {
		set("phone_num", phoneNum);
	}

	public java.lang.String getPhoneNum() {
		return get("phone_num");
	}

	public void setTelephone(java.lang.String telephone) {
		set("telephone", telephone);
	}

	public java.lang.String getTelephone() {
		return get("telephone");
	}

	public void setUserInfoId(java.lang.Integer userInfoId) {
		set("user_info_id", userInfoId);
	}

	public java.lang.Integer getUserInfoId() {
		return get("user_info_id");
	}
	
	
	public void setIsDefault(java.lang.Boolean isDefault) {
		set("is_default", isDefault);
	}

	public java.lang.Boolean getIsDefault() {
		return get("is_default");
	}
}
