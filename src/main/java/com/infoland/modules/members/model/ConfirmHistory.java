package com.infoland.modules.members.model;

import java.util.List;

import com.infoland.modules.members.model.base.BaseConfirmHistory;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class ConfirmHistory extends BaseConfirmHistory<ConfirmHistory> {
	public static final ConfirmHistory dao = new ConfirmHistory();
	/**
	 * 有效性确认记录
	 * @author lixp
	 * @param pageNumber     页码
	 * @param pageSize		   每页显示行数
	 * @param userName       会员名称
	 * @param standardCode   标准号
	 * @param standardName   标准名称
	 * @param startTime      发起开始时间
	 * @param endTime        发起结束时间
	 * @return               分页返回查询的数据
	 */
	public Page<ConfirmHistory> confirmList(Integer pageNumber, Integer pageSize, String userName, String standardCode,
			String standardName, String startTime, String endTime) {
		// 获取select请求头
		String sql = getSql("confirmSql.confirmSelect");
		// 创建集合存放对象
		Kv param = Kv.create();
		// 若对象不为空 放入集合中
		if (StrKit.notBlank(userName)) {
			param.set("userName", "%" + userName + "%");
		}
		if (StrKit.notBlank(standardCode)) {
			param.set("standardCode", "%" + standardCode + "%");
		}
		if (StrKit.notBlank(standardName)) {
			param.set("standardName", "%" + standardName + "%");
		}
		if (StrKit.notBlank(startTime) && StrKit.notBlank(endTime)) {
			param.set("startTime", startTime + " 00:00:00");
			param.set("endTime", endTime + " 23:59:59");
		}
		// 获取from之后SqlPara
		SqlPara sqlExcept = getSqlPara("confirmSql.confirmFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExcept.getSql(), sqlExcept.getPara());
	}

	/**
	 * 有效请确认记录list
	 * @author lixp
	 * @param pageNumber     页码
	 * @param pageSize		   每页显示行数
	 * @param userName       会员名称
	 * @param standardCode   标准号
	 * @param standardName   标准名称
	 * @param startTime      发起开始时间
	 * @param endTime        发起结束时间
	 * @return               返回查询的所有数据
	 */
	public List<ConfirmHistory> exportExcel(String userName, String standardCode, String standardName, String startTime,
			String endTime) {
		// 获取select请求头
		String sql = getSql("confirmSql.confirmSelect");
		// 创建集合存放对象
		Kv param = Kv.create();
		// 若对象不为空且不为undefined放入集合中
		if (StrKit.notBlank(userName) && !userName.equals("undefined")) {
			param.set("userName", "%" + userName + "%");
		}
		if (StrKit.notBlank(standardCode) && !standardCode.equals("undefined")) {
			param.set("standardCode", "%" + standardCode + "%");
		}
		if (StrKit.notBlank(standardName) && !standardName.equals("undefined")) {
			param.set("standardName", "%" + standardName + "%");
		}
		if(!startTime.equals("undefined") && !endTime.equals("undefined")) {
			if (StrKit.notBlank(startTime) && StrKit.notBlank(endTime)) {
				param.set("startTime", startTime + " 00:00:00");
				param.set("endTime", endTime + " 23:59:59");
			}
		}
		// 获取from之后SqlPara
		SqlPara sqlExcept = getSqlPara("confirmSql.confirmFrom", param);
		return find(sql+sqlExcept.getSql(), sqlExcept.getPara());
	}
}
