package com.infoland.modules.members.controller;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.infoland.app.Constants;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.TrackingInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.members.service.MembersService;

/**
 * 
 * @Description 标准跟踪Controller层
 * @author Wsy
 * @date 2017年7月17日 下午4:59:14 
 * @version V1.3.1
 */
public class TrackingController extends BaseController<TrackingInfo>{
	
	/**
	 * 
	 * @Description 跟踪定制，并保存定制信息到本地数据库
	 * @author Wsy
	 */
	public void saveTrackingInfo() {
		String standardId = getJSONPara("standard_id");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.saveTrackingInfo(userInfo.getId(), userInfo.getUserName(), standardId);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 获得跟踪列表
	 * @author Wsy
	 */
	public void findAllList() {
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		String pageNumber = getJSONPara("pageNumber","1");
		Message message = MembersService.service.findTrackList(userInfo.getUserName(), pageNumber);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description  	跟踪标准付费
	 * @author Wsy
	 */
	public void trackingInfoOrder() {
		String orgName = getJSONPara("orgName");
		String address = getJSONPara("address");
		String contact = getJSONPara("contact");
		String email = getJSONPara("email");
		String phone = getJSONPara("phone");
		List<String> standardList = new ArrayList<String>();
		JSONArray list = getJSONArray("standard_list");
		for (Object object : list) {
			String a = object.toString();
			standardList.add(a);
		}
		Message message = MembersService.service.trackingInfoOrder(standardList, orgName, address, contact, email, phone);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description		推送知道标记
	 * @author Wsy
	 */
	public void trackingInfoPush() {
		List<String> pashId = new ArrayList<String>(); 
		List<Object> pashList = getJSONArray("ids");
		for (Object object : pashList) {
			String e = object.toString();
			pashId.add(e);
		}
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.trackingInfoPush(userInfo.getUserName(), pashId);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description		获取推送消息列表
	 * @author Wsy
	 */
	public void trackingInfoPushList() {
		String type = getJSONPara("type", "");
		String pageNumber = getJSONPara("pageNumber");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.trackingInfoPushList(userInfo.getUserName(), type, pageNumber);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description		获取标记推送消息列表
	 * @author Wsy
	 */
	public void trackingInfoPushFlag() {
		Integer pageNumber = getJSONParaToInt("pageNumber",1);
		String type = getJSONPara("type", "");
		String startTime = getJSONPara("startDate", "");
		String endTime = getJSONPara("endDate", "");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.trackingInfoPushFlag(userInfo.getUserName(), type, startTime, endTime, pageNumber);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 导出跟踪数据到excel
	 * @author yinyg
	 */
	public void exportTrackingData(){
		String json = getPara("data");
		JSONArray jsonArray = JSONArray.parseArray(json);
		MembersService.service.exportTrackingData(jsonArray, this);
		renderNull();
	}
	
	/**
	 * 
	 * @Description 导出跟踪列表
	 * @author yinyg
	 */
	public void exportTrackingList(){
		String json = getPara("data");
		List<TrackingInfo> trackingInfos = JSONArray.parseArray(json, TrackingInfo.class);
		MembersService.service.exportTrackingList(trackingInfos, this);
		renderNull();
	}
}
