package com.infoland.modules.members.controller;

import com.infoland.app.Constants;
import com.infoland.common.util.RequestUtil;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.members.service.MembersService;
import com.jfinal.captcha.CaptchaRender;

/** 
 * @Description 
 * @author Wsy
 * @date 2017年7月26日 下午1:17:08 
 * @version V1.3.1
 */ 
  	
public class MembersController extends BaseController<UserInfo>{
	
	/**
	 * 
	 * @Description	调用中标院的接口验证用户名是否可用
	 * @author Wsy
	 */
	public void loginNameCheck() {
		String userName = getJSONPara("userName");
		Message message = MembersService.service.loginNameCheck(userName);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 用户注册
	 * @author Wsy
	 */
	public void registered() {
		UserInfo userInfo = getJSONModel("userInfo",UserInfo.class);
		//设置用户访问ip
		userInfo.setModifyIp(RequestUtil.ip(this.getRequest()));
		Message message = MembersService.service.registered(userInfo);
		if(message.isSuccess()){
			getSession().setAttribute(Constants.SESSION_NAME, userInfo);
		}
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 用户登录验证
	 * @author Wsy
	 */
	public void login() {
		String userName = getJSONPara("username");
		String password = getJSONPara("password");
		String loginCaptcha = getJSONPara("loginCaptcha");
		Message message = new Message();
		if(CaptchaRender.validate(this, loginCaptcha)){
			message = MembersService.service.login(userName, password, RequestUtil.ip(this.getRequest()));
		}else{
			message.setSuccess(Boolean.FALSE);
			message.setMessage("验证码错误！");
		}
		if(message.isSuccess()){
			UserInfo userInfo = (UserInfo)MembersService.service.getUserInfo(userName).getObject();
			getSession().setAttribute(Constants.SESSION_NAME, userInfo);
		}
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 更新用户信息
	 * @author Wsy
	 */
	public void updateUserInfo() {
		UserInfo userInfo = getJSONModel("userInfo",UserInfo.class);
		Message message = MembersService.service.updateUserInfo(userInfo);
		if(message.isSuccess()){
			getSession().setAttribute(Constants.SESSION_NAME, userInfo);
		}
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description  从Session中获取用户信息
	 * @author yinyg
	 */
	public void getSessionUserInfo(){
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		//从Session中获取用户
		Object obj = getSession().getAttribute(Constants.SESSION_NAME);
		//判断用户是否为""， 为空直接返回 否则强转型报错
		if(obj instanceof String){
			System.out.println(obj.toString());
			message.setMessage("用户未登录");
			renderJson(message);
			return ;
		}
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		if(userInfo == null){
			message.setMessage("用户未登录");
		}else{
			message.setObject(userInfo);
			message.setSuccess(Boolean.TRUE);
		}
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 获取用户信息
	 * @author Wsy
	 */
	public void getUserInfo() {
		String userName = getJSONPara("userName");
		Message message = MembersService.service.getUserInfo(userName);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description	显示用户能访问的标准品种
	 * @author Wsy
	 * @return		标准品种对象
	 */
	public void getUserStandardSortList() {
		Integer pageNumber = getJSONParaToInt("pageNumber",1);
		Message message = MembersService.service.getUserStandardSortList(pageNumber);
		renderJson(message);
	}
	/**
	 * 
	 * @Description  验证码验证
	 * @author yinyg
	 */
	public void validLoginCaptcha(){
		//获取前台验证码
		String loginCaptcha = getJSONPara("loginCaptcha");
		Message message = new Message();
		message.setSuccess(Boolean.FALSE);
		if(CaptchaRender.validate(this, loginCaptcha)){
			message.setSuccess(Boolean.TRUE);
		}else{
			message.setMessage("验证码错误！");
		}
		renderJson(message);
	}
	
	
	/**
	 * 
	 * @Description 发送验证邮件
	 * @author yinyg
	 */
	public void sendUpdateEmail(){
		String userName = getJSONPara("userName");
		String email = getJSONPara("email");
		UserInfo userInfo =  (UserInfo)MembersService.service.getUserInfo(userName).getObject();
		Message message = new Message();
		if(userInfo.getEmail().equals(email)){
			message = MembersService.service.sendUpdateEmail(userName, email, this);
		}else{
			message.setSuccess(Boolean.FALSE);
			message.setMessage("邮箱验证错误，请输入正确邮箱");
		}
		renderJson(message);
	}
	/**
	 * 
	 * @Description 验证邮件
	 * @author yinyg
	 */
	public void validEmail(){
		String userName = getPara(0);
		String originalCode = getPara(1);
		Message message = MembersService.service.validEmail(userName, originalCode, this);
		if(message.isSuccess()){
			redirect("/#/forget_password_success/"+userName+"-"+originalCode);
		}else{
			redirect("/#/forget_password");
		}
	}
	/**
	 * 
	 * @Description 验证成功后更新用户信息 
	 * @author yinyg
	 */
	public void successValid(){
		String userName = getJSONPara("userName").split("-")[0];
		String updatePassword = getJSONPara("updatePassword");
		Message message = MembersService.service.getUserInfo(userName);
		if(message.getObject() != null){
			UserInfo user = (UserInfo)message.getObject();
			user.setUserPassword(updatePassword);
			message = MembersService.service.updateUserInfo(user);
		}
		renderJson(message);
	}
	/**
	 * 
	 * @Description 验证用户密码是否正确
	 * @author yinyg 
	 * @return
	 */
	public void checkPassword(){
		String userName = getJSONPara("userName");
		String password = getJSONPara("password"); 
		Message message = MembersService.service.checkPassword(userName, password);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 从session中清除用户信息
	 * @author yinyg
	 */
	public void exitUser(){
		Message message = new Message();
		getSession().setAttribute(Constants.SESSION_NAME, "");
		message.setSuccess(Boolean.TRUE);
		message.setMessage("用户已退出！");
		renderJson(message);
	}
	
}
