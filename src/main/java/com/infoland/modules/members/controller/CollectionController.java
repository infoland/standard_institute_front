package com.infoland.modules.members.controller;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.infoland.app.Constants;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.CollectionInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.members.service.MembersService;

/** 
 * @Description 标准收藏控制层
 * @author Wsy
 * @date 2017年7月24日 上午9:40:40 
 * @version V1.3.1
 */ 
  	
public class CollectionController extends BaseController<CollectionInfo>{
	/**
	 * 
	 * @Description 保存收藏信息
	 * @author Wsy
	 */
	public void saveCollect() {
		String standardId = getJSONPara("standard_id");
		Integer id = getJSONParaToInt("id");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.saveCollect(userInfo.getUserName(), standardId, id);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description	获取收藏信息列表 
	 * @author Wsy
	 */
	public void getCollectList() {
		String pageNumber = getJSONPara("pageNumber","1");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.getCollectList(userInfo.getUserName(), pageNumber);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 删除收藏记录
	 * @author Wsy
	 */
	public void delCollect() {
		String standardId = getJSONPara("standard_id");
		String standardCode = getJSONPara("standard_code");
		//获取session中储存的用户信息
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.delCollect(userInfo.getId(), userInfo.getUserName(), standardId,standardCode);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 删除选中收藏记录 批量删除
	 * @author yinyg
	 */
	public void delSelectCollect(){
		List<CollectionInfo> collectionInfos = getJSONModelList("standardArr", CollectionInfo.class);
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = MembersService.service.delSelectCollect(userInfo.getId(), userInfo.getUserName(), collectionInfos);
		renderJson(message);
	}
	
	/**
	 * 
	 * @Description 导出收藏列表到Excel
	 * @author yinyg
	 */
	public void exportDate(){
		String json = getPara("data");
		List<CollectionInfo> collectionInfos = JSONArray.parseArray(json, CollectionInfo.class);
		MembersService.service.exportDate(collectionInfos, this);
		renderNull();
	}
}
