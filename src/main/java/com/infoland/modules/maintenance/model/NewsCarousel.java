package com.infoland.modules.maintenance.model;

import com.infoland.modules.maintenance.model.base.BaseNewsCarousel;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class NewsCarousel extends BaseNewsCarousel<NewsCarousel> {
	public static final NewsCarousel dao = new NewsCarousel().dao();

}
