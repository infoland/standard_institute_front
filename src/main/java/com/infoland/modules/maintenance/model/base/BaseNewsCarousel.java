package com.infoland.modules.maintenance.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseNewsCarousel<M extends BaseNewsCarousel<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setNewsNum(java.lang.String newsNum) {
		set("news_num", newsNum);
	}

	public java.lang.String getNewsNum() {
		return get("news_num");
	}

	public void setNewsName(java.lang.String newsName) {
		set("news_name", newsName);
	}

	public java.lang.String getNewsName() {
		return get("news_name");
	}

	public void setCarouselStatus(java.lang.Integer carouselStatus) {
		set("carousel_status", carouselStatus);
	}

	public java.lang.Integer getCarouselStatus() {
		return get("carousel_status");
	}

	public void setCarouselStatusV(java.lang.String carouselStatusV) {
		set("carousel_status_v", carouselStatusV);
	}

	public java.lang.String getCarouselStatusV() {
		return get("carousel_status_v");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}

	public java.util.Date getCreateDate() {
		return get("create_date");
	}

}
