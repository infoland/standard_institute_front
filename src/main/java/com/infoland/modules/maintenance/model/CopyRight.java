package com.infoland.modules.maintenance.model;

import com.infoland.modules.maintenance.model.base.BaseCopyRight;
import com.jfinal.plugin.activerecord.SqlPara;

@SuppressWarnings("serial")
public class CopyRight extends BaseCopyRight<CopyRight> {
	public static final CopyRight dao = new CopyRight();
	
	/**
	 * 版权信息数据列表
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public CopyRight findAll(){
		SqlPara sql = getSqlPara("bzyCopyRightsql.bzyCopyRightSelect");
		return findFirst(sql.getSql());
	}
}
