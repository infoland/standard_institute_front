package com.infoland.modules.maintenance.model;

import java.util.List;

import com.infoland.modules.maintenance.model.base.BaseDict;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

@SuppressWarnings("serial")
public class Dict extends BaseDict<Dict> {
	public static final Dict dao = new Dict();
	/**
	 * 查询标准状态
	 * @Description 
	 * @author dw
	 * @return
	 */
	public List<Dict> standardList(){
		SqlPara sql = getSqlPara("dict.dictStandard");
		return find(sql.getSql());
	}
	
	/**
	 * 查询高级检索时间
	 * @Description 
	 * @author dw
	 * @return
	 */
	public List<Dict> standardTimeList(){
		SqlPara sql = getSqlPara("dict.standardTime");
		return find(sql.getSql());
	}
	
	
	/**
	 * 
	 * @Description 根据类型获取字典表信息
	 * @author yinyg
	 * @param typeName
	 * @return
	 */
	public List<Dict> listDictByType(String typeName){
		Kv kv = new Kv();
		kv.set("typeName", typeName);
		SqlPara sql = getSqlPara("dict.listDictByType", kv);
		return find(sql.getSql(), typeName);
	}
}
