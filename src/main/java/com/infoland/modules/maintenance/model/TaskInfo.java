package com.infoland.modules.maintenance.model;

import com.infoland.modules.maintenance.model.base.BaseTaskInfo;

/**
 * 定时任务model,与数据库交互
 * @author lixp
 *
 */
  	
@SuppressWarnings("serial")
public class TaskInfo extends BaseTaskInfo<TaskInfo> {
	public static final TaskInfo dao = new TaskInfo().dao();
	
}

