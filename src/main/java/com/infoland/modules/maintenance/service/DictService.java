package com.infoland.modules.maintenance.service;

import java.util.List;

import com.infoland.modules.maintenance.model.Dict;
import com.infoland.modules.maintenance.service.impl.DictServiceImpl;
import com.jfinal.aop.Duang;

/**
 * @author yanglj
 */
public interface DictService {
	
	public static final DictService service = Duang.duang(DictServiceImpl.class);
	/**
	 * 字典订单状态查询
	 * @Description 
	 * @author dw
	 * @return
	 */
	List<Dict> statusList();
	
	/**
	 * 
	 * @Description 根据类型获取字典列表
	 * @author yinyg
	 * @param typeName 类型
	 * @return
	 */
	List<Dict> listDictByType(String typeName);
}
