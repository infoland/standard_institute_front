package com.infoland.modules.maintenance.service.impl;

import java.util.List;

import com.infoland.modules.maintenance.model.Dict;
import com.infoland.modules.maintenance.service.DictService;

/**
 * 
 * @Description 字典查询 DictServiceImpl
 * @author Yanglj
 * @date 2017年6月27日 下午5:52:14 
 * @version V1.3.1
 */
public class DictServiceImpl implements DictService {

	/**
	 * 字典订单状态查询
	 * Description 
	 * @return
	 */
	@Override
	public List<Dict> statusList() {
		return Dict.dao.standardList();
	}

	@Override
	public List<Dict> listDictByType(String typeName) {
		return Dict.dao.listDictByType(typeName);
	}
	
}
