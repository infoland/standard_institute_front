/**
 * 
 */
package com.infoland.modules.maintenance.service;

import com.infoland.modules.maintenance.model.CopyRight;
import com.infoland.modules.maintenance.service.impl.CopyServiceImpl;
import com.jfinal.aop.Duang;

/**
 * 维护管理  20170622
 * @author Mandarava
 *
 */
public interface CopyService{

	public static final CopyService service = Duang.duang(CopyServiceImpl.class);
	
	/**
	 * 查询版权信息
	 * @Description 
	 * @author dw
	 * @return
	 */
	CopyRight findAll();
	
}
