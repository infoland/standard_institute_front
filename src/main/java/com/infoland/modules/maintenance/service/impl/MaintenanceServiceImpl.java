
/**
 * @Title: AdminServiceImpl.java
 * @Package: com.aft.cdcb.modules.admin.service.impl
 * @Description: TODO
 * @author Mandarava
 * @date 2016年8月20日 下午5:55:39
 * @version 1.3.1
 * @Email wuyan1688@qq.com
 */

package com.infoland.modules.maintenance.service.impl;

import com.infoland.modules.maintenance.service.MaintenanceService;

/**
 * 维护管理服务层
 *
 * @author Mandarava
 * @date 20170622
 * @version V1.3.1
 */

public class MaintenanceServiceImpl implements MaintenanceService {

}
