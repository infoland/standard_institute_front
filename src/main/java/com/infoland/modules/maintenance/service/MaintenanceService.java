/**
 * 
 */
package com.infoland.modules.maintenance.service;

import com.infoland.modules.maintenance.service.impl.MaintenanceServiceImpl;
import com.jfinal.aop.Duang;

/**
 * 维护管理  20170622
 * @author Mandarava
 *
 */
public interface MaintenanceService{

	public static final MaintenanceService service = Duang.duang(MaintenanceServiceImpl.class);
}
