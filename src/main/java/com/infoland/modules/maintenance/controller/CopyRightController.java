package com.infoland.modules.maintenance.controller;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.maintenance.model.CopyRight;
import com.infoland.modules.maintenance.service.CopyService;
/**
 * 版权信息
 * @Description 
 * @author dw
 * @date 2017年7月26日 上午10:27:04 
 * @version V1.3.1
 */
public class CopyRightController extends BaseController<CopyRight> {
	
	/**
	 * 查询版权信息
	 * @Description 
	 * @author dw
	 */
	public void findAll(){
		renderJson(CopyService.service.findAll());
	}
}
