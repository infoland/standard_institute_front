package com.infoland.modules.maintenance.controller;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.maintenance.model.Dict;
import com.infoland.modules.maintenance.service.DictService;

/**
 * 字典列表控制层
 * @author lixp
 *
 */
public class DictController extends BaseController<Dict> {
	
	/**
	 * 字典订单状态查询
	 * @Description 
	 * @author dw
	 */
	public void statusList(){
		renderJson(DictService.service.statusList());
	}
	
	/**
	 * 
	 * @Description 根据类型获取字典列表
	 * @author yinyg
	 */
	public void listDictByType(){
		String typeName = getJSONPara("typeName"); //字典类型
		renderJson(DictService.service.listDictByType(typeName));
	}
}
