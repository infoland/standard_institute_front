package com.infoland.modules.orders.controller;


import com.alibaba.fastjson.JSONArray;
import com.infoland.app.Constants;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.orders.model.Cart;
import com.infoland.modules.orders.service.OrderService;

public class CartController extends BaseController<Cart>{
	
	/**
	 * 根据登录的用户id查看购物车列表数据
	 * @Description 
	 * @author dw
	 */
	public void cartList(){
		Integer pageNumber = getJSONParaToInt("pageNumber",1);
		Integer pageSize = getJSONParaToInt("pageSize",8);
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		renderJson(OrderService.service.cartList(pageNumber, pageSize,userInfo.getId()));
	}
	
	/**
	 * 单个/批量删除购物车订单
	 * @Description 
	 * @author dw
	 */
	public void deleteCartById(){
		JSONArray array = getJSONArray("ids");
		renderJson(OrderService.service.deletCartById(array));
	}
	
	/**
	 * 根据用户id和数据id查询购物车某条数据
	 * @Description 
	 * @author dw
	 */
	public void findOneCart(){
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		String standardCode = getJSONPara("code");
		renderJson(OrderService.service.findOneCart(userInfo.getId(),standardCode));
	}
	
}