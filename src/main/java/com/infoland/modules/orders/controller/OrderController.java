package com.infoland.modules.orders.controller;


import com.infoland.app.Constants;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.ConsigneeInfo;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.orders.model.Order;
import com.infoland.modules.orders.service.OrderService;


public class OrderController extends BaseController<Order>{
	/**
	 * 订购
	 * @Description 
	 * @author dw
	 */
	public void addCart(){
		renderJson(OrderService.service.addCart(this));
	}
	
	/**
	 * 保存人收货人信息
	 * @Description 
	 * @author dw
	 */
	public void saveConsignee(){
		ConsigneeInfo consigneeInfo = getJSONModel(ConsigneeInfo.class);
		renderJson(OrderService.service.saveConsignee(consigneeInfo,this));
	}
	
	/**
	 * 更新收获地址
	 * @Description 
	 * @author dw
	 */
	public void updateConsignee(){
		ConsigneeInfo consigneeInfo = getJSONModel(ConsigneeInfo.class);
		renderJson(OrderService.service.upateConsignee(consigneeInfo,this));
	}
	
	/**
	 * 单条删除订单(更改订单在购物车的状态和订单的交易状态)
	 * @Description 
	 * @author dw
	 */
	public void deleteOneOrder(){
		String id = getJSONPara("orderCode");
		renderJson(OrderService.service.deleteOneOrder(id));
	}
	
	/**
	 * 批量删除订单(批量修改订单在购物车的状态和订单交易状态)
	 * @Description 
	 * @author dw
	 */
	public void  deleteBacthOrder(){
		Object[] ids = getJSONValues("ids");
		Message message = OrderService.service.deleteBacthOrder(ids);
		renderJson(message);
	}
	
	/**
	 * 调用对账单接口
	 * @Description 
	 * @author dw
	 */
	public void  AuditList(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",15);
		String relatedid = getJSONPara("relatedid");
		String relatedtype = getJSONPara("relatedtype");
		Message message = OrderService.service.AuditList(pageNumber, pageSize, relatedid, relatedtype);
		renderJson(message);
	}
	
	/**
	 * 调用账目信息接口
	 * @Description 
	 * @author dw
	 */
	public void  itemAuditList(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",5);
		String auditId = getJSONPara("auditId");
		Message message = OrderService.service.itemAuditList(pageNumber, pageSize, auditId);
		renderJson(message);
	}
	
	/**
	 * 设置默认地址
	 * @Description 
	 * @author dw
	 */
	public void  isDefault(){
		renderJson(OrderService.service.isDefault(this));
	}
	
	/**
	 * 根据登录的用户id查询默认地址
	 * @Description 
	 * @author dw
	 */
	public void  findIsDefaultAddress(){
		UserInfo userInfo = (UserInfo) getSession().getAttribute(Constants.SESSION_NAME);
		renderJson(OrderService.service.findIsDefaultAddress(userInfo.getId()));
	}
	
	/**
	 * 删除地址
	 * @Description 
	 * @author dw
	 */
	public void deleteAddress(){
		Integer id = getParaToInt(0);
		renderJson(OrderService.service.deleteAddress(id));
	}
	
	/**
	 * 编辑地址获取
	 * @Description 
	 * @author dw
	 */
	public void findById(){
		Integer id = getParaToInt(0);
		renderJson(OrderService.service.findById(id));
	}
	
	/**
	 * 待处理订单集合
	 * @Description 
	 * @author dw
	 */
	public void handleOrderList() {
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",5);
		String code = getJSONPara("orderCode");
		String startDate = getJSONPara("startDate");
		String endDate = getJSONPara("endDate");
		UserInfo userInfo = (UserInfo) getSession().getAttribute(Constants.SESSION_NAME);
		renderJson(OrderService.service.handleOrderList(pageNumber, pageSize,code,startDate,endDate,userInfo.getId()));
	}
	
	/**
	 * 已处理订单集合
	 * @Description 
	 * @author dw
	 */
	public void proccessOrderList() {
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",5);
		String code = getJSONPara("orderCode");
		String startDate = getJSONPara("startDate");
		String endDate = getJSONPara("endDate");
		UserInfo userInfo = (UserInfo) getSession().getAttribute(Constants.SESSION_NAME);
		renderJson(OrderService.service.processedOrderList(pageNumber, pageSize,code,startDate,endDate,userInfo.getId()));
	}
	
	/**
	 * 已废弃订单集合 
	 * @Description 
	 * @author dw
	 */
	public void abandonOrderList() {
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",5);
		String code = getJSONPara("orderCode");
		String startDate = getJSONPara("startDate");
		String endDate = getJSONPara("endDate");
		UserInfo userInfo = (UserInfo) getSession().getAttribute(Constants.SESSION_NAME);
		renderJson(OrderService.service.abandonOrderList(pageNumber, pageSize,code,startDate,endDate,userInfo.getId()));
	}
	
	/**
	 * 根据订单id查看订单详情
	 * @Description 
	 * @author dw
	 */
	public void findOrderId(){
		String orderCode = getJSONPara("code");
		renderJson(OrderService.service.findByOrderId(orderCode));
	}
	
	/**
	 * 撤销订单
	 * @Description 
	 * @author dw
	 */
    public void cancleOrder(){
    	String orderCode = getJSONPara("code");
    	renderJson(OrderService.service.cancleOrder(orderCode));
    }
    
    /**
     * 提交订单
     * @Description 
     * @author dw
     */
    public void saveOrder(){
		renderJson(OrderService.service.saveOrder(this));
	}
	
}
