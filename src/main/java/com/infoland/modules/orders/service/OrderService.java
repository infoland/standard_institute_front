package com.infoland.modules.orders.service;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.ConsigneeInfo;
import com.infoland.modules.orders.controller.OrderController;
import com.infoland.modules.orders.model.Cart;
import com.infoland.modules.orders.model.Order;
import com.infoland.modules.orders.model.OrderItem;
import com.infoland.modules.orders.service.impl.OrderServiceImpl;
import com.jfinal.aop.Duang;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

import ws.client.ThiOrderWrap;


public interface OrderService{

	public static final OrderService service = Duang.duang(OrderServiceImpl.class);
	
	/**
	 * 订购
	 * @Description 
	 * @author dw
	 * @param cart 购物车对象
	 * @return
	 */
	Message addCart(OrderController orderController);
	
	/**
	 * 保存收货人信息
	 * @Description 
	 * @author dw
	 * @param consigneeInfo 要保存的收获信息对象
	 * @return
	 */
	Message saveConsignee(ConsigneeInfo consigneeInfo,Controller controller);
	
	/**
	 * 更新收获地址
	 * @Description 
	 * @author dw
	 * @param consigneeInfo
	 * @param controller
	 * @return
	 */
	Message upateConsignee(ConsigneeInfo consigneeInfo,Controller controller);
	
	/**
	 * 单条删除订单(修改订单交易状态)
	 * @Description 
	 * @author dw
	 * @param consigneeInfo
	 * @param id 订单id
	 * @return
	 */
	Message deleteOneOrder(String orderInfo);
	
	/**
	 * 批量删除订单(批量修改订单在购物车的状态和订单交易状态)
	 * @Description 
	 * @author dw
	 * @param id
	 * @return
	 */
	Message deleteBacthOrder(Object[]id);
	
	/**
	 * 调用中标院接口生成订单
	 * @Description 
	 * @author dw
	 * @param id 订单id
	 * @return 
	 */
	Message createOrder(List<ThiOrderWrap> orderInfo);
	
	/**
	 * 对账单信息
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Message AuditList(Integer pageNumber,Integer pageSize,String relatedid,String relatedtype);
	
	/**
	 * 
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param auditId 对账单主键
	 * @return
	 */
	Message itemAuditList(Integer pageNumber,Integer pageSize,String auditId);
	
	/**
	 * 更新地址为默认地址
	 * @Description 
	 * @author dw
	 * @param controller
	 * @return
	 */
	Message isDefault(Controller controller);
	/**
	 * 根据登录的用户id查询默认地址
	 * @Description 
	 * @author dw
	 * @param userId
	 * @return
	 */
	List<ConsigneeInfo> findIsDefaultAddress(Integer userId);
	
	/**
	 * 删除地址
	 * @Description 
	 * @author dw
	 * @param id
	 * @return
	 */
	Message deleteAddress(Integer id);
	/**
	 * 编辑获取地址信息
	 * @Description 
	 * @author dw
	 * @param consigneeInfo
	 * @return
	 */
	ConsigneeInfo findById(Integer id);
	
	/**
	 * 根据登录的用户id查看购物车列表数据
	 * @Description 
	 * @author dw
	 * @param userInfoId 登录的用户id
	 * @return
	 */
	Page<Cart> cartList(Integer pageNumber,Integer pageSize,Integer userInfoId);
	
	/**
	 * 单个删除/批量删除购物车列表
	 * @Description 
	 * @author dw
	 * @param array
	 * @return
	 */
	Message deletCartById(JSONArray array);
	/**
	 * 待处理订单集合
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param code
	 * @param payFlagV
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Page<Order> handleOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId);
	
	/**
	 * 已处理订单集合
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param code
	 * @param payFlagV
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Page<Order> processedOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId);
	
	/**
	 * 已废弃订单集合
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param code
	 * @param payFlagV
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Page<Order> abandonOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId);
	
	/**
	 * 根据订单id查看订单详情
	 * @Description 
	 * @author dw
	 * @param id
	 * @return
	 */
	List<OrderItem> findByOrderId(String orderCode);
	
	/**
	 * 根据订单id撤销订单(状态由待付款---已废弃)
	 * @Description 
	 * @author dw
	 * @param id 订单id
	 * @return
	 */
	Message cancleOrder(String orderCode);
	
	/**
	 * 提交订单
	 * @Description 
	 * @author dw
	 * @param cart
	 * @return
	 */
	Message saveOrder(OrderController orderController);
	
	/**
	 * 根据用户id和数据id查询购物车某条数据
	 * @Description 
	 * @author dw
	 * @param userInfoId
	 * @param cartId
	 * @return
	 */
	Cart findOneCart(Integer userInfoId,String standardCode);
}
