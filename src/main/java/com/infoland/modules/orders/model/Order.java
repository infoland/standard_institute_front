package com.infoland.modules.orders.model;


import com.infoland.modules.orders.model.base.BaseOrder;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 前台--订单管理
 * @Description 
 * @author dw
 * @date 2017年7月18日 下午1:03:48 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class Order extends BaseOrder<Order> {
	public static final Order dao = new Order().dao();
	
	/**
	 * 批量删除订单(批量修改订单在购物车的状态和订单交易状态)
	 * @Description 
	 * @author dw
	 * @param flag 交易状态
	 * @param flagV 交易状态值
	 * @param ids 订单id
	 * @return
	 */
	public Boolean deleteOrders(Integer flag,String flagV,Object id) {
		Kv param = Kv.create();
		param.set("flag",flag);
		param.set("flagV",flagV);
		param.set("id", id);
		SqlPara sql = getSqlPara("updateSql.updateSelect",param);
		return Db.update(sql.getSql(),sql.getPara()) > 0;
	}
	
	/**
	 * 待处理订单
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param order
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Page<Order> handleOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId){
		String sql = getSql("bzyOrdersql.bzyOrderSelect");
		Kv param = Kv.create();
		param.set("userInfoId",userInfoId);
		if(StrKit.notBlank(code)){
			param.set("code", "%"+code+"%");
		}
		if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)){
			param.set("startDate", startDate+" 00:00:00");
			param.set("endDate",endDate+" 23:59:59");
		}
		SqlPara sqlExceptSelect = getSqlPara("bzyOrdersql.bzyOrderFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
		
	}
	/**
	 * 已处理订单数据列表
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param orderCode 订单号
	 * @param userName 用户名
	 * @param startDate 创建订单时间
	 * @param endDate 下单结束时间
	 * @return
	 */
	public Page<Order> processedOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId){
		//select请求头
		String sql = getSql("payOrdersql.payOrderSelect");
		Kv param = Kv.create();
		param.set("userInfoId",userInfoId);
		if(StrKit.notBlank(code)){
			param.set("code", "%"+code+"%");
		}
		if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)){
			param.set("startDate", startDate+" 00:00:00");
			param.set("endDate",endDate+" 23:59:59");
		}
		SqlPara sqlExceptSelect = getSqlPara("payOrdersql.payOrderFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
	}
	
	/**
	 * 已废弃订单数据列表
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param orderCode 订单号
	 * @param userName 用户名
	 * @param startDate 创建订单时间
	 * @param endDate 下单结束时间
	 * @return
	 */
	public Page<Order> abandonOrderList(Integer pageNumber,Integer pageSize,String code,String startDate,String endDate,Integer userInfoId){
		String sql = getSql("abandonOrdersql.abandonOrderSelect");
		Kv param = Kv.create();
		param.set("userInfoId",userInfoId);
		if(StrKit.notBlank(code)){
			param.set("code", "%"+code+"%");
		}
		if(StrKit.notBlank(startDate) && StrKit.notBlank(endDate)){
			param.set("startDate", startDate+" 00:00:00");
			param.set("endDate",endDate+" 23:59:59");
		}
		SqlPara sqlExceptSelect = getSqlPara("abandonOrdersql.abandonOrderFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
	}
	
	
	/**
	 * 根据订单号查询某一条订单信息
	 * @Description 
	 * @author dw
	 * @param orderCode 订单号
	 * @return
	 */
	public Order findByOrderCode(String orderCode){
		Kv p = Kv.create();
		p.set("orderCode",orderCode);
		SqlPara sql = getSqlPara("orderSelect.mainOrderSelect",p);
		return findFirst(sql.getSql(),sql.getPara());
	}
}
