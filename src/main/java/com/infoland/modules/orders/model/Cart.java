package com.infoland.modules.orders.model;


import com.alibaba.fastjson.JSONArray;
import com.infoland.modules.orders.model.base.BaseCart;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 前台--订单管理
 * @Description 
 * @author dw
 * @date 2017年7月18日 下午1:03:48 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class Cart extends BaseCart<Cart> {
	public static final Cart dao = new Cart().dao();
	
	/**
	 * 根据登录的用户id查看购物车订单
	 * @Description 
	 * @author dw
	 * @param userInfoId 登录的用户id
	 * @return
	 */
	public Page<Cart> cartList(Integer pageNumber,Integer pageSize,Integer userInfoId){
		String sql = getSql("cartSql.cartList");
		Kv param = Kv.create();
		param.set("userInfoId",userInfoId);
		SqlPara sqlExceptSelect = getSqlPara("cartSql.cartFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
	}
	
	/**
	 * 删除订单
	 * @Description 
	 * @author dw
	 * @param array
	 * @return
	 */
	public int[] deleteCartById(JSONArray array){
		String sql = getSql("cartSql.delCartList");
		Object[][] param = new Object[array.size()][1];
		for(int i=0;i<param.length;i++){
			for(int j=0;j<1;j++){
				param[i][j] = array.get(i);
			}
		}
		return Db.batch(sql, param,array.size());
		
	}
	
	/**
	 * 根据id插叙购物车单条数据
	 * @Description 
	 * @author dw
	 * @param id
	 * @return
	 */
	public Cart findByCartId(String id){
		Kv param = Kv.create();
		param.set("id",id);
		SqlPara sql = getSqlPara("cartSql.findCart", param);
		return findFirst(sql.getSql(),sql.getPara());
	}
	
	/**
	 * 根据用户id和数据id查询购物车某条数据
	 * @Description 
	 * @author dw
	 * @param userInfoId
	 * @param cartId
	 * @return
	 */
	public Cart findOneCart(Integer userInfoId,String standardCode){
		Kv param = Kv.create();
		param.set("userInfoId",userInfoId);
		param.set("standardCode",standardCode);
		SqlPara sql = getSqlPara("findCartSql.findOneCart",param);
		return findFirst(sql.getSql(),sql.getPara());
	}
	
}
