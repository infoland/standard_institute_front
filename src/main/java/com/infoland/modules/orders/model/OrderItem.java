package com.infoland.modules.orders.model;



import java.util.List;

import com.infoland.modules.orders.model.base.BaseOrderItem;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 
 * @Description 
 * @author dw
 * @date 2017年7月18日 下午1:03:56 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class OrderItem extends BaseOrderItem<OrderItem> {
	public static final OrderItem dao = new OrderItem().dao();
	
	/**
	 * 根据订单id查看订单详情
	 * @Description 
	 * @author dw
	 * @param id
	 * @return
	 */
	public List<OrderItem> findById(String orderCode){
		Kv param  = Kv.create();
		param.set("orderCode",orderCode);
		SqlPara sql = getSqlPara("detailOrdersql.detailOrderSelect", param);
		return find(sql.getSql(),sql.getPara());
	}
	
	/**
	 * 根据主订单id查询子订单集合
	 * @Description 
	 * @author dw
	 * @param orderId 主订单id
	 * @return
	 */
	public List<OrderItem> findByMainOrderId(String orderCode){
		Kv param = Kv.create();
		param.set("orderCode",orderCode);
		SqlPara sql = getSqlPara("orderItemSql.orderItemSelect", param);
		return find(sql.getSql(),sql.getPara());
	}
}
