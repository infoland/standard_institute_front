package com.infoland.modules.orders.model.base;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCart<M extends BaseCart<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setGoodsType(java.lang.Integer goodsType) {
		set("goods_type", goodsType);
	}

	public java.lang.Integer getGoodsType() {
		return get("goods_type");
	}

	public void setIdentifier(java.lang.String identifier) {
		set("identifier", identifier);
	}

	public java.lang.String getIdentifier() {
		return get("identifier");
	}
	public void setStandardCode(java.lang.String standardCode) {
		set("standard_code", standardCode);
	}

	public java.lang.String getStandardCode() {
		return get("standard_code");
	}

	public void setStandardName(java.lang.String standardName) {
		set("standard_name", standardName);
	}

	public java.lang.String getStandardName() {
		return get("standard_name");
	}

	public void setPrice(java.math.BigDecimal price) {
		set("price", price);
	}

	public java.math.BigDecimal getPrice() {
		return get("price");
	}
	
	public void setCount(java.lang.Integer count) {
		set("count", count);
	}

	public java.lang.Integer getCount() {
		return get("count");
	}

	public void setFlag(java.lang.Integer flag) {
		set("flag", flag);
	}

	public java.lang.Integer getFlag() {
		return get("flag");
	}

	public void setFlagV(java.lang.String flagV) {
		set("flag_v", flagV);
	}

	public java.lang.String getFlagV() {
		return get("flag_v");
	}

	public void setUserInfoId(java.lang.Integer userInfoId) {
		set("user_info_id", userInfoId);
	}

	public java.lang.Integer getUserInfoId() {
		return get("user_info_id");
	}
	

}
