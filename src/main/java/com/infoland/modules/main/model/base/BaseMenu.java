package com.infoland.modules.main.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMenu<M extends BaseMenu<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}

	public java.lang.Long getId() {
		return get("id");
	}

	public void setMSort(java.lang.Integer mSort) {
		set("m_sort", mSort);
	}

	public java.lang.Integer getMSort() {
		return get("m_sort");
	}

	public void setMName(java.lang.String mName) {
		set("m_name", mName);
	}

	public java.lang.String getMName() {
		return get("m_name");
	}

	public void setMUrl(java.lang.String mUrl) {
		set("m_url", mUrl);
	}

	public java.lang.String getMUrl() {
		return get("m_url");
	}

	public void setMParentId(java.lang.Long mParentId) {
		set("m_parent_id", mParentId);
	}

	public java.lang.Long getMParentId() {
		return get("m_parent_id");
	}

	public void setMIcon(java.lang.String mIcon) {
		set("m_icon", mIcon);
	}

	public java.lang.String getMIcon() {
		return get("m_icon");
	}

	public void setMDescription(java.lang.String mDescription) {
		set("m_description", mDescription);
	}

	public java.lang.String getMDescription() {
		return get("m_description");
	}

	public void setMIsShow(java.lang.Boolean mIsShow) {
		set("m_is_show", mIsShow);
	}

	public java.lang.Boolean getMIsShow() {
		return get("m_is_show");
	}

	public void setMIsNewOpen(java.lang.Boolean mIsNewOpen) {
		set("m_is_new_open", mIsNewOpen);
	}

	public java.lang.Boolean getMIsNewOpen() {
		return get("m_is_new_open");
	}

	public void setMIdeas(java.lang.String mIdeas) {
		set("m_ideas", mIdeas);
	}

	public java.lang.String getMIdeas() {
		return get("m_ideas");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("createDate", createDate);
	}

	public java.util.Date getCreateDate() {
		return get("createDate");
	}

	public void setModifyDate(java.util.Date modifyDate) {
		set("modifyDate", modifyDate);
	}

	public java.util.Date getModifyDate() {
		return get("modifyDate");
	}

}
