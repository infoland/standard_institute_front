package com.infoland.modules.main.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.infoland.modules.main.model.base.BaseMenu;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Menu extends BaseMenu<Menu> {
	public static final Menu dao = new Menu().dao();

	/**
	 * 获取菜单层级
	 * 
	 * @author Mandarava
	 * @return
	 */
	public Integer getLevel() {
		return getMIdeas().split(",").length;
	}

	/**
	 * <p>
	 * 查询最底层的菜单，
	 * </p>
	 * <p>
	 * 过滤掉当前菜单的所有父级，
	 * </p>
	 * <p>
	 * 并查询出该菜单下对应的功能，设置数据结构
	 * </p>
	 * 
	 * @author Mandarava
	 * @return
	 */
	public List<Menu> findLowestMenus() {
		String sql = "select menu.* from si_menu menu ";
		List<Menu> menus = getLeafNodes(find(sql));
		return getPermission(menus);
	}

	/**
	 * 递归获取最底层的菜单
	 * 
	 * @author Mandarava
	 * @param menus
	 * @return
	 */
	private List<Menu> getLeafNodes(List<Menu> menus) {
		List<Menu> leafList = new ArrayList<Menu>();
		for (Menu menu : menus) {
			List<Menu> children = menu.children(menus);
			if (!children.isEmpty()) {
				getLeafNodes(children);
			} else {
				leafList.add(menu);
			}
		}
		return sort(leafList);
	}

	/**
	 * 查询某菜单下的子菜单
	 * 
	 * @author Mandarava
	 * @return
	 */
	public List<Menu> findChildren() {
		String sql = "select menu.* from si_menu menu where menu.m_parent_id = ? order by menu.m_sort";
		return find(sql, getId());
	}

	/**
	 * 某菜单下是否有子菜单
	 * 
	 * @author Mandarava
	 * @return
	 */
	public Boolean hasChildren() {
		return findChildren().size() > 0;
	}

	/**
	 * 查询全部菜单，设置父子结构
	 * 
	 * @author Mandarava
	 * @return
	 */
	public List<Menu> findAllMenus() {
		String sql = "select menu.* from si_menu menu ";
		List<Menu> list = find(sql);
		return loadLevels(list, root(list));
	}

	/**
	 * 获取菜单下的所有操作权限并设置数据结构
	 * 
	 * @author Mandarava
	 * @param menus
	 * @return
	 */
	private List<Menu> getPermission(List<Menu> menus) {
		for (Menu menu : menus) {
			menu.put("permissionList", menu.getFunctions());
		}
		return menus;
	}

	/**
	 * 获取某菜单下的所有操作权限
	 * 
	 * @author Mandarava
	 * @return
	 */
	private List<Permission> getFunctions() {
		return Permission.dao.findByMenuIdOnMenuFun(getId());
	}

	/**
	 * 根据用户ID查询并返回该用户拥有的树形菜单
	 * 
	 * @author Mandarava
	 * @param userId
	 * @return
	 */
	public List<Menu> findMenusByUserId(Long adminId) {
		String sql = (new StringBuilder()).append(" select  distinct(menu.id) menu_id,menu.*  ")
				.append(" from si_menu menu ").append(" join si_role_menu rm ").append(" on menu.id = rm.menu_id ")
				.append(" join si_role rol ").append(" on rm.role_id = rol.id ").append(" join si_role_admin ra ")
				.append(" on rol.id = ra.role_id ").append(" where ra.admin_id = ? and m_is_show = 1 ").toString();
		List<Menu> menus = find(sql, adminId);
		return loadLevels(menus, root(menus));
	}

	/**
	 * 递归设置树形菜单数据结构
	 * 
	 * @author Mandarava
	 * @param orgMenus
	 * @param current
	 * @return
	 */
	private List<Menu> loadLevels(List<Menu> orgMenus, List<Menu> root) {
		for (Menu menu : root) {
			List<Menu> children = menu.children(orgMenus);
			if (!children.isEmpty()) {
				menu.put("childrenList", children);
				loadLevels(orgMenus, children);
			}
		}
		return root;
	}

	/**
	 * 获取菜单根节点
	 * 
	 * @author Mandarava
	 * @param menus
	 * @return
	 */
	private List<Menu> root(List<Menu> menus) {
		List<Menu> rootList = new ArrayList<Menu>();
		for (Menu menu : menus) {
			if (null == menu.getMParentId()) {
				rootList.add(menu);
			}
		}
		return sort(rootList);
	}

	/**
	 * 获取某个菜单下的子节点
	 * 
	 * @author Mandarava
	 * @param menus
	 * @return
	 */
	public List<Menu> children(List<Menu> menus) {
		List<Menu> childrenList = new ArrayList<Menu>();
		for (Menu menu : menus) {
			if (getId().equals(menu.getMParentId()))
				childrenList.add(menu);
		}
		return sort(childrenList);
	}

	/**
	 * 排序
	 * 
	 * @author Mandarava
	 * @param menus
	 * @return
	 */
	public List<Menu> sort(List<Menu> menus) {
		java.util.Collections.sort(menus, new Comparator<Menu>() {
			@Override
			public int compare(Menu o1, Menu o2) {
				if (o1.getMSort() > o2.getMSort())
					return 1;
				else if (o1.getMSort() == o2.getMSort())
					return 0;
				return -1;
			}

		});
		return menus;
	}

	public List<Menu> findByParentId(Integer parentId) {
		String sql = "select * from si_menu where m_parent_id = ?";
		return find(sql, parentId);
	}

	/**
	 * 解除角色与资源菜单的关联关系
	 * 
	 * @author Mandarava
	 * @param roleId
	 * @return
	 */
	public Integer removeRoleAndMenusMapping(Long roleId) {
		String sql = " delete from si_role_menu where role_id = ?";
		return Db.update(sql, roleId);
	}

	/**
	 * 根据菜单ID验证菜单是否已和角色绑定
	 * 
	 * @author Mandarava
	 * @param menuId
	 * @return
	 */
	public Boolean hasRole() {
		String sql = "select count(*) from si_role_menu where menu_id = ?";
		Integer count = Db.queryLong(sql, getId()).intValue();
		return count > 0;
	}

	/**
	 * 验证菜单下是否存在权限
	 * 
	 * @author Mandarava
	 * @return
	 */
	public Boolean hasPermission() {
		return Permission.dao.existsPermission(getId());
	}

	/**
	 * 建立角色与资源菜单的对应关系
	 * 
	 * @author Mandarava
	 * @param menusIds
	 * @param roleId
	 */
	public void setRoleAndMenusMapping(String[] menusIds, Long roleId) {
		String sql = " INSERT INTO SI_ROLE_MENU(ROLE_ID, MENU_ID) VALUES(?,?)";
		for (String menuId : menusIds) {
			Db.update(sql, roleId,Long.valueOf(menuId));
		}
	}

	/**
	 * 查询不包括一级菜单的所有的菜单
	 * @author lixp
	 * @time:2017年7月11日 下午5:25:30
	 */
	public List<Menu> findMenus() {
		String sql = "select * from si_menu where m_parent_id != 'null' order by m_sort";
		return find(sql);
	}

	public List<Record> menusRoleList(Long roleId) {
		String sql = "select * from si_role_menu where role_id = ?";
		return Db.find(sql, roleId);
	}
}
