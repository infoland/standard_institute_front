
/**   
 * @Title: LoginValidator.java 
 * @Package: com.aft.jlgh.modules.user.validator 
 * @Description: TODO
 * @author Mandarava  
 * @date 2016年5月19日 下午2:09:44 
 * @version 1.3.1 
 * @Email wuyan1688@qq.com
 */

package com.infoland.modules.main.validator;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;

/**
 * 登陆校验器
 * 
 * @author Mandarava
 * @date 2016年5月19日 下午2:09:44
 * @version V1.3.1
 */

public class LoginValidator extends Validator {

	/**
	 * @param arg0
	 * @see com.jfinal.validate.Validator#handleError(com.jfinal.core.Controller)
	 */

	@Override
	protected void handleError(Controller arg0) {
		arg0.keepPara("username");
		arg0.keepPara("password");
		arg0.render("login.html");
	}

	/**
	 * @param arg0
	 * @see com.jfinal.validate.Validator#validate(com.jfinal.core.Controller)
	 */

	@Override
	protected void validate(Controller arg0) {
		validateRequiredString("username", "nameMessage", "请输入用户名");
		validateRequiredString("password", "passMessage", "请输入密码");
		if (StrKit.isBlank(controller.getPara("loginCaptcha"))) {
			validateRequiredString("loginCaptcha", "captchaMessage", "请输入验证码");
		} else {
			validateCaptcha("loginCaptcha", "captchaMessage", "验证码不正确");
		}
	}

}
