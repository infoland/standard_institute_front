package com.infoland.modules.main.controller;

import com.jfinal.core.Controller;

public class AdminController extends Controller {

	public void index() {
		render("/modules/index.html");
	}

	/**
	 * 登陆 图片验证码
	 *
	 * @author Mandarava
	 */
	public void loginCaptcha() {
		renderCaptcha();
	}
}
