package com.infoland.modules.standardDynamic.service;

import com.infoland.modules.basic.Message;
import com.infoland.modules.standardDynamic.service.impl.StandardDynamicServiceImpl;
import com.jfinal.aop.Duang;

/** 
 * @Description 
 * @author Wsy
 * @date 2017年7月27日 下午3:56:49 
 * @version V1.3.1
 */ 
  	
public interface StandardDynamicService {

	public static final StandardDynamicService service = Duang.duang(StandardDynamicServiceImpl.class);
	
	/**
	 * 
	 * @Description 		根据标准状态获取标准信息
	 * @author Wsy
	 * @param standardType	标准状态
	 * @param pageNumber	页数
	 * @return				标准分页对象
	 */
	public Message getStandardAvisoByType(String standardType,String pageNumber);
	
	/**
	 * 
	 * @Description 根据记录状态获取详细信息
	 * @author yinyg 
	 * @param id 记录状态 标识符
	 * @return
	 */
	public Message getStandardInfoById(String id) ;
	
	/**
	 * 
	 * @Description 根据标识符（A001）获得代替/被代替关系，展示代替/被代替关系的详细信息。
	 * @author yinyg
	 * @param id 标识符
	 * @param type 需要查询的信息类型代替（A461）/被代替（A462）
	 * @return 
	 */
	public Message getReplaceORBereplaceStandard(String id, String type);
	
	/**
	 * 
	 * @Description 根据标识符（A001）获得引用/被引用关系，展示引用/被引用关系的详细信息。
	 * @author yinyg
	 * @param id 标识符
	 * @param type 需要查询的信息类型引用（A502）/被引用（An502）
	 * @return
	 */
	public Message getQuoteORQuotedStandard(String id, String type);
	
	/**
	 * 
	 * @Description 根据标识符（A001）获得采用/被采用关系，展示采用/被采用关系的详细信息。
	 * @author yinyg
	 * @param id 标识符
	 * @param type 需要查询的信息类型采用（A800）/被采用（An800）
	 * @return
	 */
	public Message getAdoptORAdoptedStandard(String id, String type);
	
	/**
	 * 
	 * @Description 根据标识符（A001）获得同一中国/国际标准分类下的题录信息，展示同一中国/国际标准分类下的题录信息。
	 * @author yinyg
	 * @param type 需要查询的信息类型中标（Ccs）/国标（Ics）
	 * @param code 当type是中标（Ccs）时，code是中标的值（A825）,当type是国标（Ics）时，code是国标的值（A826）
	 * @return
	 */
	public Message getCcsOrIcsStandard(String type, String code);
	
	/**
	 * @Description 在线预览标准的电子全文前3页信息，预览页数默认值为3，后台可根据需要设置预览页数。
	 * @author yinyg
	 * @param userName 用户名
	 * @param code A100文献号
	 * @param filename 文件名
	 * @return
	 */
	public Message getElecFileByBefore(String code, String filename);
	
	/**
	 * @Description 根据标识符（A001）获得标准列表页或详情页中题录的电子全文信息，一条题录可能对应多个电子全文；
	 * 			  对于下订单购买的用户传入用户登录名（userLoginName），可以返回用户是否已购买（isBuy）信息。
	 * @author yinyg
	 * @param userName 用户名
	 * @param code A001标识符
	 * @return
	 */
	public Message getStandardElec(String userName, String code);
	
	/**
	 * @Description 检查该用户是否收藏该标准
	 * @author yinyg
	 * @param userName 用户名
	 * @param code 编号
	 * @return
	 */
	public Message checkCollection(String userName, String code);
	
	/**
	 * @Description 检查该用户是否跟踪该标准
	 * @author yinyg
	 * @param userName 用户名
	 * @param code 编号
	 * @return
	 */
	public Message checkTracking(String userName, String code);
}
