package com.infoland.modules.standardDynamic.controller;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.standardDynamic.service.StandardDynamicService;

/**
 * @Description
 * @author Wsy
 * @date 2017年7月27日 下午3:49:37
 * @version V1.3.1
 */

public class StandardDynamicController extends BaseController<UserInfo> {

	/**
	 * 
	 * @Description 根据标准状态获得标准信息
	 * @author Wsy
	 */
	public void getStandardAvisoByType() {
		String standardType = getJSONPara("standard_type"); // 标准类型
		String pageNumber = getJSONPara("pageNumber", "1");
		Message message = StandardDynamicService.service.getStandardAvisoByType(standardType, pageNumber);
		renderJson(message);
	}

	/**
	 * 
	 * @Description 根据标识符获取动态详细信息
	 * @author yinyg
	 */
	public void getStandardInfoById() {
		String recordStateInt = getJSONPara("recordStateInt"); // 标识符
		Message message = StandardDynamicService.service.getStandardInfoById(recordStateInt);
		renderJson(message);
	}

	/**
	 * 
	 * @Description 根据标识符（A001）获得代替/被代替关系，展示代替/被代替关系的详细信息。
	 * @author yinyg
	 */
	public void getReplaceORBereplaceStandard() {
		String id = getJSONPara("recordStateInt");
		String type = getJSONPara("type"); // 取代或被替代码
		Message message = StandardDynamicService.service.getReplaceORBereplaceStandard(id, type);
		renderJson(message);
	}

	/**
	 * 
	 * @Description 根据标识符（A001）获得引用/被引用关系，展示引用/被引用关系的详细信息。
	 * @author yinyg
	 */
	public void getQuoteORQuotedStandard() {
		String id = getJSONPara("recordStateInt");
		String type = getJSONPara("type");
		Message message = StandardDynamicService.service.getQuoteORQuotedStandard(id, type);
		renderJson(message);
	}

	/**
	 * 
	 * @Description 根据标识符（A001）获得采用/被采用关系，展示采用/被采用关系的详细信息。
	 * @author yinyg
	 */
	public void getAdoptORAdoptedStandard() {
		String id = getJSONPara("recordStateInt");
		String type = getJSONPara("type");
		Message message = StandardDynamicService.service.getAdoptORAdoptedStandard(id, type);
		renderJson(message);
	}

	/**
	 * 
	 * @Description 根据标识符（A001）获得同一中国/国际标准分类下的题录信息，展示同一中国/国际标准分类下的题录信息。
	 * @author yinyg
	 */
	public void getCcsOrIcsStandard() {
		String type = getJSONPara("type");
		String code = getJSONPara("code");
		Message message = StandardDynamicService.service.getCcsOrIcsStandard(type, code);
		renderJson(message);
	}

	/**
	 * @Description 在线预览标准的电子全文前3页信息，预览页数默认值为3，后台可根据需要设置预览页数。
	 * @author yinyg
	 */
	public void getElecFileByBefore() {
		String code = getPara("code");
		String filename = getPara("filename");
		Message message = StandardDynamicService.service.getElecFileByBefore(code, filename);
		String pdfPath = (String) message.getObject();
		setAttr("pdfPath", pdfPath);
		setAttr("pdfName", filename);
		render("/modules/template/pdf.html");
	}

	/**
	 * @Description 根据标识符（A001）获得标准列表页或详情页中题录的电子全文信息，一条题录可能对应多个电子全文；
	 *              对于下订单购买的用户传入用户登录名（userLoginName），可以返回用户是否已购买（isBuy）信息。
	 * @author yinyg
	 */
	public void getStandardElec() {
		String code = getJSONPara("code");
		String userName = getJSONPara("userName", "");
		Message message = StandardDynamicService.service.getStandardElec(userName, code);
		renderJson(message);
	}

	/**
	 * @Description 检查该用户是否收藏该标准
	 * @author yinyg
	 */
	public void checkCollection() {
		String code = getJSONPara("code");
		String userName = getJSONPara("userName");
		Message message = StandardDynamicService.service.checkCollection(userName, code);
		renderJson(message);
	}

	/**
	 * @Description 检查该用户是否跟踪该标准
	 * @author yinyg
	 */
	public void checkTracking() {
		String code = getJSONPara("code");
		String userName = getJSONPara("userName");
		Message message = StandardDynamicService.service.checkTracking(userName, code);
		renderJson(message);
	}
}
