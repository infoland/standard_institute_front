package com.infoland.modules.search.controller;
	
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.search.service.SearchService;

/**
 * 标准检索控制层
 * @author lixp
 *
 */
  	
public class SearchController extends BaseController<UserInfo>{
	/**
	 * 简单检索
	 */
	public void simpleSearch() {
		renderJson(SearchService.service.getSimpleSearch(this));
	}

	/**
	 * 高级检索
	 */
	public void advencedSearch() {
		renderJson(SearchService.service.getAdvencedSearch(this));
		
	}

	/**
	 * 获取标准品种
	 */
	public void getStandardSortList() {
		// 获取标准品种检索词
		String keyWord = getJSONPara("key_word");
		// 获取标准品种类别
		String subType = getJSONPara("sub_type");
		renderJson(SearchService.service.getStandardSortList(keyWord, subType));
	}

	/**
	 * 获取标准题录详情
	 */
	public void getStandardDetail() {
		// 获取标识符
		String standardId = getJSONPara("standard_id");
		renderJson(SearchService.service.getStandardDetail(standardId));
	}
	
	/**
	 * 获取标准电子全文信息
	 */
	public void getStandardElec() {
		// 获取标识符
		String standardId = getJSONPara("standard_id");
		renderJson(SearchService.service.getStandardElec(standardId));
	}
	
	/**
	 * 获取可能关注的其他题录
	 */
	public void getConcernStandard() {
		String identifier = getJSONPara("identifier");
		renderJson(SearchService.service.getConcernStandard(identifier));
	}
	
	/**
	 * 查询标准动态
	 * @Description 
	 * @author dw
	 */
	public void getStandard(){
		renderJson(SearchService.service.standardList());
	}
	
	/**
	 * 查询中标分类
	 * @Description 
	 * @author dw
	 */
	public void findAll(){
		renderJson(SearchService.service.findAll());
	}
	
	/**
	 * 查询国标数据
	 * @Description 
	 * @author dw
	 */ 
	public void findInterstandardClasses(){
		renderJson(SearchService.service.findInterstandardClasses());
	}
	
	/**
	 * 查询所有热词
	 * @Description 
	 * @author dw
	 */
	public void hotWordsList(){
		renderJson(SearchService.service.hotList());
	}
	
	/**
	 * 获取字典表中高级检索的时间
	 * @Description 
	 * @author dw
	 */
	public void standardTimeList(){
		renderJson(SearchService.service.standardTimeList());
	}
	
}
