package com.infoland.modules.search.service;

import java.util.List;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.maintenance.model.Dict;
import com.infoland.modules.maintenance.model.HotWords;
import com.infoland.modules.search.service.impl.SearchServiceImpl;
import com.infoland.modules.standardClass.model.ChinastandardClasses;
import com.infoland.modules.standardClass.model.InterstandardClasses;
import com.jfinal.aop.Duang;
/**
 * 检索功能service
 * @author lixp
 *
 */
public interface SearchService {
	public static final SearchService service = Duang.duang(SearchServiceImpl.class);
	
	/**
	 * 获取简单检索查询后的信息
	 * @param keyWord       查询关键词
	 * @param pageNumber	显示的页数
	 * @return   			包含调用接口成功与否的信息，以及返回的数据
	 */
	Message getSimpleSearch(@SuppressWarnings("rawtypes") BaseController controller);
	
	/**
	 * 获取标准电子全文信息
	 * @param standardId	标识符
	 * @return              包含查询全文信息成功与否的信息以及返回的数据
	 */
	Message getStandardElec(String standardId);
	
	
	/**
	 * 获取标准品种信息
	 * @param keyWord		关键词
	 * @param subType		分类标识信息
	 * @return				包含查询品种成功与否的信息以及返回的数据
	 */
	Message getStandardSortList(String keyWord,String subType);
	
	/**
	 * 获取标准题录详情
	 * @param standardId	标识符
	 * @return				包含题录详情查询成功与否的信息以及返回的数据
	 */
	Message getStandardDetail(String standardId);
	
	/**
	 * 获取高级检索查询后信息
	 * @param search		查询的对象（包括查询的所有条件）
	 * @param pageNumber	分页的页数
	 * @return
	 */
	Message getAdvencedSearch(@SuppressWarnings("rawtypes") BaseController con);
	
	/**
	 * 查询标准状态
	 * @Description 
	 * @author dw
	 * @return
	 */
	List<Dict> standardList();
	/**
	 * 查询中标数据
	 * @Description 
	 * @author dw
	 * @return
	 */
	List<ChinastandardClasses> findAll();
	
	/**
	 * 查询国标数据
	 * @Description 
	 * @author dw
	 * @return
	 */
	List<InterstandardClasses> findInterstandardClasses();
	/**
	 * 查询所有热词数据
	 * @Description 
	 * @author dw
	 * @return
	 */
	 List<HotWords> hotList();
	 
	 /**
	  * 获取其他关注的题录
	  * @Description 
	  * @author dw
	  * @param identifier
	  * @param pageNumber
	  * @param pageSize
	  * @return
	  */
	 Message getConcernStandard(String identifier);
	 
	 /**
	  * 获取字典表中的高级检索时间
	  * @Description 
	  * @author dw
	  * @return
	  */
	 List<Dict> standardTimeList();

}
