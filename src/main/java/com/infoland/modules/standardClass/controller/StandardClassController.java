package com.infoland.modules.standardClass.controller;



import com.infoland.app.Constants;
import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.members.model.UserInfo;
import com.infoland.modules.standardClass.model.ChinastandardClasses;
import com.infoland.modules.standardClass.service.StandardClassService;



/**
 * 
 * @Description 
 * @author dw
 * @date 2017年7月17日 下午3:16:32 
 * @version V1.3.1
 */
  	
public class StandardClassController extends BaseController<ChinastandardClasses> {
	
	/**
	 * 查询所有的专题分类信息
	 * @Description 
	 * @author dw
	 */
	public void findAllSpecialsList(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",25);
		renderJson(StandardClassService.service.findAllList(pageNumber, pageSize));
	}
	
	/**
	 * 查询专题目录树集合
	 * @Description 
	 * @author dw
	 */
	public void findTreeList(){   
		String pUId = getJSONPara("uid");
		Message message = StandardClassService.service.findTreeList(pUId);
		renderJson(message);
	}
	
	/**
	 * 调用中标院接口获取专题库具体子节点信息
	 * @Description 
	 * @author dw
	 */
	public void treeNodeList(){
		String pageNumber = getJSONPara("pageCount");
		String pageSize = getJSONPara("pageSize");
		String categoryID = getJSONPara("uid");
		Message message = StandardClassService.service.treeNodeList(pageNumber,pageSize,categoryID);
		renderJson(message);
	}
	
	/**
	 * 调用中标院验证获取用户请求信息接口
	 * @Description 
	 * @author dw
	 */
	public void  saveValidity(){
		String standardCodes = getJSONPara("standardCode");
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		Message message = StandardClassService.service.saveValidity(standardCodes,userInfo.getUserName());
		renderJson(message);
	}
	
	
	/**
	 * 导出题录
	 * @Description 
	 * @author dw
	 */
	public void  exportExcel(){
		UserInfo userInfo = (UserInfo)getSession().getAttribute(Constants.SESSION_NAME);
		StandardClassService.service.exportValidationItem(this,userInfo.getUserName());
		renderNull();
	}
	
	/**
	 * 导入题录
	 * @Description 
	 * @author dw
	 */
	public void  uploadValidationItem(){
		Integer userId = getParaToInt(0);
		renderJson(StandardClassService.service.uploadValidationItem(getFile(),userId));
	}
	
	/**
	 * 导入成功的数据
	 * @Description 
	 * @author dw
	 */
	public void importSuccessValidity(){
		renderJson(StandardClassService.service.importSuccessValidity(this));
	}
	
	public void importFailValidity(){
		renderJson(StandardClassService.service.importFailValidity(this));
	}
}
