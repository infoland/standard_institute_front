package com.infoland.modules.standardClass.service;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.basic.Message;
import com.infoland.modules.standardClass.model.SpecialstandarClasses;
import com.infoland.modules.standardClass.service.impl.StandardClassServiceImpl;
import com.jfinal.aop.Duang;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;


/**
 * 
 * @Description 
 * @author Yanglj
 * @date 2017年6月27日 下午5:30:41 
 * @version V1.3.1
 */
public interface StandardClassService{

	public static final StandardClassService service = Duang.duang(StandardClassServiceImpl.class);
	
	/**
	 * 查询所有的专题分类信息
	 * @Description 
	 * @author dw
	 * @param pageNumber 页数
	 * @param pageSize 数据总数
	 * @return
	 */
	Page<SpecialstandarClasses> findAllList(Integer pageNumber,Integer pageSize);
	
	/**
	 * 查询专题目录树集合
	 * @Description 
	 * @author dw
	 * @param pUId 父级标识符
	 * @return
	 */
	Message findTreeList(String pUId);
	
	/**
	 * 调用接口获取目录树中的具体节点信息
	 * @Description 
	 * @author dw
	 * @return
	 */
	Message treeNodeList(String pageNumber,String pageSize,String categoryID);
	
	/**
	 * 调用中标院验证获取用户请求信息接口
	 * @Description 
	 * @author dw
	 * @return
	 */
	Message saveValidity(String standardCode,String userName);
	
	/**
	 * 导入有效性确认成功的数据
	 * @Description 
	 * @author dw
	 * @param controller
	 * @return
	 */
	Message importSuccessValidity(@SuppressWarnings("rawtypes") BaseController controller);
	
	Message importFailValidity(@SuppressWarnings("rawtypes") BaseController controller);
	
	
	/**
	 * 导出题录
	 * @Description 
	 * @author dw
	 * @param standardCode 标准号
	 * @param unit 发布单位
	 * @param standardName 标准名称
	 * @param standardState 标准状态
	 * @param date 发布日期
	 * @return
	 */
	void exportValidationItem(@SuppressWarnings("rawtypes") BaseController controller,String userName);
	
	/**
	 * 导入题录
	 * @Description 
	 * @author dw
	 * @param uploadFile
	 * @return
	 */
	Message uploadValidationItem(UploadFile uploadFile,Integer userId);

}
