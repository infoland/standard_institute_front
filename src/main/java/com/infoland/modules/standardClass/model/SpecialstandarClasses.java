package com.infoland.modules.standardClass.model;

import java.util.List;

import com.infoland.modules.standardClass.model.base.BaseSpecialstandarClasses;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 
 * @Description 
 * @author dw
 * @date 2017年7月17日 下午2:45:11 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class SpecialstandarClasses extends BaseSpecialstandarClasses<SpecialstandarClasses> {
	public static final SpecialstandarClasses dao = new SpecialstandarClasses();
	
	/**
	 * 前台--查询所有的专题分类信息
	 * @Description 
	 * @author dw
	 * @param pageNumber 页数
	 * @param pageSize 数据总数
	 * @return
	 */
	public Page<SpecialstandarClasses> findAllList(Integer pageNumber,Integer pageSize){
		String sql = getSql("specisSql.specisSelect");
		SqlPara sqlExceptSelect = getSqlPara("specisSql.specisFrom");
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
	}
	
	/**
	 * 前台--查询专题目录树
	 * @Description 
	 * @author dw
	 * @param pUId 父级标识符
	 * @return
	 */
	public List<SpecialstandarClasses> findTreeList(String pUId){
		Kv param = Kv.create();
		if(pUId!=null){
			param.set("pUId","pUId");
		}
		SqlPara sql = getSqlPara("specisSql.specisSearch", param);
		return find(sql.getSql(),sql.getPara());
	}
	
}
