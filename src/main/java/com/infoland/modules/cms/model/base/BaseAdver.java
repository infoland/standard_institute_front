package com.infoland.modules.cms.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAdver<M extends BaseAdver<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}

	public java.lang.Long getId() {
		return get("id");
	}

	public void setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
	}

	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public void setModifyDate(java.util.Date modifyDate) {
		set("modify_date", modifyDate);
	}

	public java.util.Date getModifyDate() {
		return get("modify_date");
	}

	public void setAdName(java.lang.String adName) {
		set("ad_name", adName);
	}

	public java.lang.String getAdName() {
		return get("ad_name");
	}

	public void setAdPath(java.lang.String adPath) {
		set("ad_path", adPath);
	}

	public java.lang.String getAdPath() {
		return get("ad_path");
	}

	public void setAdParams(java.lang.String adParams) {
		set("ad_params", adParams);
	}

	public java.lang.String getAdParams() {
		return get("ad_params");
	}

	public void setAdDescription(java.lang.String adDescription) {
		set("ad_description", adDescription);
	}

	public java.lang.String getAdDescription() {
		return get("ad_description");
	}

	public void setAdStatus(java.lang.Boolean adStatus) {
		set("ad_status", adStatus);
	}

	public java.lang.Boolean getAdStatus() {
		return get("ad_status");
	}

	public void setAdverCategoryId(java.lang.Long adverCategoryId) {
		set("adver_category_id", adverCategoryId);
	}

	public java.lang.Long getAdverCategoryId() {
		return get("adver_category_id");
	}

	public void setAdSort(java.lang.Integer adSort) {
		set("ad_sort", adSort);
	}

	public java.lang.Integer getAdSort() {
		return get("ad_sort");
	}

}
