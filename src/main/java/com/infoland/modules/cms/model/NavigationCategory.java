package com.infoland.modules.cms.model;


import com.infoland.modules.cms.model.base.BaseNavigationCategory;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class NavigationCategory extends BaseNavigationCategory<NavigationCategory> {
	public static final NavigationCategory dao = new NavigationCategory();
}
