package com.infoland.modules.cms.model;

import java.util.List;

import com.infoland.modules.cms.model.base.BaseUploadimage;
import com.jfinal.plugin.activerecord.Db;

/**
 * Generated by JFinal.
 */
public class Uploadimage extends BaseUploadimage<Uploadimage> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6844465802927436050L;
	public static final Uploadimage dao = new Uploadimage();

	/**
	 * 重写保存方法
	 * 
	 * @author Janel.Han
	 * @param model
	 * @return
	 */
	public boolean saved() {
		return set("create_date", new java.util.Date()).save();
	}

	public Uploadimage getByForeignId(Long foreignId) {
		return findFirst("select * from bzy_uploadimage where foreign_id=?", foreignId);
	}
	
	public List<Uploadimage> findByForeignId(Long foreignId) {
		return find("SELECT * FROM bzy_UPLOADIMAGE WHERE FOREIGN_ID= ? ORDER BY UPLOADIMAGE_ORDER ASC", foreignId);
	}
	
	public Integer deleteByForeignId(Long foreignId, Integer uploadImageType, Integer owenType) {
		return Db.update("DELETE FROM bzy_UPLOADIMAGE WHERE FOREIGN_ID = ? AND UPLOADIMAGE_TYPE = ? AND OWEN_TYPE = ?", 
				foreignId, uploadImageType, owenType);
	}
	
	public List<Uploadimage> findByForeignId(Long foreignId, Integer uploadImageType, Integer owenType) {
		return find("SELECT * FROM bzy_UPLOADIMAGE WHERE FOREIGN_ID = ? AND UPLOADIMAGE_TYPE = ? AND OWEN_TYPE = ? ORDER BY UPLOADIMAGE_ORDER ASC", 
				foreignId, uploadImageType, owenType);
	}
	
	public Uploadimage findImageByForeignId(Long foreignId, Integer uploadImageType, Integer owenType) {
		return findFirst("SELECT * FROM bzy_UPLOADIMAGE WHERE FOREIGN_ID = ? AND UPLOADIMAGE_TYPE = ? AND OWEN_TYPE = ? ORDER BY UPLOADIMAGE_ORDER ASC", 
				foreignId, uploadImageType, owenType);
	}
	
	public Boolean updateImageSort(Long id, Integer sort) {
		return Db.update("UPDATE bzy_UPLOADIMAGE SET UPLOADIMAGE_ORDER = ? WHERE ID = ? ", sort, id) > 0 ? true : false;
	}
}
