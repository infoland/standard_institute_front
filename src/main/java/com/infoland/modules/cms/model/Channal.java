package com.infoland.modules.cms.model;


import java.util.List;

import com.infoland.modules.cms.model.base.BaseChannal;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 通知公告
 * @Description 
 * @author dw
 * @date 2017年7月25日 上午9:51:59 
 * @version V1.3.1
 */
@SuppressWarnings("serial")
public class Channal extends BaseChannal<Channal> {
	public static final Channal dao = new Channal();
	
	/**
	 * 查询所有栏目
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Channal> findAllChannal(Integer pageNumber,Integer pageSize){
		String sql = getSql("channalSql.channalSelect");
		Kv param = Kv.create();
		SqlPara sqlExceptSelect = getSqlPara("channalSql.channalFrom", param);
		return paginate(pageNumber, pageSize, sql, sqlExceptSelect.getSql(),sqlExceptSelect.getPara());
	}
	
	/**
	 * 根据父级id查询子栏目
	 * @Description 
	 * @author dw
	 * @param pId 父级栏目 id
	 * @return
	 */
	public List<Channal> findChannalByPId(Long pId){
		Kv param = Kv.create();
		param.set("pId",pId);
		SqlPara sql = getSqlPara("childSql.childSelect",param);
		return find(sql.getSql(),sql.getPara());
	}
}
