package com.infoland.modules.cms.controller;

import com.infoland.modules.basic.BaseController;
import com.infoland.modules.cms.model.Context;
import com.infoland.modules.cms.service.CmsService;
import com.jfinal.plugin.activerecord.Page;

public class ContextController extends BaseController<Context> {

	/**
	 * 根据栏目id查询下面的相关文章标题
	 * @Description 
	 * @author dw
	 */
	public void findAllContext(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",8);
		String channalId = getJSONPara("id");
		Page<Context> page = CmsService.service.findAllContextByChannalId(pageNumber, pageSize, channalId);;
		renderJson(page);
	}
	
	/**
	 * 查询某一篇文章的具体详情
	 * @Description 
	 * @author dw
	 */
	public void contextDetail(){
		Long channalId = getParaToLong(0);
		Long id = getParaToLong(1);
		renderJson(CmsService.service.detailsContext(channalId,id));
	}
	
	/**
	 * 查询所有文章
	 * @Description 
	 * @author dw
	 */
	public void findContext(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",15);
		renderJson(CmsService.service.findAllContext(pageNumber, pageSize));
	}
	
	
	/**
	 * 根据栏目ID和文章id查看文章详情
	 * @Description 
	 * @author dw
	 */
	public void findContextDetail(){
		Long id = getParaToLong(0);
		renderJson(CmsService.service.findContext(id));
	}
	
	
	/**
	 * 查询标准服务下面子介绍
	 * @Description 
	 * @author dw
	 */
	public void findContextByService(){
		Long id = getJSONParaToLong("id");
		renderJson(CmsService.service.findContextDetail(id));
	}
	
	/**
	 * 查询所有文章标题
	 * @Description 
	 * @author dw
	 */
	public void titleList(){
		renderJson(CmsService.service.contextTitle());
	}
	

}
