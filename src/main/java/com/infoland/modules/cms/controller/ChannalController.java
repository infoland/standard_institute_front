package com.infoland.modules.cms.controller;


import com.infoland.modules.basic.BaseController;
import com.infoland.modules.cms.model.Channal;
import com.infoland.modules.cms.service.CmsService;
import com.jfinal.plugin.activerecord.Page;

public class ChannalController extends BaseController<Channal> {

	/**
	 * 查询所有一级栏目
	 * @Description 
	 * @author dw
	 */
	public void findAllChannal(){
		Integer pageNumber = getJSONParaToInt("pageNumber", 1);
		Integer pageSize = getJSONParaToInt("pageSize",15);
		Page<Channal> page = CmsService.service.findAllChannal(pageNumber, pageSize);
		renderJson(page);
	}
	
	/**
	 * 根据父级id查询子栏目
	 * @Description 
	 * @author dw
	 */
	public void findChannalByPId(){
		Long pId = getJSONParaToLong("id");
		renderJson(CmsService.service.findChannalByPId(pId));
	}
	
	
	
}
