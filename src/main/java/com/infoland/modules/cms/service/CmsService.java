package com.infoland.modules.cms.service;

import java.util.List;

import com.infoland.modules.basic.Message;
import com.infoland.modules.cms.model.Channal;
import com.infoland.modules.cms.model.Context;
import com.infoland.modules.cms.service.impl.CmsServiceImpl;
import com.jfinal.aop.Duang;
import com.jfinal.plugin.activerecord.Page;

public interface CmsService {

	public static final CmsService service = Duang.duang(CmsServiceImpl.class);
	
	/**
	 * 查询所有栏目
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Page<Channal> findAllChannal(Integer pageNumber,Integer pageSize);
	
	/**
	 * 根据父级栏目id查询子栏目
	 * @Description 
	 * @author dw
	 * @param pId
	 * @return
	 */
	List<Channal> findChannalByPId(Long pId);
	
	/**
	 * 根据栏目id查询相关子内容
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @param channalId
	 * @return
	 */
	Page<Context> findAllContextByChannalId(Integer pageNumber,Integer pageSize,String channalId);
	
	/**
	 * 查询某一篇文章的具体详情
	 * @Description 
	 * @author dw
	 * @param id
	 * @param channalId
	 * @return
	 */
	Context detailsContext(Long channalId,Long id);
	
	/**
	 * 查询所有文章
	 * @Description 
	 * @author dw
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	Page<Context> findAllContext(Integer pageNumber,Integer pageSize);
	
	/**
	 * 根据栏目ID和文章id查看文章详情
	 * @Description 
	 * @author dw
	 * @param channalId
	 * @param id
	 * @return
	 */
	Context findContext(Long id);
	
	/**
	 * 查询标准服务下面子介绍
	 * @Description 
	 * @author dw
	 * @param channalId
	 * @return
	 */
	List<Context> findContextDetail(Long channalId);
	
	/**
	 * 查询所有文章标题
	 * @Description 
	 * @author dw
	 * @return
	 */
	Message contextTitle();
	
	/**
	 * 查询上个月份的前五条记录 
	 * @Description 
	 * @author dw
	 * @return
	 */
	List<Context> lastMonthContextTitle();
	

}
