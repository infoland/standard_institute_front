package com.infoland.modules.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.infoland.modules.basic.Message;
import com.infoland.modules.cms.model.Channal;
import com.infoland.modules.cms.model.Context;
import com.infoland.modules.cms.service.CmsService;
import com.jfinal.plugin.activerecord.Page;

public class CmsServiceImpl implements CmsService {

	/**
	 * 查询所有栏目
	 * Description 
	 * @param pageNumber
	 * @param pageSize
	 * @return 
	 */
	@Override
	public Page<Channal> findAllChannal(Integer pageNumber, Integer pageSize) {
		return Channal.dao.findAllChannal(pageNumber, pageSize);
	}

	/**
	 * 根据父级id查询子栏目
	 * Description 
	 * @param pId父级栏目id
	 * @return 
	 */
	@Override
	public List<Channal> findChannalByPId(Long pId) {
		return Channal.dao.findChannalByPId(pId);
	}

	/**
	 * 根据栏目id查询相关子内容
	 * Description 
	 * @param pageNumber
	 * @param pageSize
	 * @param channalId
	 * @return 
	 */
	@Override
	public Page<Context> findAllContextByChannalId(Integer pageNumber, Integer pageSize, String channalId) {
		return Context.dao.findAllContextByChannalId(pageNumber, pageSize, channalId);
	}

	/**
	 * 查询某一篇文章的具体详情
	 * Description 
	 * @param id
	 * @param channalId
	 * @return 
	 */
	@Override
	public Context detailsContext(Long channalId,Long id) {
		return Context.dao.contextDetail(channalId,id);
	}

	/**
	 * 查询所有文章
	 * Description 
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public Page<Context> findAllContext(Integer pageNumber, Integer pageSize) {
		return Context.dao.findAllContext(pageNumber, pageSize);
	}

	/**
	 * 根据栏目ID和文章id查看文章详情
	 * Description 
	 * @param channalId
	 * @param id
	 * @return
	 */
	@Override
	public Context findContext(Long id) {
		return Context.dao.findContext(id);
	}

	/**
	 * 查询标准服务下面子介绍
	 * Description 
	 * @param channalId
	 * @return
	 */
	@Override
	public List<Context> findContextDetail(Long channalId) {
		return Context.dao.findContextDetail(channalId);
	}

	/**
	 * 查询所有文章标题
	 * Description 
	 * @return
	 */
	@Override
	public Message contextTitle() {
		Message message = new Message();
		List<Context> contextList = Context.dao.contextTitle();
		List<Context> list= new ArrayList<Context>();
		if(contextList!=null && contextList.size()>0){
			list = Context.dao.contextTitle();
		}else{
			list = Context.dao.lastMonthContextTitle();
		}
		if(list!=null && list.size()>0){
			message.setObject(list);
			message.setMessage("获取数据成功");
			message.setSuccess(Boolean.TRUE);
		}else{
			message.setMessage("本月或上月没有通知公告");
		}
		return message;
	}

	/**	
	 * 查询上个月的前5条记录
	 * Description 
	 * @return
	 */
	@Override
	public List<Context> lastMonthContextTitle() {
		return Context.dao.lastMonthContextTitle();
	}


}
