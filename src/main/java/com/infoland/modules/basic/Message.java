
/**   
 * @Title: Message.java 
 * @Package: com.aft.jlgh.modules.base 
 * @Description: TODO
 * @author Mandarava  
 * @date 2016年5月19日 下午2:32:30 
 * @version 1.3.1 
 * @Email wuyan1688@qq.com
 */

package com.infoland.modules.basic;

/**
 * @author Mandarava
 * @date 2016年5月19日 下午2:32:30
 * @version V1.3.1
 */

public class Message {

	private boolean success;

	private String message;

	private Object object;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
