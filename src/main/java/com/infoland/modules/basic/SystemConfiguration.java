
/**   
 * @Title: SystemConfiguration.java 
 * @Package: com.aft.jlgh.modules.base 
 * @Description: TODO
 * @author Mandarava  
 * @date 2016年5月19日 下午2:46:54 
 * @version 1.3.1 
 * @Email wuyan1688@qq.com
 */

package com.infoland.modules.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infoland.modules.main.model.Configuation;

/** 
 * @author Mandarava
 * @date 2016年5月19日 下午2:46:54 
 * @version V1.3.1
 */

public class SystemConfiguration {

	private static Map<String, String> map;
	
	public static void initConfiguration(){
		Configuation.dao.updateVersion();
		map = new HashMap<String, String>();
		List<Configuation> configList = Configuation.dao.findAll();
		for(Configuation config : configList){
			map.put(config.getKey(), config.getValue());
		}
	}

	public static Map<String, String> getMap() {
		return map;
	}

	public static void setMap(Map<String, String> map) {
		SystemConfiguration.map = map;
	}
}
