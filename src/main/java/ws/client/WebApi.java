package ws.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.json.FastJson;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

public class WebApi {
	/**
	 * 
	 * @Description 
	 * @author Wsy
	 * @param wsdl		
	 * @param nameSpace	
	 * @param method
	 * @param params
	 * @param paramsValue
	 * @return
	 * @throws AxisFault
	 */
	public OMElement callMethod(String wsdl,String nameSpace,String method, String[] params, String[] paramsValue) throws AxisFault{
		ServiceClient sender = new ServiceClient();
         EndpointReference endpointReference = new EndpointReference(wsdl);  
         Options options = new Options();  
         options.setTo(endpointReference);  
         sender.setOptions(options);  
         OMFactory fac = OMAbstractFactory.getOMFactory();          
         OMNamespace omNs = fac.createOMNamespace(nameSpace,method);  
         OMElement data = fac.createOMElement(method, omNs);  
         for (int i = 0; i < params.length; i++) {  
             OMElement inner = fac.createOMElement(params[i], omNs);  
             inner.setText(paramsValue[i]);  
             data.addChild(inner);  
         }          
        OMElement result = sender.sendReceive(data);
		return  result;
	}
	//带分页信息的
	public List<Map<String, Object>> getNodeList(OMElement element) {
		if (element == null) {
			return null;
		}
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Iterator<?> iter = element.getChildElements();
		while (iter.hasNext()) {
			OMElement node = (OMElement) iter.next();
			if (node.getLocalName().equals("return")) {
				Map<String, Object> map = new HashMap<String, Object>();
				Iterator<?> iter1 = node.getChildElements();
				while (iter1.hasNext()) {
					OMElement node1 = (OMElement) iter1.next();
					map.put(node1.getLocalName(), node1.getText());
					if (node1.getLocalName().equals("pagination")) {
						Map<String, String> pagination = new HashMap<String, String>();
						Iterator<?> iter2 = node1.getChildElements();
						while (iter2.hasNext()) {
							OMElement node2 = (OMElement) iter2.next();
							pagination.put(node2.getLocalName(), node2.getText());
						}
						map.put("pagination", pagination);
					}
				}
				list.add(map);
			}
		}
		return list;
	}
	//返回字符串  包括boolean和其他标识性字符串（如：注册成功，注册失败等）
	public String getString(OMElement element) {
		if (element == null) {
			return "";
		}
		String n = "";
		Iterator<?> iter = element.getChildElements();
		while (iter.hasNext()) {
			OMElement node = (OMElement) iter.next();
			n = node.getText();
		}
		return n;
	}
	public  Map<String,Object> getElement(OMElement element) {
		if (element == null) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<?> iter = element.getChildElements();
		while (iter.hasNext()) {
			OMElement node = (OMElement) iter.next();
			if (node.getLocalName().equals("return")) {
				if(node.getChildElements()!=null){
					Iterator<?> iter1 = node.getChildElements();
					while (iter1.hasNext()) {
						OMElement node1 = (OMElement) iter1.next();
						map.put(node1.getLocalName(), node1.getText());
					}
				}
			}
		}
		return map;
	}
	public  Map<String, Object> getReturn(OMElement element) {
		if (element == null) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<?> iter = element.getChildElements();
		while (iter.hasNext()) {
			OMElement node = (OMElement) iter.next();
			if (node.getLocalName().equals("return")) {
				map.put(node.getLocalName(),node.getText());
			}
		//System.out.println(map);
		}
		return map;
	}
	//解析标准有效性数据
	public  List<Map<String, Record>> getValidityList(List<Object> list) throws java.io.IOException {
		if (list == null) {
			return null;
		}
		Iterator<?> iter = list.iterator();
		List<Map<String, Record>> newList = new ArrayList<Map<String,Record>>();
		while (iter.hasNext()) {
			String s =  iter.next().toString();
			String str = s.substring(s.indexOf(":")+1,s.length()-1);
			JSONObject js= JSONObject.parseObject(str);
			Set<String> set = js.keySet();
			String key = s.substring(s.indexOf("\"")+1,s.indexOf(":")-1);
			Map<String,Record> map = new HashMap<String,Record>();
			Record record = new Record();
			map.put(key,record);
			newList.add(map);
			for (String string : set) {
				record.set(string, js.getString(string));
			}
		}
		return newList;
	}
	
	//
	public  Map<String,Record> getRecord(List<Object> list){
		if(list == null || list.size() < 1){
			return null;
		}
		Map<String,Record> r = new HashMap<String,Record>(); 
		for (int i = 0; i < list.size(); i++) {
			String str = JsonKit.toJson(list.get(i));
			HashMap<String,Object> map = FastJson.getJson().parse(str, HashMap.class);
			Set<String> set = map.keySet();
			for (String s : set) {
				r.put(s, getRecord(map.get(s).toString()));
			}
		}
		return r;
	}
	
	@SuppressWarnings("unchecked")
	public  Map<String,Record> getRecordByString(String str){
		if(StrKit.isBlank(str)){
			return null;
		}
		Map<String,Record> r = new HashMap<String,Record>(); 
		Map<String,Object> map = JSON.parseObject(str);
		Set<String> set = map.keySet();
		for (String s : set) {
			r.put(s, getRecord(map.get(s).toString()));
		}
		return r;
	}
	
	public  Record getRecord(String str){
		Record r = new Record();
		HashMap<String,Object> map = FastJson.getJson().parse(str, HashMap.class);
		Set<String> set = map.keySet();
		for (String s : set) {
			r.set(s, map.get(s));
		}
		return r;
	}
	
	public  String readPDF(String pdfUrl) {
	    PDDocument helloDocument;
	    String t = "";
	    try {
	        helloDocument = PDDocument.load(new File(pdfUrl));
	        PDFTextStripper textStripper = new PDFTextStripper();
	        String text = textStripper.getText(helloDocument);
	        if(StrKit.notBlank(text)){
	        	t = text;
	        	System.out.println(t);
	        }else{
	        	System.out.println("************");
	        }
	        System.out.println(t);
	        helloDocument.close();
	     } catch (IOException e) {
	        e.printStackTrace();
	     }
	    if(StrKit.notBlank(t)){
	    	return t;
        }else{
        	return null;
        }
	 }
}
